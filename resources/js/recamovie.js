$(document).ready(function(){
    var screenshots = document.getElementById('screenshots');
    var addScreenshot = document.getElementById('addScreenshot');
    var deleteScreenshot = document.getElementById('deleteScreenshot');
    var screenshotFields = document.getElementsByClassName('screenshotFields');
    var indicators = document.getElementById('indicators');
    var mobileIndicators = document.getElementById('mobileIndicators')
    var innerCarousel = document.getElementById('innerCarousel');
    var mobileInnerCarousel = document.getElementById('mobileInnerCarousel');
    var screenshotNum = 0;
    var urls = [];

    fillCarousel();

    /**
     * This function creates a new input for the screenshots when you click on the "addScreenshot" button.
     */
    addScreenshot.onclick = function () {
        var fatherDiv = document.createElement("DIV");
        fatherDiv.classList.add("row", "py-2", "screenshotFields");
        var firstChildDiv = document.createElement("DIV");
        firstChildDiv.classList.add("col-2");
        var num = document.createTextNode(screenshotFields.length + 1);
        firstChildDiv.appendChild(num);
        var secondChildDiv = document.createElement("DIV");
        secondChildDiv.classList.add("col-10");
        var inputChild = document.createElement("INPUT");
        inputChild.onblur = function() {fillCarousel();};
        inputChild.type = "url";
        inputChild.name = "screenshots[]";
        inputChild.classList.add("form-control", "border");
        if (screenshotFields.length == 0) {
            inputChild.setAttribute("required", "required");
        }
        secondChildDiv.appendChild(inputChild);
        fatherDiv.appendChild(firstChildDiv);
        fatherDiv.appendChild(secondChildDiv);
        screenshots.appendChild(fatherDiv);
    }

    /**
     * This function deletes the las input created in the screenshots field, except the first one.
     */
    deleteScreenshot.onclick = function () {
        if (screenshotFields.length > 1) {
            screenshots.removeChild(screenshotFields[screenshotFields.length - 1]);
            fillCarousel();
        }
    }

    /**
     * This function trigger a click on the button that adds a scrrenshot once you load the webpage
     */
    if ($(screenshots).is(':empty')) {
        $(function() {
            $("#addScreenshot").click();
        });
    }

    /**
     * This function validates the title value, if it's valid the function includes the value in the preview, if i'ts not it deletes it from the preview and makes an alert visible
     */
    $("input[name='title']").blur(function(){
        if (this.value!="" && validTitle(this.value)) {
            $(this).removeClass("border-danger").addClass("border-success");
            $("#titlePreview").html($(this).val());
            $("#titleMobilePreview").html($(this).val());
        }
        else if (this.value=="") {
            $(this).removeClass("border-success border-danger");
            $("#titlePreview").empty();
            $("#titleMobilePreview").empty();
        }
        else {
            $(this).removeClass("border-success").addClass("border-danger");
            $("#titlePreview").empty();
            $("#titleMobilePreview").empty();
            $("#titleError").removeClass("d-none").addClass("show");
            $("#titleError span:last-child").remove();
            $("#titleError").append("<span>Error en el formato del título.</span>");
        }
    });

    /**
     * This function validates the year value, if it's valid the function includes the value in the preview, if i'ts not it deletes it from the preview and makes an alert visible
     */
    $("input[name='year']").blur(function(){
        if (this.value!="" && validYear(this.value)) {
            $(this).removeClass("border-danger").addClass("border-success");
            $("#yearPreview").html($(this).val());
            $("#yearMobilePreview").html($(this).val());
            $("#separator").removeClass("d-none");
            $("#mobileSeparator").removeClass("d-none");
        }
        else if (this.value=="") {
            $(this).removeClass("border-success border-danger");
            $("#yearPreview").empty();
            $("#yearMobilePreview").empty();
            $("#separator").addClass("d-none");
            $("#mobileSeparator").addClass("d-none");
        }
        else {
            $(this).removeClass("border-success").addClass("border-danger");
            $("#yearPreview").empty();
            $("#yearMobilePreview").empty();
            $("#separator").addClass("d-none");
            $("#mobileSeparator").addClass("d-none");
            $("#yearError").removeClass("d-none").addClass("show");
            $("#yearError span:last-child").remove();
            $("#yearError").append("<span>Error en el año de la película. Ha de estar entre 1833 y el año actual.</span>");
        }
    });

    /**
     * This function validates the director value, if it's valid the function includes the value in the preview, if i'ts not it deletes it from the preview and makes an alert visible
     */
    $("input[name='director']").blur(function(){
        if (this.value!="" && validDirector(this.value)) {
            $(this).removeClass("border-danger").addClass("border-success");
            $("#directorPreview").html($(this).val());
            $("#directorMobilePreview").html($(this).val());
        }
        else if (this.value=="") {
            $(this).removeClass("border-success border-danger");
            $("#directorPreview").empty();
            $("#directorMobilePreview").empty();
        }
        else {
            $(this).removeClass("border-success").addClass("border-danger");
            $("#directorPreview").empty();
            $("#directorMobilePreview").empty();
            $("#directorError").removeClass("d-none").addClass("show");
            $("#directorError span:last-child").remove();
            $("#directorError").append("<span>Error en el director introducido.</span>");
        }
    });

    /**
     * This function validates the poster value, if it's valid the function includes the value in the preview, if i'ts not it deletes it from the preview and makes an alert visible
     */
    $("input[name='poster']").blur(function(){
        if (this.value!="" && validURL(this.value)) {
            $(this).removeClass("border-danger").addClass("border-success");
            $("#posterPreview").attr({
                src: $(this).val(),
                alt: "Portada "+$("input[name='title']").val()
            });
            $("#posterMobilePreview").attr({
                src: $(this).val(),
                alt: "Portada "+$("input[name='title']").val()
            });
        }
        else if (this.value=="") {
            $(this).removeClass("border-success border-danger");
            $("#posterPreview").attr({
                src: "",
                alt: ""
            });
            $("#posterMobilePreview").attr({
                src: "",
                alt: ""
            });
        }
        else {
            $(this).removeClass("border-success").addClass("border-danger");
            $("#posterPreview").attr({
                src: "",
                alt: ""
            });
            $("#posterMobilePreview").attr({
                src: "",
                alt: ""
            });
            $("#posterError").removeClass("d-none").addClass("show");
            $("#posterError span:last-child").remove();
            $("#posterError").append("<span>Error en la URL del póster.</span>");
        }
    });

    /**
     * This function validates the duration value, if it's valid the function includes the value in the preview, if i'ts not it deletes it from the preview and makes an alert visible
     */
    $("input[name='duration']").blur(function(){
        if (this.value!="" && validDuration(this.value)) {
            $(this).removeClass("border-danger").addClass("border-success");
            $("#durationPreview").html($(this).val());
            $("#durationMobilePreview").html($(this).val());
            $("#secondSeparator").removeClass("d-none");
            $("#secondMobileSeparator").removeClass("d-none");
        }
        else if (this.value=="") {
            $(this).removeClass("border-success border-danger");
            $("#durationPreview").empty();
            $("#durationMobilePreview").empty();
        }
        else {
            $(this).removeClass("border-success").addClass("border-danger");
            $("#durationPreview").empty();
            $("#durationMobilePreview").empty();
            $("#secondSeparator").addClass("d-none");
            $("#secondMobileSeparator").addClass("d-none");
            $("#durationError").removeClass("d-none").addClass("show");
            $("#durationError span:last-child").remove();
            $("#durationError").append("<span>Error en el formato de duración. El formato es: Xh XYmin</span>");
        }
    });

    /**
     * This function validates the trailer value, if it's valid the function includes the value in the preview, if i'ts not it deletes it from the preview and makes an alert visible
     */
    $("input[name='trailer']").blur(function(){
        if (this.value!="" && validYouTubeID(this.value)) {
            $(this).removeClass("border-danger").addClass("border-success");
            $("#modalTitlePreview").html(
                "Tráiler \""+$("input[name='title']").val()+"\" ("+$("input[name='year']").val()+")"
            );
            $("#trailerPreview").attr('src','https://www.youtube.com/embed/'+$(this).val());
            $(".trailerBtn").removeClass("d-none");
        }
        else if (this.value=="") {
            $(this).removeClass("border-success border-danger");
            $(".trailerBtn").addClass("d-none");
        }
        else {
            $(this).removeClass("border-success").addClass("border-danger");
            $("#trailerError").removeClass("d-none").addClass("show");
            $("#trailerError span:last-child").remove();
            $("#trailerError").append("<span>Se necesita la id del vídeo en YouTube.</span>");
            $(".trailerBtn").addClass("d-none");
        }
    });

    /**
     * This function validates the genre value, if it's valid the function includes the value in the preview, if i'ts not it deletes it from the preview and makes an alert visible
     */

    var genres = {'action':'Acción','animation':'Animación','adventure':'Aventuras','biography':'Biográfica','war':'Bélica','sci-fi':'Ciencia ficción','comedy':'Comedia','documentary':'Documental','drama':'Drama','family':'Familiar','fantasy':'Fantasía','musical':'Musical','thriller':'Suspense','horror':'Terror','western':'Western'}
    $("select[name='genre']").blur(function(){
        if (this.value!="Selecciona un género") {
            $(this).removeClass("border-danger").addClass("border-success");
            $("#genrePreview").html(genres[$(this).val()]);
            $("#genreMobilePreview").html(genres[$(this).val()]);
        }
        else {
            $(this).removeClass("border-success").addClass("border-danger");
            $("#genrePreview").empty();
            $("#genreMobilePreview").empty();
            $("#genreError").removeClass("d-none").addClass("show");
            $("#genreError span:last-child").remove();
            $("#genreError").append("<span>Se necesita el género de la película.</span>");
        }
    });

    /**
     * This function validates the synopsis value, if it's valid the function includes the value in the preview, if i'ts not it deletes it from the preview and makes an alert visible
     */
    $("#synopsis").blur(function(){
        if (this.value!="" && validSynopsis(this.value)) {
            $(this).removeClass("border-danger").addClass("border-success");
            $("#synopsisPreview").html($(this).val());
        }
        else if (this.value=="") {
            $(this).removeClass("border-success border-danger");
            $("#synopsisPreview").empty();
        }
        else {
            $(this).removeClass("border-success").addClass("border-danger");
            $("#synopsisPreview").empty();
            $("#synopsisError").removeClass("d-none").addClass("show");
            $("#synopsisError span:last-child").remove();
            $("#synopsisError").append("<span>Error en la sinópsis.</span>");
        }
    });

    /**
     * This function validates the first rating value, if it's valid the function includes the value in the preview, if i'ts not it deletes it from the preview and makes an alert visible
     */
    $("input[name='ratings[0][value]']").blur(function() {
        if (this.value!="" && validTenRating(this.value)) {
            $(this).removeClass("border-danger").addClass("border-success");
            $("#imdbRatingPreview").html("<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/6/69/IMDB_Logo_2016.svg/1920px-IMDB_Logo_2016.svg.png' alt='Logo IMDb' width='30'><span> · "+$(this).val()+"/10</span>");
            $("#imdbRatingMobilePreview").html("<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/6/69/IMDB_Logo_2016.svg/1920px-IMDB_Logo_2016.svg.png' alt='Logo IMDb' width='30'><span> · "+$(this).val()+"/10</span>");
        }
        else if (this.value=="") {
            $(this).removeClass("border-success border-danger");
            $("#imdbRatingPreview").empty();
            $("#imdbRatingMobilePreview").empty();
        }
        else {
            $(this).removeClass("border-success").addClass("border-danger");
            $("#imdbRatingPreview").empty();
            $("#imdbRatingMobilePreview").empty();
            $("#ratingsError").removeClass("d-none").addClass("show");
            $("#ratingsErrorMessage").empty();
            $("#ratingsErrorMessage").append("<span>Error en una o varias de las puntuaciones.</span>");
        }
    });

    /**
     * This function validates the second rating value, if it's valid the function includes the value in the preview, if i'ts not it deletes it from the preview and makes an alert visible
     */
    $("input[name='ratings[1][value]']").blur(function() {
        if (this.value!="" && validTenRating(this.value)) {
            $(this).removeClass("border-danger").addClass("border-success");
            $("#filmaffinityRatingPreview").html("<img src='http://www.filmaffinity.com/images/logo4.png' alt='Logo FilmAffinity' width='30'><span> · "+$(this).val()+"/10</span>");
            $("#filmaffinityRatingMobilePreview").html("<img src='http://www.filmaffinity.com/images/logo4.png' alt='Logo FilmAffinity' width='30'><span> · "+$(this).val()+"/10</span>");
        }
        else if (this.value=="") {
            $(this).removeClass("border-success border-danger");
            $("#filmaffinityRatingPreview").empty();
            $("#filmaffinityRatingMobilePreview").empty();
        }
        else {
            $(this).removeClass("border-success").addClass("border-danger");
            $("#filmaffinityRatingPreview").empty();
            $("#filmaffinityRatingMobilePreview").empty();
            $("#ratingsError").removeClass("d-none").addClass("show");
            $("#ratingsErrorMessage").empty();
            $("#ratingsErrorMessage").append("<span>Error en una o varias de las puntuaciones.</span>");
        }
    });

    /**
     * This function validates the third rating value, if it's valid the function includes the value in the preview, if i'ts not it deletes it from the preview and makes an alert visible
     */
    $("input[name='ratings[2][value]']").blur(function() {
        if (this.value!="" && validHundredRating(this.value)) {
            $(this).removeClass("border-danger").addClass("border-success");
            $("#rottentomatoesRatingPreview").html("<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Rotten_Tomatoes_logo.svg/1024px-Rotten_Tomatoes_logo.svg.png' alt='Logo Rotten Tomatoes' width='30'><span> · "+$(this).val()+"%</span>");
            $("#rottentomatoesRatingMobilePreview").html("<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Rotten_Tomatoes_logo.svg/1024px-Rotten_Tomatoes_logo.svg.png' alt='Logo Rotten Tomatoes' width='30'><span> · "+$(this).val()+"%</span>");
        }
        else if (this.value=="") {
            $(this).removeClass("border-success border-danger");
            $("#rottentomatoesRatingPreview").empty();
            $("#rottentomatoesRatingMobilePreview").empty();
        }
        else {
            $(this).removeClass("border-success").addClass("border-danger");
            $("#rottentomatoesRatingPreview").empty();
            $("#rottentomatoesRatingMobilePreview").empty();
            $("#ratingsError").removeClass("d-none").addClass("show");
            $("#ratingsErrorMessage").empty();
            $("#ratingsErrorMessage").append("<span>Error en una o varias de las puntuaciones.</span>");
        }
    });

    /**
     * The following functions hide the alerts that input's value can create
     */
    $("#titleErrorBtn").click(function() {
        $("#titleError").removeClass("show").addClass("d-none");
    });

    $("#yearErrorBtn").click(function() {
        $("#yearError").removeClass("show").addClass("d-none");
    });

    $("#durationErrorBtn").click(function() {
        $("#durationError").removeClass("show").addClass("d-none");
    });

    $("#directorErrorBtn").click(function() {
        $("#directorError").removeClass("show").addClass("d-none");
    });

    $("#posterErrorBtn").click(function() {
        $("#posterError").removeClass("show").addClass("d-none");
    });

    $("#trailerErrorBtn").click(function() {
        $("#trailerError").removeClass("show").addClass("d-none");
    });

    $("#genreErrorBtn").click(function() {
        $("#genreError").removeClass("show").addClass("d-none");
    });

    $("#synopsisErrorBtn").click(function() {
        $("#synopsisError").removeClass("show").addClass("d-none");
    });

    $("#ratingsErrorBtn").click(function() {
        $("#ratingsError").removeClass("show").addClass("d-none");
    });

    $("#errorSubmittingBtn").click(function() {
        $("#errorSubmitting").removeClass("show").addClass("d-none");
    });

    /**
     * This function gives an example image for how to get the ID from a YouTube video in a Bootstrap popover
     */
    $('[data-toggle="popover"]').popover({
        placement : 'top',
        html : true,
        content : '<img src="https://camo.githubusercontent.com/dc3c8a27f941982d8938ce8be4199fa4e914d1ba/687474703a2f2f692e696d6775722e636f6d2f4f6c776b3463722e706e67" class="img-fluid" alt="Youtube video ID">'
    });

    /**
     * This function fills the carousel with the images that the scrreenshot inputs have after validation
     */
    function fillCarousel() {
        indicators.innerHTML = "";
        innerCarousel.innerHTML = "";
        mobileIndicators.innerHTML = "";
        mobileInnerCarousel.innerHTML = "";

        urls = [];

        var inputs = document.getElementsByName("screenshots[]");

        for (var i=0; i<inputs.length; i++) {
            if (!urls.includes(inputs[i].value) && inputs[i].value != "" && validURL(inputs[i].value)) {
                inputs[i].classList.remove("border-danger")
                inputs[i].classList.add("border-success");
                urls.push(inputs[i].value);
            }
            else {
                inputs[i].classList.remove("border-success")
                inputs[i].classList.add("border-danger");
            }
        }

        for (var i=0; i<urls.length; i++) {
            var cIndicator = document.createElement("LI");
            cIndicator.setAttribute("data-target", "#screenshotsPreview");
            cIndicator.setAttribute("data-slide-to", screenshotNum);
            var cInner = document.createElement("DIV");
            cInner.classList.add("carousel-item");
            var cImg = document.createElement("IMG");
            cImg.src = inputs[i].value;
            cImg.classList.add("img-fluid");
            cImg.alt = "screenshot"+(screenshotNum+1);
            cImg.width = "1920";
            cImg.height = "1080";
            cInner.appendChild(cImg);
            if (i==0) {
                cIndicator.classList.add("active");
                cInner.classList.add("active");
                indicators.appendChild(cIndicator);
                innerCarousel.appendChild(cInner);
                screenshotNum++;
            }
            else {
                indicators.appendChild(cIndicator);
                innerCarousel.appendChild(cInner);
                screenshotNum++;
            }
        }

        for (var i=0; i<urls.length; i++) {
            var cIndicator = document.createElement("LI");
            cIndicator.setAttribute("data-target", "#screenshotsMobilePreview");
            cIndicator.setAttribute("data-slide-to", screenshotNum);
            var cInner = document.createElement("DIV");
            cInner.classList.add("carousel-item");
            var cImg = document.createElement("IMG");
            cImg.src = inputs[i].value;
            cImg.classList.add("img-fluid");
            cImg.alt = "screenshot"+(screenshotNum+1);
            cImg.width = "1920";
            cImg.height = "1080";
            cInner.appendChild(cImg);
            if (i==0) {
                cIndicator.classList.add("active");
                cInner.classList.add("active");
                mobileIndicators.appendChild(cIndicator);
                mobileInnerCarousel.appendChild(cInner);
            }
            else {
                mobileIndicators.appendChild(cIndicator);
                mobileInnerCarousel.appendChild(cInner);
            }
        }
    }
    
    /**
     * This function stops and resets the video embeded in the modal when it's closed
     */
    $("#movieTrailer").on('hidden.bs.modal', function (e) {
        $("#movieTrailer iframe").attr("src", $("#movieTrailer iframe").attr("src"));
    });
    
    /**
     * The following functions are the validation ones
     */
    function validYouTubeID(YTid) {
        var regexp = /^[^"&?\/\s]{11}$/;
        return regexp.test(YTid);
    }

    function validURL(url) {
        var regexp = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
        return regexp.test(url);
    }

    function validDuration(runtime) {
        var regexp = /^\d{1}h \d{2}min$/;
        return regexp.test(runtime);
    }

    function validDirector(directors) {
        var regexp = /^[A-z ,\.'ÁáÉéÍíÓóÚúÑñÀàÈèÌìÒòÙù]{0,100}$/;
        return regexp.test(directors);
    }

    function validTitle(movieTitle) {
        var regexp = /^[\w ,':\-¿?¡!ÁáÉéÍíÓóÚúÑñÀàÈèÌìÒòÙù]{0,100}$/;
        return regexp.test(movieTitle);
    }

    function validYear(movieYear) {
        var regexp = /^\d{4}$/;
        var actualYear = new Date().getFullYear();
        if (regexp.test(movieYear) && movieYear>=1833 && movieYear<=actualYear) {
            return true;
        }
        else {
            return false;
        }
    }

    function validSynopsis(movieSynopsis) {
        var regexp = /^[\w .,;:"\-()'?¿!¡ÁáÉéÍíÓóÚúÑñÀàÈèÌìÒòÙù]{0,1000}$/gm;
        return regexp.test(movieSynopsis);
    }

    function validTenRating(rating) {
        var regexp = /^\d{0,2}(\.\d?)?$/;
        if (rating>=0 && rating<=10 && regexp.test(rating)) {
            return true;
        }
        else {
            return false;
        }
    }

    function validHundredRating(rating) {
        var regexp = /^\d{1,3}$/;
        if (rating>=0 && rating<=100 && regexp.test(rating)) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
    * This function validates the whole form before submitting, if there's errors it shows up an alert with a list of the errors found in the form, if there's no errors the form is submitted
    */
    $("form").submit(function() {
        var validGenre = false;
        var inputs = document.getElementsByName("screenshots[]");
        var validRatings = false;
        var screenshotsArray = [];
        var validScreenshots = false;

        if ($("select[name='genre']").val()!="Selecciona un género") {
            validGenre = true;
        }
        else {
            validGenre = false;
        }

        var imdb = $("input[name='ratings[0][value]']").val();
        var filmaffinity = $("input[name='ratings[1][value]']").val();
        var rottentomatoes = $("input[name='ratings[2][value]']").val();

        if ((imdb!="" && validTenRating(imdb)) && (filmaffinity=="" || validTenRating(filmaffinity)) && (rottentomatoes=="" || validHundredRating(rottentomatoes))) {
            validRatings = true;
        }

        for (var i=0; i<inputs.length; i++) {
            if (validURL(inputs[i].value)) {
                screenshotsArray.push("true");
            }
            else {
                screenshotsArray.push("false");
            }
        }

        if (!screenshotsArray.includes("false")) {
            validScreenshots = true;
        }
        else {
            validScreenshots = false;
        }

        if (validTitle($("input[name='title']").val()) && validYear($("input[name='year']").val()) && validDirector($("input[name='director']").val()) && 
        validURL($("input[name='poster']").val()) && validDuration($("input[name='duration']").val()) && validYouTubeID($("input[name='trailer']").val()) &&
        validSynopsis($("#synopsis").val()) && validGenre && validRatings && validScreenshots) {
            return true;
        }
        else {
            $("#submitErrorMessage").empty();
            $("#submitErrorMessage").append("<p><strong>Hay errores en los siguientes campos:</strong></p>");
            var errorsList = $("<ul></ul>");
            if (!validTitle($("input[name='title']").val())) {
                errorsList.append("<li>Error en el título de la película</li>");
            }
            if (!validYear($("input[name='year']").val())) {
                errorsList.append("<li>Error en el año de la película</li>");
            }
            if (!validDirector($("input[name='director']").val())) {
                errorsList.append("<li>Error en el nombre del director/a/es</li>");
            }
            if (!validURL($("input[name='poster']").val())) {
                errorsList.append("<li>Error en el enlace del póster</li>");
            }
            if (!validDuration($("input[name='duration']").val())) {
                errorsList.append("<li>Error en el formato de la duración</li>");
            }
            if (!validYouTubeID($("input[name='trailer']").val())) {
                errorsList.append("<li>Error en la ID del tráiler en YouTube</li>");
            }
            if (!validGenre) {
                errorsList.append("<li>Error en el género de la película</li>");
            }
            if (!validSynopsis($("#synopsis").val())) {
                errorsList.append("<li>Error en la sinópsis</li>");
            }
            if (!validRatings) {
                errorsList.append("<li>Error en el formato de las puntuaciones</li>");
            }
            if (!validScreenshots) {
                errorsList.append("<li>Error en las url de las fotos de la película</li>");
            }
            $("#submitErrorMessage").append(errorsList);
            $("#errorSubmitting").removeClass("d-none").addClass("show");
            return false;
        }
    });
});
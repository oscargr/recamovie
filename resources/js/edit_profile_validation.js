$(document).ready(function() {

    /**
     * Here we retrieve the old name to set that name in the input in case the user leaves the name field with a blank space
     */

    var oldName = $("input[name='oldName']").val();

    /**
     * The following functions are the validation ones
     */

    function validImage(image) {
        var extension = image.substring(image.lastIndexOf('.') + 1).toLowerCase();
        if (extension=="gif" || extension=="jpg" || extension=="png") {
            return true;
        }
        else {
            return false;
        }
    }

    function validName(name) {
        var regexp = /^[\w ,\.':\-¿?¡!ÁáÉéÍíÓóÚúÑñÀàÈèÌìÒòÙù]{0,100}$/;
        return regexp.test(name);
    }

    function validAboutme(aboutme) {
        var regexp = /^[\w \.,;:"\-()'?¿!¡ÁáÉéÍíÓóÚúÑñÀàÈèÌìÒòÙù]{0,280}$/gm;
        return regexp.test(aboutme)
    }

    /**
     * This function validates the name value, if it's not valid it shows an error alert.
     */
    $("input[name='name']").blur(function(){
        if (this.value!="" && validName(this.value)) {
            $(this).removeClass("border-danger").addClass("border-success");
            $("#nameStatus").html('<i class="fas fa-check-square text-success"></i>');
        }
        else if (this.value=="") {
            $(this).removeClass("border-success border-danger");
            $("#nameStatus").empty();
            this.value = oldName;
        }
        else {
            $(this).removeClass("border-success").addClass("border-danger");
            $("#nameStatus").html('<i class="fas fa-window-close text-danger"></i>');
            $("#nameError").removeClass("d-none").addClass("show");
            $("#nameError span:last-child").remove();
            $("#nameError").append("<span>Error en el formato del titular.</span>");
        }
    });

    /**
     * This function validates the about me value, if it's not valid it makes an error alert visible
     */
    $("#about_me").blur(function(){
        if (this.value!="" && validAboutme(this.value)) {
            $(this).removeClass("border-danger").addClass("border-success");
            $("#aboutmeStatus").html('<i class="fas fa-check-square text-success"></i>');
        }
        else if (this.value=="") {
            $(this).removeClass("border-success border-danger");
            $("#aboutmeStatus").empty();
        }
        else {
            $(this).removeClass("border-success").addClass("border-danger");
            $("#aboutmeStatus").html('<i class="fas fa-window-close text-danger"></i>');
            $("#aboutmeError").removeClass("d-none").addClass("show");
            $("#aboutmeError span:last-child").remove();
            $("#aboutmeError").append("<span>Error en el campo sobre mí.</span>");
        }
    });

    /**
     * This function validates the profile pic file, if it's not valid it shows an error alert.
     */

    $("#profile_pic").change(function() {

        var val = $(this).val();
    
        if (validImage(val) && this.files[0].size <= 10485760) {
            $("#profilePicStatus").html('<i class="fas fa-check-square text-success"></i>');
        }
        else if (this.files[0].size > 10485760) {
            $("#profilePicStatus").html('<i class="fas fa-window-close text-danger"></i>');
            $("#profilePicError").removeClass("d-none").addClass("show");
            $("#profilePicError span:last-child").remove();
            $("#profilePicError").append("<span>¡Error! El tamaño del archivo es superior a 10MB.</span>");
        }
        else {
            $("#profilePicStatus").html('<i class="fas fa-window-close text-danger"></i>');
            $("#profilePicError").removeClass("d-none").addClass("show");
            $("#profilePicError span:last-child").remove();
            $("#profilePicError").append("<span>¡Error! El archivo no es una imagen.</span>");
        }

    });

    /**
     * This function validates the banner pic file, if it's not valid it shows an error alert.
     */

    $("#banner_pic").change(function() {

        var val = $(this).val();
    
        if (validImage(val) && this.files[0].size <= 10485760) {
            $("#bannerPicStatus").html('<i class="fas fa-check-square text-success"></i>');
        }
        else if (this.files[0].size > 10485760) {
            $("#bannerPicStatus").html('<i class="fas fa-window-close text-danger"></i>');
            $("#bannerPicError").removeClass("d-none").addClass("show");
            $("#bannerPicError span:last-child").remove();
            $("#bannerPicError").append("<span>¡Error! El tamaño del archivo es superior a 10MB.</span>");
        }
        else {
            $("#bannerPicStatus").html('<i class="fas fa-window-close text-danger"></i>');
            $("#bannerPicError").removeClass("d-none").addClass("show");
            $("#bannerPicError span:last-child").remove();
            $("#bannerPicError").append("<span>¡Error! El archivo no es una imagen.</span>");
        }

    });

    /**
    * This function validates the whole form before submitting, if there's errors it shows up an alert with a list of the errors found in the form, if there's no errors the form is submitted
    */
    $("form").submit(function() {

        if ( validName($("input[name='name']").val()) && validAboutme($("#about_me").val()) ) {
            return true;
        }
        else {
            $("#submitErrorMessage").empty();
            $("#submitErrorMessage").append("<p><strong>Hay errores en los siguientes campos:</strong></p>");
            var errorsList = $("<ul></ul>");
            if (!validName($("input[name='name']").val())) {
                errorsList.append("<li>Error en el nombre</li>");
            }
            if (!validAboutme($("#about_me").val())) {
                errorsList.append("<li>Error en el campo sobre mí (error en el formato)</li>");
            }
            $("#submitErrorMessage").append(errorsList);
            $("#errorSubmitting").removeClass("d-none").addClass("show");
            return false;
        }

    });

    /**
     * The following functions hide the alerts that input's value can create
     */
    $("#nameErrorBtn").click(function() {
        $("#nameError").removeClass("show").addClass("d-none");
    });

    $("#aboutmeErrorBtn").click(function() {
        $("#aboutmeError").removeClass("show").addClass("d-none");
    });

    $("#profilePicErrorBtn").click(function() {
        $("#profilePicError").removeClass("show").addClass("d-none");
    });

    $("#bannerPicErrorBtn").click(function() {
        $("#bannerPicError").removeClass("show").addClass("d-none");
    });

    $("#errorSubmittingBtn").click(function() {
        $("#errorSubmitting").removeClass("show").addClass("d-none");
    });

});
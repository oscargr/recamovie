$(document).ready(function() {

    /**
     * The following functions are the validation ones
     */

    function validHeadline(headline) {
        var regexp = /^[\w ,\.':\-¿?¡!ÁáÉéÍíÓóÚúÑñÀàÈèÌìÒòÙù]{0,100}$/;
        return regexp.test(headline);
    }

    function validReview(review) {
        var regexp = /^[\w \.,;:"\-()'?¿!¡ÁáÉéÍíÓóÚúÑñÀàÈèÌìÒòÙù]{0,1000}$/gm;
        if (regexp.test(review) && review!="") {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * This function validates the headline value, if it's not valid it shows an error alert.
     */
    $("input[name='headline']").blur(function(){
        if (this.value!="" && validHeadline(this.value)) {
            $(this).removeClass("border-danger").addClass("border-success");
            $("#headlineStatus").html('<i class="fas fa-check-square text-success"></i>');
        }
        else if (this.value=="") {
            $(this).removeClass("border-success border-danger");
            $("#headlineStatus").empty();
        }
        else {
            $(this).removeClass("border-success").addClass("border-danger");
            $("#headlineStatus").html('<i class="fas fa-window-close text-danger"></i>');
            $("#headlineError").removeClass("d-none").addClass("show");
            $("#headlineError span:last-child").remove();
            $("#headlineError").append("<span>Error en el formato del titular.</span>");
        }
    });

    /**
     * This function validates the review value, if it's not valid it makes an error alert visible
     */
    $("#review").blur(function(){
        if (this.value!="" && validReview(this.value)) {
            $(this).removeClass("border-danger").addClass("border-success");
            $("#reviewStatus").html('<i class="fas fa-check-square text-success"></i>');
        }
        else if (this.value=="") {
            $(this).removeClass("border-success border-danger").addClass("border-danger");
            $("#reviewStatus").html('<i class="fas fa-window-close text-danger"></i>');
            $("#reviewError").removeClass("d-none").addClass("show");
            $("#reviewError span:last-child").remove();
            $("#reviewError").append("<span>El campo de la reseña es un campo obligatorio.</span>");
        }
        else {
            $(this).removeClass("border-success").addClass("border-danger");
            $("#reviewStatus").html('<i class="fas fa-window-close text-danger"></i>');
            $("#reviewError").removeClass("d-none").addClass("show");
            $("#reviewError span:last-child").remove();
            $("#reviewError").append("<span>Error en la reseña.</span>");
        }
    });

    /**
    * This function validates the whole form before submitting, if there's errors it shows up an alert with a list of the errors found in the form, if there's no errors the form is submitted
    */
    $("form").submit(function() {

        if (validHeadline($("input[name='headline']").val()) && validReview($("#review").val())) {
            return true;
        }
        else {
            $("#submitErrorMessage").empty();
            $("#submitErrorMessage").append("<p><strong>Hay errores en los siguientes campos:</strong></p>");
            var errorsList = $("<ul></ul>");
            if (!validHeadline($("input[name='headline']").val())) {
                errorsList.append("<li>Error en el titular</li>");
            }
            if (!validReview($("#review").val())) {
                errorsList.append("<li>Error en la reseña (campo sin rellenar o error en el formato)</li>");
            }
            $("#submitErrorMessage").append(errorsList);
            $("#errorSubmitting").removeClass("d-none").addClass("show");
            return false;
        }

    });

    /**
     * The following functions hide the alerts that input's value can create
     */
    $("#headlineErrorBtn").click(function() {
        $("#headlineError").removeClass("show").addClass("d-none");
    });

    $("#reviewErrorBtn").click(function() {
        $("#reviewError").removeClass("show").addClass("d-none");
    });

    $("#errorSubmittingBtn").click(function() {
        $("#errorSubmitting").removeClass("show").addClass("d-none");
    });

});
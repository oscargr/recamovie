<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Esas credenciales no existen en nuestra base de datos.',
    'password' => 'La contraseña es incorrecta.',
    'throttle' => 'Demasiados intentos de login. Inténtelo de nuevo en :seconds segundos.',

];

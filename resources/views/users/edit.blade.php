@extends('layouts.app')

@section('content')

<div class="container-fluid h-100">
    <div id="errorSubmitting" class="alert alert-danger alert-dismissible d-none fade mt-5 fixed-top w-75 mx-auto">
        <button id="errorSubmittingBtn" type="button" class="close">&times;</button>
        <div id="submitErrorMessage">
        </div>
    </div>
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show mt-3">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>¡Error!</strong> {{$errors->first()}}
    </div>
    @endif
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-md-8">
            <div class="card bg-dark text-white">
                <div class="card-header">Edita tu perfil {{ $user->name }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ action('App\Http\Controllers\UsersController@update'), $user->id }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <div class="form-group row">
                            <label for="name" class="col-lg-4 col-form-label text-lg-right"><span class="mr-2" id="nameStatus"></span>Nombre</label>

                            <div class="col-lg-6">
                                <input type="hidden" name="oldName" value="{{$user->name}}">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}" placeholder="{{ $user->name }}" autocomplete="name" autofocus>
                                <div id="nameError" class="alert alert-danger alert-dismissible d-none fade mt-1">
                                    <button id="nameErrorBtn" type="button" class="close">&times;</button>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="about_me" class="col-lg-4 col-form-label text-lg-right"><span class="mr-2" id="aboutmeStatus"></span>Sobre mí</label>

                            <div class="col-lg-6">
                                <textarea style="resize: none;" id="about_me" type="text" class="form-control" name="about_me" rows="3" maxlength="280" placeholder="¿Qué quieres que los otros usuarios sepan de ti? (max: 280 caracteres)">{{ $user->about_me }}</textarea>
                                <div id="aboutmeError" class="alert alert-danger alert-dismissible d-none fade mt-1">
                                    <button id="aboutmeErrorBtn" type="button" class="close">&times;</button>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="profile_pic" class="col-lg-4 col-form-label text-lg-right"><span class="mr-2" id="profilePicStatus"></span>Foto de perfil</label>

                            <div class="col-lg-6">
                                <div class="custom-file mb-3">
                                    <input type="file" class="custom-file-input" id="profile_pic" name="profile_pic" accept="image/*">
                                    <label class="custom-file-label" for="profile_pic">Selecciona una imagen</label>
                                    <div id="profilePicError" class="alert alert-danger alert-dismissible d-none fade mt-1">
                                        <button id="profilePicErrorBtn" type="button" class="close">&times;</button>
                                    </div>
                                </div>
                                @if (File::exists('storage/'.$user->profile_pic))
                                <button type="button" class="btn btn-danger my-2 deleteBtn" data-pic="profile">
                                    <i class="fas fa-trash mr-1"></i>Eliminar tu foto de perfil
                                </button>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="banner_pic" class="col-lg-4 col-form-label text-lg-right"><span class="mr-2" id="bannerPicStatus"></span>Foto del banner</label>

                            <div class="col-lg-6">
                                <div class="custom-file mb-3">
                                    <input type="file" class="custom-file-input" id="banner_pic" name="banner_pic" accept="image/*">
                                    <label class="custom-file-label" for="banner_pic">Selecciona una imagen</label>
                                    <div id="bannerPicError" class="alert alert-danger alert-dismissible d-none fade mt-1">
                                        <button id="bannerPicErrorBtn" type="button" class="close">&times;</button>
                                    </div>
                                </div>
                                @if (File::exists('storage/'.$user->banner_pic))
                                <button type="button" class="btn btn-danger my-2 deleteBtn" data-pic="banner">
                                    <i class="fas fa-trash mr-1"></i>Eliminar tu foto del banner
                                </button>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-success my-2">
                                    <i class="fas fa-save mr-1"></i>Guardar cambios
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- User images delete confirmation modal -->
<div class="modal fade" id="deleteConfirmation">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content bg-dark text-white">
        
        <div class="modal-header border-secondary">
            <h4 class="modal-title">Estás apunto de eliminar tu foto de perfil y banner</h4>
            <button type="button" class="close text-white" data-dismiss="modal">×</button>
        </div>
        
        <div class="modal-body">
            <h4 class="text-center" id="question"></h4>
            <form action="{{action('App\Http\Controllers\UsersController@delete')}}" method="POST">
                @csrf
                @method('DELETE')
                <input type="hidden" name="picture">
                <div class="row">
                    <div class="col">
                        <button type="button" class="btn btn-danger btn-block" data-dismiss="modal"><i class="fas fa-times-circle mr-1"></i>Cancelar</button>
                    </div>
                    <div class="col">
                        <button type="submit" class="btn btn-success btn-block"><i class="fas fa-trash mr-1"></i>Confirmar</button>
                    </div>
                </div>
            </form>
        </div>
        
        </div>
    </div>
</div>

<script src="{{ asset('js/edit_profile_validation.js') }}" defer></script>

<script>

$(document).ready(function(){

    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    $(".deleteBtn").click(function() {
        var picture = $(this).data("pic");
        if (picture == "profile") {
            $("#question").html("¿Quieres eliminar tu foto de perfil?");
        }
        else {
            $("#question").html("¿Quieres eliminar tu foto de banner?");
        }
        $('input[name=picture]').val(picture);
        $("#deleteConfirmation").modal();
    });

});

</script>

@stop
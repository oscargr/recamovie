@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <h2 class="text-center w-50 mx-auto text-white mt-3 py-3" style="background: url(<?php echo asset('storage/bg_images/pincel.png') ?>) no-repeat center center; background-size: cover;">Lista de seguidores</h2>
    <div class="row">
        <div class="col m-3 bg-gradient text-white">
            <div class="d-flex flex-row justify-content-around flex-wrap">
            @foreach($followers as $user)
            @if (Storage::exists('user_images/'.$user->id.'/profile_pic/User_'.$user->id.'_profile_pic.png') || Storage::exists('user_images/'.$user->id.'/profile_pic/User_'.$user->id.'_profile_pic.jpg') || Storage::exists('user_images/'.$user->id.'/profile_pic/User_'.$user->id.'_profile_pic.gif'))
                <div class="py-3 mx-2" style="width: 300px;">
                    <a href="{{url('user/'.$user->id)}}" class="text-white">
                        <img src="{{ asset('storage/'.$user->profile_pic) }}" alt="Foto de perfil de {{ $user->name }}" width="50" height="50" class="mb-2 img-thumbnail rounded-circle">
                        <span>{{$user->name}}<i class="fas fa-external-link-square-alt ml-1"></i></span>
                    </a>
                </div>
            @else
                <div class="py-3 mx-2" style="width: 300px;">
                    <a href="{{url('user/'.$user->id)}}" class="text-white">
                        <img src="{{ asset('storage/images/default/Default_profile_pic.png') }}" alt="Foto de perfil de {{ $user->name }}" width="50" height="50" class="mb-2 img-thumbnail rounded-circle">
                        <span>{{$user->name}}<i class="fas fa-external-link-square-alt ml-1"></i></span>
                    </a>
                </div>
            @endif
            @endforeach
            </div>
            {{-- Pagination --}}
            <div class="d-flex justify-content-center mt-4">
                {!! $followers->links() !!}
            </div>
        </div>
    </div>
</div>

@stop
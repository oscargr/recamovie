@extends('layouts.app')

@section('content')

@php
$recommendationslength = sizeof($userRecommendations);
@endphp

<div class="container-fluid px-0">
    <div class="bg-white overflow-hidden">
        @if (Storage::exists('user_images/'.$user->id.'/banner_pic/User_'.$user->id.'_banner_pic.png') || Storage::exists('user_images/'.$user->id.'/banner_pic/User_'.$user->id.'_banner_pic.jpg') || Storage::exists('user_images/'.$user->id.'/banner_pic/User_'.$user->id.'_banner_pic.gif'))
        <div class="px-4 pt-0 pb-4 bg-dark" style='background: no-repeat center center; background-size: cover; background-image: linear-gradient(to top, rgba(41, 43, 44, 1), rgba(41, 43, 44, 0.3)), url("{{asset('storage/'.$user->banner_pic)}}");'>
        @else
        <div class="px-4 pt-0 pb-4 bg-dark" style='background: no-repeat center center; background-size: cover; background-image: linear-gradient(to top, rgba(41, 43, 44, 1), rgba(41, 43, 44, 0.3)), url("{{asset('storage/images/default/Default_profile_background.jpg')}}");'>
        @endif
            <div class="media align-items-end" style="transform: translateY(5rem);">
                @if (Storage::exists('user_images/'.$user->id.'/profile_pic/User_'.$user->id.'_profile_pic.png') || Storage::exists('user_images/'.$user->id.'/profile_pic/User_'.$user->id.'_profile_pic.jpg') || Storage::exists('user_images/'.$user->id.'/profile_pic/User_'.$user->id.'_profile_pic.gif'))
                <div class="profile mr-3"><img src="{{ asset('storage/'.$user->profile_pic) }}" alt="Foto de perfil de {{ $user->name }}" width="200" class="rounded mb-2 img-thumbnail">
                @else
                <div class="profile mr-3"><img src="{{ asset('storage/images/default/Default_profile_pic.png') }}" alt="Foto de perfil de {{ $user->name }}" width="200" class="rounded mb-2 img-thumbnail">
                @endif
                    @if (Auth::user()->id == $user->id)
                    <a href="{{ url('user/edit/'.$user->id) }}" class="btn btn-warning btn-sm btn-block"><i class="fas fa-user-edit mr-1"></i>Editar perfil</a>
                    @elseif(!$subscribed)
                    <form action="{{ action('App\Http\Controllers\UsersController@followUser') }}" method="POST">
                    @csrf
                    @php 
                    $cryptedCreatorId = Crypt::encryptString($user->id);
                    $cryptedSubscriberId = Crypt::encryptString(Auth::user()->id);
                    @endphp
                        <input type="hidden" name="creatorId" value="{{$cryptedCreatorId}}">
                        <input type="hidden" name="subscriberId" value="{{$cryptedSubscriberId}}">
                        <button type="submit" class="btn btn-primary btn-sm btn-block"><i class="fas fa-user-plus mr-1"></i>Seguir</button>
                    </form>
                    @else
                    <form action="{{ action('App\Http\Controllers\UsersController@unfollowUser') }}" method="POST">
                    @csrf
                    @method('DELETE')
                    @php 
                    $cryptedCreatorId = Crypt::encryptString($user->id);
                    $cryptedSubscriberId = Crypt::encryptString(Auth::user()->id);
                    @endphp
                        <input type="hidden" name="creatorId" value="{{$cryptedCreatorId}}">
                        <input type="hidden" name="subscriberId" value="{{$cryptedSubscriberId}}">
                        <button type="submit" class="btn btn-danger btn-sm btn-block"><i class="fas fa-user-minus mr-1"></i>Dejar de seguir</button>
                    </form>
                    @endif
                </div>
                <div class="media-body mb-5 text-white">
                    <h2 class="my-3">{{ $user->name }}</h2>
                    @if (isset($user->about_me))
                    <h6>Sobre mí:</h6>
                    <p class="w-50 d-md-block d-none small mb-4">{{ $user->about_me }}</p>
                    <p class="d-md-none d-block small mb-4">{{ $user->about_me }}</p>
                    @elseif (Auth::user()->id == $user->id)
                    <p class="w-50 d-md-block d-none small mb-4">Edita tu perfil para incluir una pequeña descripción en este espacio</p>
                    <p class="d-md-none d-block small mb-4">Edita tu perfil para incluir una pequeña descripción en este espacio</p>
                    @endif
                </div>
            </div>
        </div>

        <div class="bg-secondary text-white p-4 pt-5 d-flex justify-content-end text-center">
            <ul class="list-inline mb-0 pt-3">
                <li class="list-inline-item">
                    <h5 class="font-weight-bold mb-0 d-block">{{$countUserRecommendations}}</h5><small> <i class="fas fa-feather-alt mr-1"></i>Reseñas</small>
                </li>
                <li class="list-inline-item">
                    <h5 class="font-weight-bold mb-0 d-block">{{$countUserSubscribers}}</h5><small> <i class="fas fa-user-friends mr-1"></i>
                    @if(Auth::user()->id == $user->id && $countUserSubscribers>0)
                    <a href="{{url('user/followers/'.Auth::user()->id)}}" class="text-white" data-toggle="tooltip" data-placement="left" title="Ver tu lista de seguidores">
                    Followers
                    </a>
                    @else
                    Followers
                    @endif
                    </small>
                </li>
            </ul>
        </div>

        <div class="py-4 px-4 bg-dark text-white">
            <div class="py-3">
                <div class="row mb-3">
                    <div class="col">
                        <h3>Últimas reseñas</h3>
                    </div>
                    {{-- Pagination --}}
                    <div class="col">
                        {!! $userRecommendations->links() !!}
                    </div>
                </div>
                @if ($userRecommendations->total()>0)
                <div class="card-deck">
                @for ($i=0; $i < $recommendationslength; $i++)
                    <div class="card text-white shadow-lg border-0" style="background: no-repeat center center; background-size: cover; background-image: linear-gradient(to top, rgba(41, 43, 44, 1), rgba(41, 43, 44, 0.3)), url('{{$movies[$i]->screenshots[0]}}');">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xl-4 col-12">
                                    <a href="{{url('catalog/show/'.$movies[$i]->id)}}" title="Ir a la ficha de '{{$movies[$i]->title}}' ({{$movies[$i]->year}})">
                                        <img src="{{ $movies[$i]->poster }}" alt='Portada de "{{ $movies[$i]->title }}" ({{ $movies[$i]->year }})' class="mx-auto d-none d-xl-block" height="252">
                                        <img src="{{ $movies[$i]->poster }}" alt='Portada de "{{ $movies[$i]->title }}" ({{ $movies[$i]->year }})' class="mx-auto d-block d-xl-none" height="252">
                                    </a>
                                </div>
                                <div class="col-xl-8 col-12">
                                    <h2><a class="text-white pl-3 pr-5 py-3" style="background: url(<?php echo asset('storage/bg_images/pincel.png') ?>) no-repeat center center; background-size: cover;" href="{{url('catalog/show/'.$movies[$i]->id)}}" title="Ir a la ficha de '{{$movies[$i]->title}}' ({{$movies[$i]->year}})">{{ $movies[$i]->title }}</a></h2>
                                    <h3 class="pt-3"><i class="fas fa-star" style="color: yellow;"></i> {{ $userRecommendations[$i]->rating }}/10</h3>
                                    <h4>{{ $userRecommendations[$i]->headline }}</h4>
                                    <div class="d-none d-xl-block">
                                        <p>{{ $userRecommendations[$i]->review }}</p>
                                    </div>
                                    <div id="review" class="d-block d-xl-none pb-3">
                                        <p class="collapse" id="reviewText">{{ $userRecommendations[$i]->review }}</p>
                                        <a class="collapsed" data-toggle="collapse" href="#reviewText" aria-expanded="false" aria-controls="reviewText"></a>
                                    </div>
                                    @if (Auth::user()->id !== $user->id)
                                    @php
                                    $cryptedRecId = Crypt::encryptString($userRecommendations[$i]->id);
                                    $cryptedUser = Crypt::encryptString(Auth::user()->id);
                                    $recId = $userRecommendations[$i]->id;
                                    @endphp
                                        <p>
                                        @if ($totalVal[$i] == 0)
                                        <span>Todavía no existen valoraciones de esta reseña</span>
                                        @elseif ($totalVal[$i] > 0 && $valorations[$i] >= 0 && $valorations[$i] <= 25)
                                        <span class="text-danger" data-toggle="tooltip" data-placement="right" title="Un {{ $valorations[$i] }}% de las {{ $totalVal[$i] }} valoraciones son positivas.">Mala reseña</span>
                                        @elseif ($totalVal[$i] > 0 && $valorations[$i] > 25 && $valorations[$i] <= 50)
                                        <span class="text-warning" data-toggle="tooltip" data-placement="right" title="Un {{ $valorations[$i] }}% de las {{ $totalVal[$i] }} valoraciones son positivas.">Reseña mejorable</span>
                                        @elseif ($totalVal[$i] > 0 && $valorations[$i] > 50 && $valorations[$i] <= 75)
                                        <span class="text-info" data-toggle="tooltip" data-placement="right" title="Un {{ $valorations[$i] }}% de las {{ $totalVal[$i] }} valoraciones son positivas.">Buena reseña</span>
                                        @else
                                        <span class="text-primary" data-toggle="tooltip" data-placement="right" title="Un {{ $valorations[$i] }}% de las {{ $totalVal[$i] }} valoraciones son positivas.">Reseña excelente</span>
                                        @endif
                                        </p>
                                        <button type="button" id="useful{{$i}}" class="my-2 btn {{ ( isset($usefulness[$recId]) && $usefulness[$recId]->usefulness == 'useful') ? 'btn-primary' : 'btn-outline-primary' }} {{ ( isset($usefulness[$recId]) && $usefulness[$recId]->usefulness == 'useful') ? 'active' : 'inactive' }} usefulnessBtn" data-id="useful{{$i}}" data-status="{{ ( isset($usefulness[$recId]) && $usefulness[$recId]->usefulness == 'useful') ? 'active' : 'inactive' }}" data-usefulness="useful" data-rec="{{$cryptedRecId}}" data-user="{{$cryptedUser}}"><i class="fas fa-thumbs-up mr-1"></i>Me resulta útil</button>
                                        <button type="button" id="useless{{$i}}" class="my-2 btn {{ ( isset($usefulness[$recId]) && $usefulness[$recId]->usefulness == 'useless') ? 'btn-danger' : 'btn-outline-danger' }} {{ ( isset($usefulness[$recId]) && $usefulness[$recId]->usefulness == 'useless') ? 'active' : 'inactive' }} usefulnessBtn" data-id="useless{{$i}}" data-status="{{ ( isset($usefulness[$recId]) && $usefulness[$recId]->usefulness == 'useless') ? 'active' : 'inactive' }}" data-usefulness="useless" data-rec="{{$cryptedRecId}}" data-user="{{$cryptedUser}}"><i class="fas fa-thumbs-down mr-1"></i>No me resulta útil</button>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col text-right">
                                @if (Auth::user()->id == $user->id)
                                @php 
                                $cryptedMovieId = Crypt::encryptString($movies[$i]->id);
                                $cryptedUserId = Crypt::encryptString($user->id);
                                @endphp
                                    <a href="{{url('recommendations/edit/'.$movies[$i]->id.'/'.$user->id)}}" type="button" class="btn btn-warning my-2"><i class="fas fa-edit mr-1"></i>Editar tu reseña</a>
                                    <a type="button" class="btn btn-danger deleteBtn my-2" data-movie="{{$movies[$i]->title}}" data-movieid="{{$cryptedMovieId}}" data-userid="{{$cryptedUserId}}"><i class="fas fa-trash mr-1"></i>Eliminar tu reseña</a>
                                @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endfor
                </div>
                <div class="row mt-3">
                    <div class="col">
                        <h3 class="text-center">Número de reseñas por género</h3>
                        <div class="d-flex flex-wrap justify-content-around align-content-stretch align-items-stretch">
                        @foreach($genres as $genreId=>$genre)
                            <div>
                                <a href="{{url('recommendations/genres/'.$genreId.'/user/'.$user->id)}}" type="button" class="btn btn-{{$genreId}} btn-lg m-2 shadow" style="text-shadow: -1px 0px 2px #000;">
                                    {{$genresTrans[$genreId]}} <span class="badge badge-primary" style="text-shadow:none !important;">{{$genre}}</span>
                                </a>
                            </div>
                        @endforeach
                        </div>
                    </div>
                </div>
                @else
                <h3 class="text-center">¡Vaya! Aún no has hecho ninguna reseña, busca tu película preferida en el catálogo y comparte tu opinión sobre ella.</h3>
                @endif
            </div>
        </div>
    </div>
</div>

<!-- Review delete confirmation modal -->
<div class="modal fade" id="deleteConfirmation">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content bg-dark text-white">
        
        <div class="modal-header border-secondary">
            <h4 class="modal-title">Estás apunto de eliminar tu reseña</h4>
            <button type="button" class="close text-white" data-dismiss="modal">×</button>
        </div>
        
        <div class="modal-body">
            <h4 class="text-center" id="question"></h4>
            <form action="{{action('App\Http\Controllers\RecommendationsController@destroy')}}" method="POST">
                @csrf
                @method('DELETE')
                <input type="hidden" name="movieId">
                <input type="hidden" name="userId">
                <div class="row">
                    <div class="col">
                        <button type="button" class="btn btn-danger btn-block" data-dismiss="modal"><i class="fas fa-times-circle mr-1"></i>Cancelar</button>
                    </div>
                    <div class="col">
                        <button type="submit" class="btn btn-success btn-block"><i class="fas fa-trash mr-1"></i>Confirmar</button>
                    </div>
                </div>
            </form>
        </div>
        
        </div>
    </div>
</div>

<script>

$(document).ready(function(){

    $('[data-toggle="tooltip"]').tooltip();

    $(".deleteBtn").click(function() {
        var movie = $(this).data("movie");
        var movieId = $(this).data("movieid");
        var userId = $(this).data("userid");
        $("#question").html("¿Quieres eliminar tu reseña sobre "+movie+"?");
        $('input[name=movieId]').val(movieId);
        $('input[name=userId]').val(userId);
        $("#deleteConfirmation").modal();
    });

    $(".usefulnessBtn").click(function() {
        var id = $(this).data('id');
        var status = $(this).data('status');
        var usefulness = $(this).data('usefulness');
        var recommendation = $(this).data("rec");
        var user = $(this).data("user");
        var _token = $('input[name="_token"]').val();
        $.ajax({
            url:"{{ route('recommendation.usefulness') }}",
            method:"POST",
            data:{status:status, usefulness:usefulness, recommendation:recommendation, user:user, _token:_token},
            success:function(status) {
                if (usefulness == "useful") {
                    var contraryId = id.slice(6,7);
                    if (status == "active") {
                        $("#"+id).removeClass("btn-outline-primary").addClass("btn-primary");
                        $("#"+id).removeClass('inactive').addClass('active');
                        if ($("#useless"+contraryId).hasClass("active")) {
                            $("#useless"+contraryId).removeClass("btn-danger").addClass("btn-outline-danger");
                            $("#useless"+contraryId).removeClass('active').addClass('inactive');
                        }
                    }
                    else {
                        $("#"+id).removeClass("btn-primary").addClass("btn-outline-primary");
                        $("#"+id).removeClass('active').addClass('inactive');
                    }
                }
                else {
                    var contraryId = id.slice(7,8);
                    if (status == "active") {
                        $("#"+id).removeClass("btn-outline-danger").addClass("btn-danger");
                        $("#"+id).removeClass('inactive').addClass('active');
                        if ($("#useful"+contraryId).hasClass("active")) {
                            $("#useful"+contraryId).removeClass("btn-primary").addClass("btn-outline-primary");
                            $("#useful"+contraryId).removeClass('active').addClass('inactive');
                        }
                    }
                    else {
                        $("#"+id).removeClass("btn-danger").addClass("btn-outline-danger");
                        $("#"+id).removeClass('active').addClass('inactive');
                    }
                }
            }
        });
    });

});

</script>

@stop
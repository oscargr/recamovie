@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <h2 class="text-center w-50 mx-auto text-white mt-3 py-3" style="background: url(<?php echo asset('storage/bg_images/pincel.png') ?>) no-repeat center center; background-size: cover;">Eliminar "{{$movie->title}}" ({{$movie->year}})</h2>
    <div class="card bg-dark text-white text-center w-75 mx-auto">
        <div class="card-header">
            <img class="img-fluid" src="{{$movie->poster}}" alt="Poster {{$movie->title}}" width="219" height="324">
        </div>
        <div class="card-body">
            <h3>"{{$movie->title}}" ({{$movie->year}})</h3>
            <p>Dirigida por <strong>{{$movie->director}}</strong></p>
        </div>
        <div class="card-footer">
            <h4>¿Desea eliminar la película?</h4>
            <form action="{{action('App\Http\Controllers\CatalogController@deleteMovie', $movie->id)}}" method="POST"  style="width: 50%; margin: 0px auto;">
                @csrf
                @method('DELETE')
                <a href="{{url('catalog/show/'.$movie->id)}}" class="btn btn-danger my-2"><i class="fas fa-times-circle"></i> Cancelar</a>
                <button type="submit" class="btn btn-success my-2"><i class="fas fa-trash"></i> Confirmar</button>
            </form>
        </div>
    </div>

</div>

@stop
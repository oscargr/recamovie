@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div id="errorSubmitting" class="alert alert-danger alert-dismissible d-none fade mt-5 fixed-top w-75 mx-auto">
        <button id="errorSubmittingBtn" type="button" class="close">&times;</button>
        <div id="submitErrorMessage">
        </div>
    </div>
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show mt-3">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>¡Error!</strong> {{$errors->first()}}
    </div>
    @endif
    <div class="row">
        <div class="col">
            <h2 class="text-center w-50 mx-auto text-white mt-3 py-3" style="background: url(<?php echo asset('storage/bg_images/pincel.png') ?>) no-repeat center center; background-size: cover;">Editar "{{$movie->title}}" ({{$movie->year}})</h2>
        </div>
    </div>
    <div class="row bg-gradient text-white p-3">
        <div class="col-12 col-md-6 col-xl-8">
            <form action="{{ action('App\Http\Controllers\CatalogController@putEdit', $movie->id) }}" method="POST"
                autocomplete="on">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col">
                        <div class="form-group py-2">
                            <label for="title">Título:</label>
                            <input type="text" name="title" class="form-control border"
                                placeholder="Título de la película" value="{{$movie->title}}" 
                                required autofocus>
                            <div id="titleError" class="alert alert-danger alert-dismissible d-none fade mt-1">
                                <button id="titleErrorBtn" type="button" class="close">&times;</button>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group py-2">
                            <label for="year">Año:</label>
                            <input type="text" name="year" class="form-control border"
                                placeholder="Año de la película" value="{{$movie->year}}"
                                required>
                            <div id="yearError" class="alert alert-danger alert-dismissible d-none fade mt-1">
                                <button id="yearErrorBtn" type="button" class="close">&times;</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group py-2">
                            <label for="director">Director/a:</label>
                            <input type="text" name="director" class="form-control border"
                                placeholder="Director/a/es de la película" value="{{$movie->director}}" 
                                required>
                            <div id="directorError" class="alert alert-danger alert-dismissible d-none fade mt-1">
                                <button id="directorErrorBtn" type="button" class="close">&times;</button>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group py-2">
                            <label for="poster">Póster:</label>
                            <input type="url" name="poster" class="form-control border"
                                placeholder="URL del póster de la película" value="{{$movie->poster}}" 
                                required>
                            <div id="posterError" class="alert alert-danger alert-dismissible d-none fade mt-1">
                                <button id="posterErrorBtn" type="button" class="close">&times;</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group py-2">
                            <label for="duration">Duración:</label>
                            <input type="text" name="duration" class="form-control border"
                                placeholder="Duración de la película. Formato: 1h 30min" value="{{$movie->duration}}" 
                                required>
                            <div id="durationError" class="alert alert-danger alert-dismissible d-none fade mt-1">
                                <button id="durationErrorBtn" type="button" class="close">&times;</button>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group py-2">
                            <label for="trailer">Tráiler:</label>
                            <button type="button" class="btn btn-sm btn-outline-info" data-toggle="popover" data-trigger="focus"><i class="fas fa-info-circle"></i> ¿Qué es la ID de un vídeo en YouTube?</button>
                            <input type="text" name="trailer" class="form-control border"
                                placeholder="ID del tráiler de la película en youtube" value="{{$movie->trailer}}" 
                                required>
                            <div id="trailerError" class="alert alert-danger alert-dismissible d-none fade mt-1">
                                <button id="trailerErrorBtn" type="button" class="close">&times;</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group py-2">
                    <label for="genre">Género:</label>
                    <select name="genre" class="custom-select" required>
                        <option>Selecciona un género</option>
                        @foreach($genres as $genreId=>$genre)
                        @if ($movie->genre == $genreId)
                        <option value="{{$genreId}}" selected>{{$genre}}</option>
                        @else
                        <option value="{{$genreId}}">{{$genre}}</option>
                        @endif
                        @endforeach
                    </select>
                    <div id="genreError" class="alert alert-danger alert-dismissible d-none fade mt-1">
                        <button id="genreErrorBtn" type="button" class="close">&times;</button>
                    </div>
                </div>
                <div class="form-group py-2">
                    <label for="synopsis">Sinópsis:</label>
                    <textarea name="synopsis" id="synopsis" class="form-control border" rows="5" required>{{$movie->synopsis}}</textarea>
                    <div id="synopsisError" class="alert alert-danger alert-dismissible d-none fade mt-1">
                        <button id="synopsisErrorBtn" type="button" class="close">&times;</button>
                    </div>
                </div>
                <div class="form-group py-2">
                    <label for="rating">Puntuaciones:</label>
                    <div class="row py-2">
                        <div class="col-12 col-xl-2">IMDb</div>
                        <div class="col-12 col-xl-10">
                            <input type="hidden" name="ratings[0][key]" value="imdb">
                            <input type="number" name="ratings[0][value]" min="0" max="10" step="0.1" class="form-control border"
                                placeholder="Nota de IMDb, Formato: 0-10 con 1 decimal" value="{{$movie->ratings[0]['value']}}" required>
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-12 col-xl-2">Filmaffinity</div>
                        <div class="col-12 col-xl-10">
                            <input type="hidden" name="ratings[1][key]" value="filmaffinity">
                            <input type="number" name="ratings[1][value]" min="0" max="10" step="0.1" class="form-control border"
                            placeholder="Nota de FilmAffinity, Formato: 0-10 con 1 decimal" value="{{$movie->ratings[1]['value']}}">
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-12 col-xl-2">Rotten Tomatoes</div>
                        <div class="col-12 col-xl-10">
                            <input type="hidden" name="ratings[2][key]" value="rottentomatoes">
                            <input type="number" name="ratings[2][value]" min="0" max="100" class="form-control border"
                            placeholder="Nota de Rotten Tomatoes, Formato: 0-100 sin decimales" value="{{$movie->ratings[2]['value']}}">
                        </div>
                    </div>
                    <div id="ratingsError" class="alert alert-danger alert-dismissible d-none fade mt-1">
                        <button id="ratingsErrorBtn" type="button" class="close">&times;</button>
                        <div id="ratingsErrorMessage">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="screenshots">Fotos</label>
                    <div class="row py-2">
                        <div class="col">
                            <button type="button" class="btn btn-success btn-block" id="addScreenshot"><i class="fas fa-plus"></i> Añadir foto</button>
                        </div>
                        <div class="col">
                            <button type='button' class='btn btn-danger btn-block' id="deleteScreenshot"><i class="fas fa-minus"></i> Eliminar foto</button>
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="col-2">
                            Nº foto:
                        </div>
                        <div class="col-10">
                            URL:
                        </div>
                    </div>
                    <div id="screenshots">
                        @foreach ($movie->screenshots as $screenshot)
                        <div class="row py-2 screenshotFields">
                            <div class="col-2">{{$loop->index+1}}</div>
                            <div class="col-10">
                                <input type="url" name="screenshots[]" class="form-control border border-danger" value="{{$screenshot}}">
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <button type="submit" class="btn btn-success btn-block mb-5"><i class="fas fa-save"></i> Guardar cambios</button>
                    </div>
                    <div class="col">
                        <a href="{{url('catalog/show/'.$movie->id)}}" class="btn btn-danger btn-block"><i class="fas fa-times-circle"></i> Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
        <div class="d-none d-md-block col-md-6 col-xl-4">
        <div class="sticky-top">
            <h4 class="text-center">Vista previa</h4>
            <div class="row">
                <div class="col">
                    <h5 id="titlePreview"></h5>
                    <h6><span id="yearPreview"></span><span id="separator" class="d-none"> · </span><span id="durationPreview"></span><span id="secondSeparator" class="d-none"> · </span><span id="genrePreview"></span></h6>
                </div>
                <div class="col">
                    <h6 id="directorPreview"></h6>
                </div>
                <div class="col-12 d-flex justify-content-around flex-wrap my-2">
                    <div id="imdbRatingPreview"></div>
                    <div id="filmaffinityRatingPreview"></div>
                    <div id="rottentomatoesRatingPreview"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-4">
                    <div class="row">
                        <div class="col">
                            <img id="posterPreview" src="" alt="" class="img-fluid">
                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-8">
                    <div id="screenshotsPreview" class="carousel slide" data-ride="carousel">
                        <ul id="indicators" class="carousel-indicators">

                        </ul>
                        <div id="innerCarousel" class="carousel-inner">

                        </div>
                        <a href="#screenshotsPreview" class="carousel-control-prev" data-slide="prev">
                            <span class="carousel-control-prev-icon"></span>
                        </a>
                        <a href="#screenshotsPreview" class="carousel-control-next" data-slide="next">
                            <span class="carousel-control-next-icon"></span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col text-center">
                    <button type="button" class="btn btn-info btn-block mt-2 trailerBtn d-none" data-toggle="modal"
                        data-target="#movieTrailer"><i class="fab fa-youtube"></i> Ver tráiler</button>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p id="synopsisPreview"></p>
                </div>
            </div>
            </div>
        </div>
    </div>
    <div class="row d-block d-md-none fixed-bottom">
        <div class="col">
            <button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#mobilePreview"><i class="fas fa-eye"></i> Vista previa</button>
        </div>
    </div>
</div>

<!-- Modal for the mobile preview -->
<div class="modal fade" id="mobilePreview">
    <div class="modal-dialog modal-xl modal-dialog-centered">
        <div class="modal-content bg-dark text-white">

            <div class="modal-header border-secondary">
                <h4 class="modal-title">Vista previa</h4>
                <button type="button" class="close text-white" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col">
                        <h6 id="titleMobilePreview"></h6>
                        <h6><span id="yearMobilePreview"></span><span id="mobileSeparator" class="d-none"> · </span><span id="durationMobilePreview"></span><span id="secondMobileSeparator" class="d-none"> · </span><span id="genreMobilePreview"></span></h6>
                    </div>
                    <div class="col">
                        <h6 id="directorMobilePreview"></h6>
                    </div>
                    <div class="col-12 d-flex justify-content-around flex-wrap my-2">
                        <div id="imdbRatingMobilePreview"></div>
                        <div id="filmaffinityRatingMobilePreview"></div>
                        <div id="rottentomatoesRatingMobilePreview"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-4">
                        <div class="row">
                            <div class="col">
                                <img id="posterMobilePreview" src="" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-8">
                        <div id="screenshotsMobilePreview" class="carousel slide" data-ride="carousel">
                            <ul id="mobileIndicators" class="carousel-indicators">

                            </ul>
                            <div id="mobileInnerCarousel" class="carousel-inner">

                            </div>
                            <a href="#screenshotsMobilePreview" class="carousel-control-prev" data-slide="prev">
                                <span class="carousel-control-prev-icon"></span>
                            </a>
                            <a href="#screenshotsMobilePreview" class="carousel-control-next" data-slide="next">
                                <span class="carousel-control-next-icon"></span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-center">
                        <button type="button" class="btn btn-info btn-block mt-2 trailerBtn d-none" data-toggle="modal"
                            data-target="#movieTrailer"><i class="fab fa-youtube"></i> Ver tráiler</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <p id="synopsisPreview"></p>
                    </div>
                </div>
            </div>

            <div class="modal-footer border-secondary">
                <button type="button" class="btn btn-danger btn-lg btn-block" data-dismiss="modal"><i class="fas fa-window-close"></i> Cerrar</button>
            </div>

        </div>
    </div>
</div>

<!-- Modal for the movie trailer -->
<div class="modal fade" id="movieTrailer">
    <div class="modal-dialog modal-xl modal-dialog-centered">
        <div class="modal-content bg-dark text-white">

            <div class="modal-header border-secondary">
                <h4 id="modalTitlePreview" class="modal-title"></h4>
                <button type="button" class="close text-white" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe id="trailerPreview" width="1280" height="720" src="" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                </div>
            </div>

            <div class="modal-footer border-secondary">
                <button type="button" class="btn btn-danger btn-lg btn-block" data-dismiss="modal"><i class="fas fa-window-close"></i> Cerrar</button>
            </div>

        </div>
    </div>
</div>

<script src="{{ asset('js/recamovie.js') }}"></script>

@stop
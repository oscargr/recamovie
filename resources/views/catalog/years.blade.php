@extends('layouts.app')

@section('content') 

<div class="container-fluid">
    <h2 class="text-center w-50 mx-auto text-white mt-3 py-3" style="background: url(<?php echo asset('storage/bg_images/pincel.png') ?>) no-repeat center center; background-size: cover;">Películas del año {{$intYear}}</h2>
    <div class="row">
        <div class="col-6 mx-auto">
            <select name="year" class="custom-select my-3">
                <option selected>Selecciona un año</option>
                @foreach($years as $year)
                <option value="{{$year}}">{{$year}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="d-flex flex-row justify-content-around flex-wrap">
    @if ($movies->total()>0)
    @foreach ($movies as $movie)
        <div class="card bg-dark text-white my-3 mx-3 rounded-top text-center shadow" style="width: 400px;">
            <div class="card-header d-flex justify-content-center">
                <img src="{{$movie->poster}}" alt="Portada '{{$movie->title}}'" height="250" width="auto">
            </div>
            <div class="card-body d-flex flex-column">
                <h3 class="card-title">{{$movie->title}} ({{$movie->year}})</h3>
                <h6>Género: <a href="{{url('catalog/genres/'.$movie->genre.'/years/'.$intYear)}}">{{$genres[$movie->genre]}}</a></h6>
                <h6>dirigida por <span class="font-weight-bold">{{$movie->director}}<span></h6>
                <div class="mt-auto">
                    <a href="{{url('catalog/show/'.$movie->id)}}" title="Ir a la ficha de '{{$movie->title}}' ({{$movie->year}})" type="button" class="btn btn-primary btn-block"><i class="fas fa-file-alt"></i> Ver ficha</a>
                    @if (Auth::user()->role=="user")
                    <a href="{{url('recommendations/create/'.$movie->id)}}" class="btn btn-warning btn-block"><i class="fas fa-pen"></i> Escribir una recomendación</a>
                    @endif
                </div>
            </div>
        </div>
    @endforeach
    @else
    <div class="card bg-dark text-white">
        <div class="card-body">
            <h3>Aún no se han añadido películas del año {{ $intYear }}</h3>
        </div>
    </div>
    @endif
    </div>
    {{-- Pagination --}}
    <div class="d-flex justify-content-center mt-auto">
        {!! $movies->links() !!}
    </div>
</div>

<script>

$(document).ready(function() {

    $("select[name='year']").change(function() {
        var year = $(this).val();
        location.assign("http://localhost/recamovie/public/catalog/years/"+year);
    });

});

</script>

@stop
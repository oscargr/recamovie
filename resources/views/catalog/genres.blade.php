@extends('layouts.app')

@section('content') 

<div class="container-fluid">
    <h2 class="text-center w-50 mx-auto text-white mt-3 py-3" style="background: url(<?php echo asset('storage/bg_images/pincel.png') ?>) no-repeat center center; background-size: cover;">Películas del género <span class="text-lowercase">{{$moviesGenre}}</span></h2>
    @if ($movies->total()>0)
    <div class="row">
        <div class="col">
            {{-- Pagination --}}
            <div class="d-flex justify-content-center mt-auto">
                {!! $movies->links() !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-2 d-none d-xl-block">
            <ul class="list-group mt-3">
            @foreach($genres as $genreId=>$genre)
                @if ($genre == $moviesGenre)
                <a class="list-group-item list-group-item-action list-group-item-dark active d-flex justify-content-between align-items-center" href="">{{$genre}}
                <span class="badge badge-primary badge-pill">{{$moviesCountByGenre[$genreId]}}</span>
                </a>
                @else
                <a class="list-group-item list-group-item-action list-group-item-dark d-flex justify-content-between align-items-center" href="{{url('catalog/genres/'.$genreId)}}">{{$genre}}
                <span class="badge badge-primary badge-pill">{{$moviesCountByGenre[$genreId]}}</span>
                </a>
                @endif
            @endforeach
            </ul>
        </div>
        <div class="col-xl-10 col-12">
            <div class="d-block d-xl-none w-75 mx-auto">
                <a href="#genresList" class="btn btn-dark btn-block" data-toggle="collapse"><i class="fas fa-filter mr-1"></i>Seleccionar género</a>
            </div>
            <div id="genresList" class="collapse w-75 mx-auto">
                <ul class="list-group mt-3">
                @foreach($genres as $genreId=>$genre)
                    @if ($genre == $moviesGenre)
                    <a class="list-group-item list-group-item-action list-group-item-dark active d-flex justify-content-between align-items-center" href="">{{$genre}}
                    <span class="badge badge-primary badge-pill">{{$moviesCountByGenre[$genreId]}}</span>
                    </a>
                    @else
                    <a class="list-group-item list-group-item-action list-group-item-dark d-flex justify-content-between align-items-center" href="{{url('catalog/genres/'.$genreId)}}">{{$genre}}
                    <span class="badge badge-primary badge-pill">{{$moviesCountByGenre[$genreId]}}</span>
                    </a>
                    @endif
                @endforeach
                </ul>
            </div>
            <div class="d-flex flex-row justify-content-around flex-wrap">
            @foreach ($movies as $movie)
                <div class="card bg-dark text-white my-3 mx-3 rounded-top text-center shadow" style="width: 400px;">
                    <div class="card-header d-flex justify-content-center">
                        <img src="{{$movie->poster}}" alt="Portada '{{$movie->title}}'" height="250" width="auto">
                    </div>
                    <div class="card-body d-flex flex-column">
                        <h3 class="card-title">{{$movie->title}} (<a href="{{url('catalog/genres/'.$movie->genre.'/years/'.$movie->year)}}">{{$movie->year}}</a>)</h3>
                        <h6>dirigida por <span class="font-weight-bold">{{$movie->director}}<span></h6>
                        <div class="mt-auto">
                            <a href="{{url('catalog/show/'.$movie->id)}}" title="Ir a la ficha de '{{$movie->title}}' ({{$movie->year}})" type="button" class="btn btn-primary btn-block"><i class="fas fa-file-alt"></i> Ver ficha</a>
                            @if (Auth::user()->role=="user")
                            <a href="{{url('recommendations/create/'.$movie->id)}}" class="btn btn-warning btn-block"><i class="fas fa-pen"></i> Escribir una recomendación</a>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    </div>
    {{-- Pagination --}}
    <div class="d-flex justify-content-center mt-auto">
        {!! $movies->links() !!}
    </div>
    @else
    <div class="row">
        <div class="col-xl-2 d-none d-xl-block">
            <ul class="list-group mt-3">
            @foreach($genres as $genreId=>$genre)
                @if ($genre == $moviesGenre)
                <a class="list-group-item list-group-item-action list-group-item-dark active d-flex justify-content-between align-items-center" href="">{{$genre}}
                <span class="badge badge-primary badge-pill">{{$moviesCountByGenre[$genreId]}}</span>
                </a>
                @else
                <a class="list-group-item list-group-item-action list-group-item-dark d-flex justify-content-between align-items-center" href="{{url('catalog/genres/'.$genreId)}}">{{$genre}}
                <span class="badge badge-primary badge-pill">{{$moviesCountByGenre[$genreId]}}</span>
                </a>
                @endif
            @endforeach
            </ul>
        </div>
        <div class="col-xl-10 col-12">
            <div class="d-block d-xl-none w-75 mx-auto">
                <a href="#genresList" class="btn btn-dark btn-block" data-toggle="collapse"><i class="fas fa-filter mr-1"></i>Seleccionar género</a>
            </div>
            <div id="genresList" class="collapse w-75 mx-auto">
                <ul class="list-group mt-3">
                @foreach($genres as $genreId=>$genre)
                    @if ($genre == $moviesGenre)
                    <a class="list-group-item list-group-item-action list-group-item-dark active d-flex justify-content-between align-items-center" href="">{{$genre}}
                    <span class="badge badge-primary badge-pill">{{$moviesCountByGenre[$genreId]}}</span>
                    </a>
                    @else
                    <a class="list-group-item list-group-item-action list-group-item-dark d-flex justify-content-between align-items-center" href="{{url('catalog/genres/'.$genreId)}}">{{$genre}}
                    <span class="badge badge-primary badge-pill">{{$moviesCountByGenre[$genreId]}}</span>
                    </a>
                    @endif
                @endforeach
                </ul>
            </div>
            <div class="d-flex flex-row justify-content-around flex-wrap mt-3">
                <div class="card bg-dark text-white">
                    <div class="card-body">
                        <h3 class="text-center">Aún no se han añadido películas del género <span class="text-lowercase">{{$moviesGenre}}</span></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>

@stop
@extends('layouts.app')

@section('content')

@php
$recommendationslength = sizeof($recommendations);
@endphp


<div class="container-fluid" style="background: url('{{$movie->screenshots[0]}}') no-repeat center center fixed; background-size: cover;">
    <div class="bg-gradient p-3 rounded-bottom">
        <div class="row text-white">
            <div class="col-12 col-md">
                <h2>{{$movie->title}}</h2>
                <h4><span><a href="{{url('catalog/years/'.$movie->year)}}">{{$movie->year}}</a></span> · <span>{{$movie->duration}}</span> · <span><a href="{{url('catalog/genres/'.$movie->genre)}}">{{$genres[$movie->genre]}}</a></span></h4>
            </div>
            <div class="col-12 col-md text-md-center">
                <h4>{{$movie->director}}</h4>
            </div>
            <div class="col-12 d-flex justify-content-around flex-wrap my-2">
                @foreach ($movie->ratings as $rating)
                @if (!is_null($rating['value']))
                @switch($rating['key'])
                    @case('imdb')
                    <div>
                        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/69/IMDB_Logo_2016.svg/1920px-IMDB_Logo_2016.svg.png" alt="Logo IMDb" width="50">
                        <span> · {{$rating['value']}}/10</span>
                    </div>
                    @break
                    @case('filmaffinity')
                    <div>
                        <img src="http://www.filmaffinity.com/images/logo4.png" alt="Logo FilmAffinity" width="50">
                        <span> · {{$rating['value']}}/10</span>
                    </div>
                    @break
                    @case('rottentomatoes')
                    <div>
                        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Rotten_Tomatoes_logo.svg/1024px-Rotten_Tomatoes_logo.svg.png" alt="Logo Rotten Tomatoes" width="50">
                        <span> · {{$rating['value']}}%</span>
                    </div>
                    @break        
                @endswitch
                @endif
                @endforeach
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-4">
                <div class="row">
                    <div class="col">
                        <img src="{{$movie->poster}}" alt="Portada {{$movie->title}}" class="img-fluid w-100" style="object-fit: cover;">
                    </div>
                </div>
                <div class="row d-none d-md-block">
                    <div class="col text-center">
                        <button type="button" class="btn btn-info btn-lg btn-block mt-2" data-toggle="modal" data-target="#movieTrailer"><i class="fab fa-youtube"></i> Ver tráiler</button>
                    </div>
                </div>
            </div>
            <div class="col-md-9 col-8">
                <div id="screenshots" class="carousel slide h-100" data-ride="carousel">
                    <ul class="carousel-indicators">
                        @foreach ($movie->screenshots as $screenshot)
                        @if ($loop->first)
                        <li data-target="#screenshots" data-slide-to="{{$loop->index}}" class="active"></li>
                        @else
                        <li data-target="#screenshots" data-slide-to="{{$loop->index}}"></li>
                        @endif
                        @endforeach
                    </ul>
                    <div class="carousel-inner h-100">
                        @foreach ($movie->screenshots as $screenshot)
                        @if ($loop->first)
                        <div class="carousel-item h-100 active"  style="background: url('{{$screenshot}}') no-repeat center center; background-size: cover;">
                            {{-- Use this if the background image fails <img src="{{$screenshot}}" class="img-fluid w-100" style="object-fit: cover;" alt="screenshot{{$loop->index}}" width="1920" height="1080"> --}}
                        </div>
                        @else
                        <div class="carousel-item h-100" style="background: url('{{$screenshot}}') no-repeat center center; background-size: cover;">
                        </div>
                        @endif
                        @endforeach
                    </div>
                    <a href="#screenshots" class="carousel-control-prev" data-slide="prev">
                        <span class="carousel-control-prev-icon"></span>
                    </a>
                    <a href="#screenshots" class="carousel-control-next" data-slide="next">
                        <span class="carousel-control-next-icon"></span>
                    </a>
                </div>
            </div>
        </div>
        <div class="row d-md-none d-block">
            <div class="col text-center">
                <button type="button" class="btn btn-info btn-sm btn-block mt-2" data-toggle="modal" data-target="#movieTrailer"><i class="fab fa-youtube"></i> Ver tráiler</button>
            </div>
        </div>
        <div class="row">
            <div class="col w-75 mx-auto text-justify pt-2 text-white">
                <h5 class="pt-2">Sinópsis:</h5>
                <p>{{$movie->synopsis}}</p>
            </div>
        </div>
        <div class="row d-flex flex-column flex-md-row">
            @if(Auth::user()->role=="admin")
            <div class="col my-2">
                <a href="{{url('catalog/edit/'.$movie->id)}}" class="btn btn-warning btn-block"><i class="fas fa-edit"></i> Editar película</a>
            </div>
            <div class="col my-2">
                <a href="{{url('catalog/delete/'.$movie->id)}}" class="btn btn-danger btn-block"><i class="fas fa-trash"></i> Eliminar película</a>
            </div>
            @else
            <div class="col my-2">
                <a href="{{url('recommendations/create/'.$movie->id)}}" class="btn btn-warning btn-block"><i class="fas fa-pen"></i> Escribir una recomendación sobre esta película</a>
            </div>
            @endif
            <div class="col my-2">
                <a href="{{url('catalog')}}" class="btn btn-secondary btn-block"><i class="fas fa-arrow-left"></i> Volver al catálogo</a>
            </div>
        </div>
    </div>
    @if (Auth::user()->role != "admin")
    <div class="row">
        <div class="col">
            <div class="d-flex flex-row justify-content-around flex-wrap">
                @if ($recommendationslength>0)
                @for ($i=0; $i < $recommendationslength; $i++)
                @php
                $countScreenshots = count($movie->screenshots);
                $screenshotIndex = mt_rand(0,($countScreenshots-1));
                @endphp
                <div class="card card w-75 mx-auto m-3 text-white shadow-lg border-0" style="background: no-repeat center center; background-size: cover; background-image: linear-gradient(to top, rgba(41, 43, 44, 1), rgba(41, 43, 44, 0.3)), url('{{$movie->screenshots[$screenshotIndex]}}');">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <h4 class="clearfix"><span class="float-left">{{ $recommendations[$i]->headline }}</span><span class="float-right" style="font-size:large;"><small style="font-size:small;">reseña hecha por</small> <a href="{{url('user/'.$users[$i]->id)}}" title="Ir al perfil de {{$users[$i]->name}}" class="btn btn-sm btn-primary">{{ $users[$i]->name }}<i class="fas fa-external-link-alt ml-1"></i></a></span></h4>
                                <h5><i class="fas fa-star" style="color: yellow;"></i> {{ $recommendations[$i]->rating }}/10</h5>
                                <div class="d-none d-xl-block">
                                    <p>{{ $recommendations[$i]->review }}</p>
                                </div>
                                <div id="review" class="d-block d-xl-none pb-3">
                                    <p class="collapse" id="reviewText">{{ $recommendations[$i]->review }}</p>
                                    <a class="collapsed" data-toggle="collapse" href="#reviewText" aria-expanded="false" aria-controls="reviewText"></a>
                                </div>
                                @if (Auth::user()->id !== $users[$i]->id)
                                @php
                                $cryptedRecId = Crypt::encryptString($recommendations[$i]->id);
                                $cryptedUser = Crypt::encryptString(Auth::user()->id);
                                $recId = $recommendations[$i]->id;
                                @endphp
                                    <p>
                                    @if ($totalVal[$i] == 0)
                                    <span>Todavía no existen valoraciones de esta reseña</span>
                                    @elseif ($totalVal[$i] > 0 && $valorations[$i] >= 0 && $valorations[$i] <= 25)
                                    <span class="text-danger"data-toggle="tooltip" data-placement="right" title="Un {{ $valorations[$i] }}% de las {{ $totalVal[$i] }} valoraciones son positivas.">Mala reseña</span>
                                    @elseif ($totalVal[$i] > 0 && $valorations[$i] > 25 && $valorations[$i] <= 50)
                                    <span class="text-warning"data-toggle="tooltip" data-placement="right" title="Un {{ $valorations[$i] }}% de las {{ $totalVal[$i] }} valoraciones son positivas.">Reseña mejorable</span>
                                    @elseif ($totalVal[$i] > 0 && $valorations[$i] > 50 && $valorations[$i] <= 75)
                                    <span class="text-info"data-toggle="tooltip" data-placement="right" title="Un {{ $valorations[$i] }}% de las {{ $totalVal[$i] }} valoraciones son positivas.">Buena reseña</span>
                                    @else
                                    <span class="text-primary"data-toggle="tooltip" data-placement="right" title="Un {{ $valorations[$i] }}% de las {{ $totalVal[$i] }} valoraciones son positivas.">Reseña excelente</span>
                                    @endif
                                    </p>
                                    <button type="button" id="useful{{$i}}" class="my-2 btn {{ ( isset($usefulness[$recId]) && $usefulness[$recId]->usefulness == 'useful') ? 'btn-primary' : 'btn-outline-primary' }} {{ ( isset($usefulness[$recId]) && $usefulness[$recId]->usefulness == 'useful') ? 'active' : 'inactive' }} usefulnessBtn" data-id="useful{{$i}}" data-status="{{ ( isset($usefulness[$recId]) && $usefulness[$recId]->usefulness == 'useful') ? 'active' : 'inactive' }}" data-usefulness="useful" data-rec="{{$cryptedRecId}}" data-user="{{$cryptedUser}}"><i class="fas fa-thumbs-up mr-1"></i>Me resulta útil</button>
                                    <button type="button" id="useless{{$i}}" class="my-2 btn {{ ( isset($usefulness[$recId]) && $usefulness[$recId]->usefulness == 'useless') ? 'btn-danger' : 'btn-outline-danger' }} {{ ( isset($usefulness[$recId]) && $usefulness[$recId]->usefulness == 'useless') ? 'active' : 'inactive' }} usefulnessBtn" data-id="useless{{$i}}" data-status="{{ ( isset($usefulness[$recId]) && $usefulness[$recId]->usefulness == 'useless') ? 'active' : 'inactive' }}" data-usefulness="useless" data-rec="{{$cryptedRecId}}" data-user="{{$cryptedUser}}"><i class="fas fa-thumbs-down mr-1"></i>No me resulta útil</button>
                                @endif
                            </div>
                        </div>
                        @if (Auth::user()->id == $users[$i]->id)
                        <div class="row">
                            <div class="col text-right">
                            @if (Auth::user()->id == $users[$i]->id)
                                @php
                                $cryptedMovieId = Crypt::encryptString($movie->id);
                                $cryptedUserId = Crypt::encryptString($users[$i]->id);
                                @endphp
                                <a href="{{url('recommendations/edit/'.$movie->id.'/'.$users[$i]->id)}}" type="button" class="my-2 btn btn-warning"><i class="fas fa-edit mr-1"></i>Editar tu reseña</a>
                                <a type="button" class="my-2 btn btn-danger deleteBtn" data-movie="{{$movie->title}}" data-movieid="{{$cryptedMovieId}}" data-userid="{{$cryptedUserId}}"><i class="fas fa-trash mr-1"></i>Eliminar tu reseña</a>
                            @endif
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
                @endfor
                @else
                <div class="card card w-75 mx-auto m-3 text-white text-center shadow-lg border-0" style="background: no-repeat center center; background-size: cover; background-image: linear-gradient(to top, rgba(41, 43, 44, 1), rgba(41, 43, 44, 0.3)), url('{{$movie->screenshots[0]}}');">
                    <div class="card-body">
                        <a id="collapseAnimation" href="{{url('recommendations/create/'.$movie->id)}}" title='Escribir recomendación de "{{$movie->title}}" ({{$movie->year}})' class="text-white">
                            <h4>No hay ninguna reseña de esta película</h4>
                            <h4 id="collapsedText" class="collapse"><i class="fas fa-pen mr-1"></i>Haz click para escribir tu reseña</h4>
                        </a>
                    </div>
                </div>
                @endif
            </div>
            {{-- Pagination --}}
            <div class="d-flex justify-content-center mt-auto">
                {!! $recommendations->links() !!}
            </div>
        </div>
    </div>
    @endif
</div>

<!-- Movie trailer modal -->
<div class="modal fade" id="movieTrailer">
    <div class="modal-dialog modal-xl modal-dialog-centered">
        <div class="modal-content bg-dark text-white">
        
        <div class="modal-header border-secondary">
            <h4 class="modal-title">Tráiler "{{$movie->title}}" ({{$movie->year}})</h4>
            <button type="button" class="close text-white" data-dismiss="modal">&times;</button>
        </div>
        
        <div class="modal-body">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe width="1280" height="720" src="https://www.youtube.com/embed/{{$movie->trailer}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
        
        <div class="modal-footer border-secondary">
            <button type="button" class="btn btn-danger btn-lg btn-block" data-dismiss="modal"><i class="fas fa-window-close mr-1"></i>Cerrar</button>
        </div>
        
        </div>
    </div>
</div>

<!-- Review delete confirmation modal -->
<div class="modal fade" id="deleteConfirmation">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content bg-dark text-white">
        
        <div class="modal-header border-secondary">
            <h4 class="modal-title">Estás apunto de eliminar tu reseña</h4>
            <button type="button" class="close text-white" data-dismiss="modal">×</button>
        </div>
        
        <div class="modal-body">
            <h4 class="text-center" id="question"></h4>
            <form action="{{action('App\Http\Controllers\RecommendationsController@destroy')}}" method="POST">
                @csrf
                @method('DELETE')
                <input type="hidden" name="movieId">
                <input type="hidden" name="userId">
                <div class="row">
                    <div class="col">
                        <button type="button" class="btn btn-danger btn-block" data-dismiss="modal"><i class="fas fa-times-circle mr-1"></i>Cancelar</button>
                    </div>
                    <div class="col">
                        <button type="submit" class="btn btn-success btn-block"><i class="fas fa-trash mr-1"></i>Confirmar</button>
                    </div>
                </div>
            </form>
        </div>
        
        </div>
    </div>
</div>

<script>
$(document).ready(function(){

    $('[data-toggle="tooltip"]').tooltip();

    /**
     * This function stops and resets the video embeded in the modal when it's closed
     */
    $("#movieTrailer").on('hidden.bs.modal', function (e) {
        $("#movieTrailer iframe").attr("src", $("#movieTrailer iframe").attr("src"));
    });

    $("#collapseAnimation").hover(
        function() {
        $('#collapsedText').collapse('show');
        }, function() {
            $('#collapsedText').collapse('hide');
        }
    );

    $(".deleteBtn").click(function() {
        var movie = $(this).data("movie");
        var movieId = $(this).data("movieid");
        var userId = $(this).data("userid");
        $("#question").html("¿Quieres eliminar tu reseña sobre "+movie+"?");
        $('input[name=movieId]').val(movieId);
        $('input[name=userId]').val(userId);
        $("#deleteConfirmation").modal();
    });

    $(".usefulnessBtn").click(function() {
        var id = $(this).data('id');
        var status = $(this).data('status');
        var usefulness = $(this).data('usefulness');
        var recommendation = $(this).data("rec");
        var user = $(this).data("user");
        var _token = $('input[name="_token"]').val();
        $.ajax({
            url:"{{ route('recommendation.usefulness') }}",
            method:"POST",
            data:{status:status, usefulness:usefulness, recommendation:recommendation, user:user, _token:_token},
            success:function(status) {
                if (usefulness == "useful") {
                    var contraryId = id.slice(6,7);
                    if (status == "active") {
                        $("#"+id).removeClass("btn-outline-primary").addClass("btn-primary");
                        $("#"+id).removeClass('inactive').addClass('active');
                        if ($("#useless"+contraryId).hasClass("active")) {
                            $("#useless"+contraryId).removeClass("btn-danger").addClass("btn-outline-danger");
                            $("#useless"+contraryId).removeClass('active').addClass('inactive');
                        }
                    }
                    else {
                        $("#"+id).removeClass("btn-primary").addClass("btn-outline-primary");
                        $("#"+id).removeClass('active').addClass('inactive');
                    }
                }
                else {
                    var contraryId = id.slice(7,8);
                    if (status == "active") {
                        $("#"+id).removeClass("btn-outline-danger").addClass("btn-danger");
                        $("#"+id).removeClass('inactive').addClass('active');
                        if ($("#useful"+contraryId).hasClass("active")) {
                            $("#useful"+contraryId).removeClass("btn-primary").addClass("btn-outline-primary");
                            $("#useful"+contraryId).removeClass('active').addClass('inactive');
                        }
                    }
                    else {
                        $("#"+id).removeClass("btn-danger").addClass("btn-outline-danger");
                        $("#"+id).removeClass('active').addClass('inactive');
                    }
                }
            }
        });
    });

});
</script>

@stop
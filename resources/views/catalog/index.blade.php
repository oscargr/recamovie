@extends('layouts.app')

@section('content') 

<div class="container-fluid">
    <div class="row">
        <div class="col">
            <h2 class="text-center w-50 mx-auto text-white mt-3 py-3" style="background: url(<?php echo asset('storage/bg_images/pincel.png') ?>) no-repeat center center; background-size: cover;">Catálogo de películas</h2>
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="input-group w-50 mx-auto">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-search"></i></span>
                                </div>
                                <input type="text" name="search" class="form-control" placeholder="Búsqueda por título...">
                                {{ csrf_field() }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div id="movieList">
                            
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-center py-3">
                        <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#filters"><i class="fas fa-filter mr-1"></i>Filtros</button>
                    </div>
                </div>
                <div id="filters" class="row collapse">
                    <div class="col">
                        <select name="genre" class="custom-select my-3">
                            <option selected>Selecciona un género</option>
                            @foreach($genres as $genreId=>$genre)
                            <option value="{{$genreId}}">{{$genre}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col">
                        <select name="year" class="custom-select my-3">
                            <option selected>Selecciona un año</option>
                            @foreach($years as $year)
                            <option value="{{$year}}">{{$year}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-12">
                        <a id="filter" href="" type="button" class="btn btn-success btn-block"><i class="fas fa-filter mr-1"></i>Filtrar películas</a>
                    </div>
                </div>
                <div class="row">
                    {{-- Pagination --}}
                    <div class="col d-flex justify-content-center mt-3">
                        {!! $movies->links() !!}
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-around flex-wrap">
                    @foreach ($movies as $movie)
                    <div class="card bg-dark text-white my-3 mx-3 rounded-top text-center shadow" style="width: 400px;">
                        <div class="card-header d-flex justify-content-center">
                            <img src="{{$movie->poster}}" alt="Portada '{{$movie->title}}'" height="250" width="auto">
                        </div>
                        <div class="card-body d-flex flex-column">
                            <h3 class="card-title">{{$movie->title}} (<a href="{{url('catalog/years/'.$movie->year)}}">{{$movie->year}}</a>)</h3>
                            <h6>Género: <a href="{{url('catalog/genres/'.$movie->genre)}}">{{$genres[$movie->genre]}}</a></h6>
                            <h6>dirigida por <span class="font-weight-bold">{{$movie->director}}<span></h6>
                            <div class="mt-auto">
                                <a href="{{url('catalog/show/'.$movie->id)}}" title="Ir a la ficha de '{{$movie->title}}' ({{$movie->year}})" type="button" class="btn btn-primary btn-block"><i class="fas fa-file-alt"></i> Ver ficha</a>
                                @if (Auth::user()->role=="user")
                                <a href="{{url('recommendations/create/'.$movie->id)}}" class="btn btn-warning btn-block"><i class="fas fa-pen"></i> Escribir una recomendación</a>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                {{-- Pagination --}}
                <div class="d-flex justify-content-center mt-auto">
                    {!! $movies->links() !!}
                </div>
        </div>
    </div>
</div>

<script>

    $(document).ready(function() {

        $("input[name='search']").keyup(function() {
            var query = $(this).val();
            var queryLength = $(this).val().length;
            if (query != '' && queryLength>1) {
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('catalog.search') }}",
                    method:"GET",
                    data:{query:query, _token:_token},
                    success:function(data) {
                        $("#movieList").removeClass('d-none').addClass('d-block');
                        $("#movieList").html(data);
                    }
                });
            }
            else {
                $("#movieList").removeClass('d-block').addClass('d-none');
            }
        });

        $("select[name='genre']").change(function() {
            if ($(this).val() != "Selecciona un género"){
                if ($("select[name='year']").val() == "Selecciona un año") {
                    $("#filter").attr("href","http://localhost/recamovie/public/catalog/genres/"+$(this).val());
                }
                else {
                    $("#filter").attr("href","http://localhost/recamovie/public/catalog/genres/"+$(this).val()+"/years/"+$("select[name='year']").val());
                }
            }
        });

        $("select[name='year']").change(function() {
            if ($(this).val() != "Selecciona un año"){
                if ($("select[name='genre']").val() == "Selecciona un género") {
                    $("#filter").attr("href","http://localhost/recamovie/public/catalog/years/"+$(this).val());
                }
                else {
                    $("#filter").attr("href","http://localhost/recamovie/public/catalog/genres/"+$("select[name='genre']").val()+"/years/"+$(this).val());
                }
            }
        });

    });

</script>

@stop
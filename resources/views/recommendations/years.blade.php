@extends('layouts.app')

@section('content')

@php
$recommendationslength = sizeof($recommendations);
@endphp

<div class="container-fluid">
    <h2 class="text-center w-50 mx-auto text-white mt-3 py-3" style="background: url(<?php echo asset('storage/bg_images/pincel.png') ?>) no-repeat center center; background-size: cover;">Reseñas de películas del año {{$intYear}}</h2>
    <div class="row">
        <div class="col-6 mx-auto">
            <select name="year" class="custom-select my-3">
                <option selected>Selecciona un año</option>
                @foreach($years as $year)
                <option value="{{$year}}">{{$year}}</option>
                @endforeach
            </select>
        </div>
    </div>
    @if ($recommendations->total()>0)
    {{-- Pagination --}}
    <div class="d-flex justify-content-center mt-4">
        {!! $recommendations->links() !!}
    </div>
    <div class="row">
        <div class="col">
        @for ($i=0; $i < $recommendationslength; $i++)
            <div class="card w-75 mx-auto m-3 text-white shadow-lg border-0" style="background: no-repeat center center; background-size: cover; background-image: linear-gradient(to top, rgba(41, 43, 44, 1), rgba(41, 43, 44, 0.3)), url('{{$movies[$i]->screenshots[0]}}');">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-xl-2">
                            <a href="{{url('catalog/show/'.$movies[$i]->id)}}" title="Ir a la ficha de '{{$movies[$i]->title}}' ({{$movies[$i]->year}})">
                                <img src="{{ $movies[$i]->poster }}" alt='Portada de "{{ $movies[$i]->title }}" ({{ $movies[$i]->year }})' class="img-fluid d-none d-xl-block">
                                <img src="{{ $movies[$i]->poster }}" alt='Portada de "{{ $movies[$i]->title }}" ({{ $movies[$i]->year }})' class="mx-auto d-block d-xl-none" height="150">
                            </a>
                        </div>
                        <div class="col-12 col-xl-10">
                            <h2 class="clearfix"><span class="float-left pl-3 pr-5 py-4" style="background: url(<?php echo asset('storage/bg_images/pincel.png') ?>) no-repeat center center; background-size: cover;"><a class="text-white" href="{{url('catalog/show/'.$movies[$i]->id)}}" title="Ir a la ficha de '{{$movies[$i]->title}}' ({{$movies[$i]->year}})">{{ $movies[$i]->title }}</a></span><span class="float-right py-4" style="font-size:large;"><small style="font-size:small;">reseña hecha por</small> <a href="{{url('user/'.$users[$i]->id)}}" data-toggle="tooltip" data-placement="bottom" title="Ir al perfil de {{$users[$i]->name}}" class="btn btn-sm btn-primary">{{ $users[$i]->name }}<i class="fas fa-external-link-alt ml-1"></i></a></span></h2>
                            <h3><i class="fas fa-star" style="color: yellow;"></i> {{ $recommendations[$i]->rating }}/10</h3>
                            <h4>{{ $recommendations[$i]->headline }}</h4>
                            <div class="d-none d-xl-block">
                                <p>{{ $recommendations[$i]->review }}</p>
                            </div>
                            <div id="review" class="d-block d-xl-none pb-3">
                                <p class="collapse" id="reviewText">{{ $recommendations[$i]->review }}</p>
                                <a class="collapsed" data-toggle="collapse" href="#reviewText" aria-expanded="false" aria-controls="reviewText"></a>
                            </div>
                            @if (Auth::user()->id !== $users[$i]->id)
                            @php 
                            $cryptedRecId = Crypt::encryptString($recommendations[$i]->id);
                            $cryptedUser = Crypt::encryptString(Auth::user()->id);
                            $recId = $recommendations[$i]->id;
                            @endphp
                                <p>
                                @if ($totalVal[$i] == 0)
                                <span>Todavía no existen valoraciones de esta reseña</span>
                                @elseif ($totalVal[$i] > 0 && $valorations[$i] >= 0 && $valorations[$i] <= 25)
                                <span class="text-danger"data-toggle="tooltip" data-placement="right" title="Un {{ $valorations[$i] }}% de las {{ $totalVal[$i] }} valoraciones son positivas.">Mala reseña</span>
                                @elseif ($totalVal[$i] > 0 && $valorations[$i] > 25 && $valorations[$i] <= 50)
                                <span class="text-warning"data-toggle="tooltip" data-placement="right" title="Un {{ $valorations[$i] }}% de las {{ $totalVal[$i] }} valoraciones son positivas.">Reseña mejorable</span>
                                @elseif ($totalVal[$i] > 0 && $valorations[$i] > 50 && $valorations[$i] <= 75)
                                <span class="text-info" data-toggle="tooltip" data-placement="right" title="Un {{ $valorations[$i] }}% de las {{ $totalVal[$i] }} valoraciones son positivas.">Buena reseña</span>
                                @else
                                <span class="text-primary" data-toggle="tooltip" data-placement="right" title="Un {{ $valorations[$i] }}% de las {{ $totalVal[$i] }} valoraciones son positivas.">Reseña excelente</span>
                                @endif
                                </p>
                                <button type="button" id="useful{{$i}}" class="my-2 btn {{ ( isset($usefulness[$recId]) && $usefulness[$recId]->usefulness == 'useful') ? 'btn-primary' : 'btn-outline-primary' }} {{ ( isset($usefulness[$recId]) && $usefulness[$recId]->usefulness == 'useful') ? 'active' : 'inactive' }} usefulnessBtn" data-id="useful{{$i}}" data-status="{{ ( isset($usefulness[$recId]) && $usefulness[$recId]->usefulness == 'useful') ? 'active' : 'inactive' }}" data-usefulness="useful" data-rec="{{$cryptedRecId}}" data-user="{{$cryptedUser}}"><i class="fas fa-thumbs-up mr-1"></i>Me resulta útil</button>
                                <button type="button" id="useless{{$i}}" class="my-2 btn {{ ( isset($usefulness[$recId]) && $usefulness[$recId]->usefulness == 'useless') ? 'btn-danger' : 'btn-outline-danger' }} {{ ( isset($usefulness[$recId]) && $usefulness[$recId]->usefulness == 'useless') ? 'active' : 'inactive' }} usefulnessBtn" data-id="useless{{$i}}" data-status="{{ ( isset($usefulness[$recId]) && $usefulness[$recId]->usefulness == 'useless') ? 'active' : 'inactive' }}" data-usefulness="useless" data-rec="{{$cryptedRecId}}" data-user="{{$cryptedUser}}"><i class="fas fa-thumbs-down mr-1"></i>No me resulta útil</button>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col text-right">
                        @if (Auth::user()->id == $users[$i]->id)
                            @php 
                            $cryptedMovieId = Crypt::encryptString($movies[$i]->id);
                            $cryptedUserId = Crypt::encryptString($users[$i]->id);
                            @endphp
                            <a href="{{url('recommendations/edit/'.$movies[$i]->id.'/'.$users[$i]->id)}}" type="button" class="my-2 btn btn-warning"><i class="fas fa-edit mr-1"></i>Editar tu reseña</a>
                            <a type="button" class="my-2 btn btn-danger deleteBtn" data-movie="{{$movies[$i]->title}}" data-movieid="{{$cryptedMovieId}}" data-userid="{{$cryptedUserId}}"><i class="fas fa-trash mr-1"></i>Eliminar tu reseña</a>
                        @endif
                        </div>
                    </div>
                </div>
            </div>
        @endfor
        </div>
    </div>
    {{-- Pagination --}}
    <div class="d-flex justify-content-center mt-auto">
        {!! $recommendations->links() !!}
    </div>
    @else
    <div class="d-flex flex-row justify-content-around flex-wrap">
        <div class="card bg-dark text-white">
            <div class="card-body">
                <h3>Aún no se han reseñado películas del año {{$intYear}}</h3>
            </div>
        </div>
    </div>
    @endif
</div>

<!-- Review delete confirmation modal -->
<div class="modal fade" id="deleteConfirmation">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content bg-dark text-white">
        
        <div class="modal-header border-secondary">
            <h4 class="modal-title">Estás apunto de eliminar tu reseña</h4>
            <button type="button" class="close text-white" data-dismiss="modal">×</button>
        </div>
        
        <div class="modal-body">
            <h4 id="question"></h4>
            <form action="{{action('App\Http\Controllers\RecommendationsController@destroy')}}" method="POST">
                @csrf
                @method('DELETE')
                <input type="hidden" name="movieId">
                <input type="hidden" name="userId">
                <div class="row">
                    <div class="col">
                        <button type="button" class="btn btn-danger btn-block" data-dismiss="modal"><i class="fas fa-times-circle mr-1"></i>Cancelar</button>
                    </div>
                    <div class="col">
                        <button type="submit" class="btn btn-success btn-block"><i class="fas fa-trash mr-1"></i>Confirmar</button>
                    </div>
                </div>
            </form>
        </div>
        
        </div>
    </div>
</div>

<script>

$(document).ready(function(){

    $('[data-toggle="tooltip"]').tooltip();

    $(".deleteBtn").click(function() {
        var movie = $(this).data("movie");
        var movieId = $(this).data("movieid");
        var userId = $(this).data("userid");
        $("#question").html("¿Quieres eliminar tu reseña sobre "+movie+"?");
        $('input[name=movieId]').val(movieId);
        $('input[name=userId]').val(userId);
        $("#deleteConfirmation").modal();
    });

    $(".usefulnessBtn").click(function() {
        var id = $(this).data('id');
        var status = $(this).data('status');
        var usefulness = $(this).data('usefulness');
        var recommendation = $(this).data("rec");
        var user = $(this).data("user");
        var _token = $('input[name="_token"]').val();
        $.ajax({
            url:"{{ route('recommendation.usefulness') }}",
            method:"POST",
            data:{status:status, usefulness:usefulness, recommendation:recommendation, user:user, _token:_token},
            success:function(status) {
                if (usefulness == "useful") {
                    var contraryId = id.slice(6,7);
                    if (status == "active") {
                        $("#"+id).removeClass("btn-outline-primary").addClass("btn-primary");
                        $("#"+id).removeClass('inactive').addClass('active');
                        if ($("#useless"+contraryId).hasClass("active")) {
                            $("#useless"+contraryId).removeClass("btn-danger").addClass("btn-outline-danger");
                            $("#useless"+contraryId).removeClass('active').addClass('inactive');
                        }
                    }
                    else {
                        $("#"+id).removeClass("btn-primary").addClass("btn-outline-primary");
                        $("#"+id).removeClass('active').addClass('inactive');
                    }
                }
                else {
                    var contraryId = id.slice(7,8);
                    if (status == "active") {
                        $("#"+id).removeClass("btn-outline-danger").addClass("btn-danger");
                        $("#"+id).removeClass('inactive').addClass('active');
                        if ($("#useful"+contraryId).hasClass("active")) {
                            $("#useful"+contraryId).removeClass("btn-primary").addClass("btn-outline-primary");
                            $("#useful"+contraryId).removeClass('active').addClass('inactive');
                        }
                    }
                    else {
                        $("#"+id).removeClass("btn-danger").addClass("btn-outline-danger");
                        $("#"+id).removeClass('active').addClass('inactive');
                    }
                }
            }
        });
    });

    $("select[name='year']").change(function() {
        var year = $(this).val();
        location.assign("http://localhost/recamovie/public/recommendations/years/"+year);
    });

});

</script>


@stop
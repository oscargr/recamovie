@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div id="errorSubmitting" class="alert alert-danger alert-dismissible d-none fade mt-5 fixed-top w-75 mx-auto">
        <button id="errorSubmittingBtn" type="button" class="close">&times;</button>
        <div id="submitErrorMessage">
        </div>
    </div>
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show mt-3">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>¡Error!</strong> {{$errors->first()}}
    </div>
    @endif
    <div class="row">
        <div class="col-md-8 col-12 mx-auto">
            <div class="card bg-dark text-white my-5">
                <div class="card-header">
                    <h2 class="text-center">Escribe tu recomendación sobre {{$movie->title}}</h2>
                </div>
                <div class="card-body">
                    <form action="{{ action('App\Http\Controllers\RecommendationsController@store') }}" method="POST">
                    @csrf
                    @php 
                    $cryptedMovieId = Crypt::encryptString($movie->id);
                    $cryptedUserId = Crypt::encryptString(Auth::user()->id);
                    @endphp
                    <input type="hidden" name="movieId" value="{{$cryptedMovieId}}">
                    <input type="hidden" name="userId" value="{{$cryptedUserId}}">
                    <div class="row">
                        <div class="col-10">
                            <h4 class="value text-center text-warning">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </h4>
                        </div>
                        <div class="col-2">
                            <h3 class="valueNum">5</h3>
                        </div>
                    </div>
                    <input type="range" name="rating" min="0" max="10" step="1" value="5" class="custom-range">
                    <div class="form-group">
                        <label for="headline">Titular:<span class="ml-2" id="headlineStatus"></span></label>
                        <input type="text" class="form-control" id="headline" name="headline" placeholder="Escribe un titular para la recomendación..." value="{{old('headline')}}">
                        <div id="headlineError" class="alert alert-danger alert-dismissible d-none fade mt-1">
                            <button id="headlineErrorBtn" type="button" class="close">&times;</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="review">Recomendación:<span class="ml-2" id="reviewStatus"></span></label>
                        <textarea class="form-control" rows="10" id="review" name="review" placeholder="Escribe aquí tu reseña..." required>{{old('review')}}</textarea>
                        <div id="reviewError" class="alert alert-danger alert-dismissible d-none fade mt-1">
                            <button id="reviewErrorBtn" type="button" class="close">&times;</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <button type="submit" class="btn btn-success btn-block"><i class="fas fa-save"></i> Guardar recomendación</button>
                        </div>
                        <div class="col">
                            <a href="{{url()->previous()}}" class="btn btn-secondary btn-block"><i class="fas fa-arrow-left"></i> Volver a la página anterior</a>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('js/form_validations.js') }}" defer></script>

<script>
    var elem = document.querySelector('input[type="range"]');

    var rangeValue = function(){
        var newValue = elem.value;
        var target = document.querySelector('.value');
        var target2 = document.querySelector('.valueNum');
        target.innerHTML = "";
        for (var i=0; i<newValue; i++) {
            target.innerHTML += '<i class="fas fa-star mx-1"></i>';
        }
        target2.innerHTML = newValue;
    }

    elem.addEventListener("input", rangeValue);
</script>

@stop
        <nav id="navbar" class="sticky-top navbar navbar-expand-md bg-dark navbar-dark d-flex shadow-lg">
            <a href="{{url('/')}}" class="navbar-brand order-2 order-md-1 text-center" title="Volver a la página de inicio">
                <img src="<?php echo asset('storage/logo/logo.png') ?>" alt="Logo REC A Movie" width="75" height="75">
                <p class="logo-name mb-0">REC A MOVIE</p>
            </a>
            @if(Auth::check())
            <button class="navbar-toggler order-1 order-md-2" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="navbar-toggler-icon"></span>
                <span>Menú</span>
            </button>
            <div class="collapse navbar-collapse justify-content-center order-2" id="collapsibleNavbar">
                <ul class="navbar-nav">
                @if (Auth::user()->role=="admin")
                    <li class="nav-item">
                        @if(Request::is('catalog') && !Request::is('catalog/create'))
                        <a href="{{url('/catalog')}}" class="nav-link active"><i class="fas fa-film"></i> Catálogo</a>
                        @else
                        <a href="{{url('/catalog')}}" class="nav-link"><i class="fas fa-film"></i> Catálogo</a>
                        @endif
                    </li>
                    <li class="nav-item">
                        @if(Request::is('catalog/create'))
                        <a href="{{url('/catalog/create')}}" class="nav-link active"><i class="fas fa-plus-square"></i> Añadir película</a>
                        @else
                        <a href="{{url('/catalog/create')}}" class="nav-link"><i class="fas fa-plus-square"></i> Añadir película</a>
                        @endif
                    </li>
                @else
                    <li class="nav-item">
                        @if(Request::is('home'))
                        <a href="{{url('/home')}}" class="nav-link active"><i class="fas fa-house-user"></i> Inicio</a>
                        @else
                        <a href="{{url('/home')}}" class="nav-link"><i class="fas fa-house-user"></i> Inicio</a>
                        @endif
                    </li>
                    <li class="nav-item">
                        @if(Request::is('catalog'))
                        <a href="{{url('/catalog')}}" class="nav-link active"><i class="fas fa-film"></i> Catálogo</a>
                        @else
                        <a href="{{url('/catalog')}}" class="nav-link"><i class="fas fa-film"></i> Catálogo</a>
                        @endif
                    </li>
                    <li class="nav-item">
                        @if(Request::is('recommendations'))
                        <a href="{{url('/recommendations')}}" class="nav-link active"><i class="fas fa-certificate"></i> Últimas recomendaciones</a>
                        @else
                        <a href="{{url('/recommendations')}}" class="nav-link"><i class="fas fa-certificate"></i> Últimas recomendaciones</a>
                        @endif
                    </li>     
                @endif    
                    <li class="nav-item dropdown d-block d-md-none">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        @if (Storage::exists('user_images/'.Auth::user()->id.'/profile_pic/User_'.Auth::user()->id.'_profile_pic.png') || Storage::exists('user_images/'.Auth::user()->id.'/profile_pic/User_'.Auth::user()->id.'_profile_pic.jpg') || Storage::exists('user_images/'.Auth::user()->id.'/profile_pic/User_'.Auth::user()->id.'_profile_pic.gif'))
                            <img src="{{ asset('storage/'.Auth::user()->profile_pic) }}" alt="Foto de perfil de {{ Auth::user()->name }}" width="20" height="20" class="mb-1 img-thumbnail rounded-circle">
                        @else
                            <img src="{{ asset('storage/images/default/Default_profile_pic.png') }}" alt="Foto de perfil de {{ Auth::user()->name }}" width="20" height="20" class="mb-1 img-thumbnail rounded-circle">
                        @endif
                            {{ Auth::user()->name }}
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            @if(Auth::user()->role == "user")
                            <a class="dropdown-item" href="{{url('user/'.Auth::user()->id)}}">
                                Ir a tu perfil<i class="fas fa-external-link-square-alt ml-1"></i>
                            </a>
                            @endif
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                Cerrar sesión<i class="fas fa-sign-out-alt ml-1"></i>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="d-none d-md-block order-3">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        @if (Storage::exists('user_images/'.Auth::user()->id.'/profile_pic/User_'.Auth::user()->id.'_profile_pic.png') || Storage::exists('user_images/'.Auth::user()->id.'/profile_pic/User_'.Auth::user()->id.'_profile_pic.jpg') || Storage::exists('user_images/'.Auth::user()->id.'/profile_pic/User_'.Auth::user()->id.'_profile_pic.gif'))
                            <img src="{{ asset('storage/'.Auth::user()->profile_pic) }}" alt="Foto de perfil de {{ Auth::user()->name }}" width="30" height="30" class="mb-1 img-thumbnail rounded-circle">
                        @else
                            <img src="{{ asset('storage/images/default/Default_profile_pic.png') }}" alt="Foto de perfil de {{ Auth::user()->name }}" width="30" height="30" class="mb-1 img-thumbnail rounded-circle">
                        @endif
                            {{ Auth::user()->name }}
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            @if(Auth::user()->role == "user")
                            <a class="dropdown-item" href="{{url('user/'.Auth::user()->id)}}">
                                Ir a tu perfil<i class="fas fa-external-link-square-alt ml-1"></i>
                            </a>
                            @endif
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                Cerrar sesión <i class="fas fa-sign-out-alt"></i>
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
            @else
            <button class="navbar-toggler order-1 order-md-2" type="button" data-toggle="collapse" data-target="#collapsibleLoginRegister">
                <span>Menú</span>
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end order-3 order-md-2" id="collapsibleLoginRegister">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}"><i class="fas fa-sign-in-alt"></i> Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}"><i class="fas fa-address-card"></i> Registro</a>
                    </li>
                </ul>
            </div>
            @endif
        </nav>
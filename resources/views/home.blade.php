@extends('layouts.app')

@section('content')

@php
$recommendationslength = sizeof($recommendations);
$usersLength = sizeof($usersToFollow)
@endphp

<div class="container-fluid">
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show mt-3">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>¡Error!</strong> {{$errors->first()}}
    </div>
    @endif
    <div class="row d-flex">
        <div class="col-md-8 col-11 mx-auto m-3 bg-gradient text-white order-md-1 order-2">
            <h2 class="py-3 d-md-block d-none">Bienvenido/a, {{ Auth::user()->name }}</h2>
            @if($recommendations->total()>0)
            <h4 class="py-3">Últimas reseñas de gente a la que sigues</h4>
            @for ($i=0; $i < $recommendationslength; $i++)
                <div class="card w-100 mx-auto m-3 text-white shadow-lg border-0" style="background: no-repeat center center; background-size: cover; background-image: linear-gradient(to top, rgba(41, 43, 44, 1), rgba(41, 43, 44, 0.3)), url('{{$movies[$i]->screenshots[0]}}');">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-xl-2">
                                <a href="{{url('catalog/show/'.$movies[$i]->id)}}" title="Ir a la ficha de '{{$movies[$i]->title}}' ({{$movies[$i]->year}})">
                                    <img src="{{ $movies[$i]->poster }}" alt='Portada de "{{ $movies[$i]->title }}" ({{ $movies[$i]->year }})' class="img-fluid d-none d-xl-block">
                                    <img src="{{ $movies[$i]->poster }}" alt='Portada de "{{ $movies[$i]->title }}" ({{ $movies[$i]->year }})' class="mx-auto d-block d-xl-none" height="150">
                                </a>
                            </div>
                            <div class="col-12 col-xl-10">
                                <h2 class="clearfix"><span class="float-left pl-3 pr-5 py-4" style="background: url(<?php echo asset('storage/bg_images/pincel.png') ?>) no-repeat center center; background-size: cover;"><a class="text-white" href="{{url('catalog/show/'.$movies[$i]->id)}}" title="Ir a la ficha de '{{$movies[$i]->title}}' ({{$movies[$i]->year}})">{{ $movies[$i]->title }}</a></span><span class="float-right py-2" style="font-size:large;"><small style="font-size:small;">reseña hecha por</small> <a href="{{url('user/'.$users[$i]->id)}}" title="Ir al perfil de {{$users[$i]->name}}" class="btn btn-sm btn-primary">{{ $users[$i]->name }}<i class="fas fa-external-link-alt ml-1"></i></a></span></h2>
                                <h3><i class="fas fa-star" style="color: yellow;"></i> {{ $recommendations[$i]->rating }}/10</h3>
                                <h4>{{ $recommendations[$i]->headline }}</h4>
                                <div class="d-none d-xl-block">
                                    <p>{{ $recommendations[$i]->review }}</p>
                                </div>
                                <div id="review" class="d-block d-xl-none pb-3">
                                    <p class="collapse" id="reviewText">{{ $recommendations[$i]->review }}</p>
                                    <a class="collapsed" data-toggle="collapse" href="#reviewText" aria-expanded="false" aria-controls="reviewText"></a>
                                </div>
                                @if (Auth::user()->id !== $users[$i]->id)
                                @php
                                $cryptedRecId = Crypt::encryptString($recommendations[$i]->id);
                                $cryptedUser = Crypt::encryptString(Auth::user()->id);
                                $recId = $recommendations[$i]->id;
                                @endphp
                                    <p>
                                    @if ($totalVal[$i] == 0)
                                    <span>Todavía no existen valoraciones de esta reseña</span>
                                    @elseif ($totalVal[$i] > 0 && $valorations[$i] >= 0 && $valorations[$i] <= 25)
                                    <span class="text-danger"data-toggle="tooltip" data-placement="right" title="Un {{ $valorations[$i] }}% de las {{ $totalVal[$i] }} valoraciones son positivas.">Mala reseña</span>
                                    @elseif ($totalVal[$i] > 0 && $valorations[$i] > 25 && $valorations[$i] <= 50)
                                    <span class="text-warning"data-toggle="tooltip" data-placement="right" title="Un {{ $valorations[$i] }}% de las {{ $totalVal[$i] }} valoraciones son positivas.">Reseña mejorable</span>
                                    @elseif ($totalVal[$i] > 0 && $valorations[$i] > 50 && $valorations[$i] <= 75)
                                    <span class="text-info"data-toggle="tooltip" data-placement="right" title="Un {{ $valorations[$i] }}% de las {{ $totalVal[$i] }} valoraciones son positivas.">Buena reseña</span>
                                    @else
                                    <span class="text-primary"data-toggle="tooltip" data-placement="right" title="Un {{ $valorations[$i] }}% de las {{ $totalVal[$i] }} valoraciones son positivas.">Reseña excelente</span>
                                    @endif
                                    </p>
                                    <button type="button" id="useful{{$i}}" class="my-2 btn {{ ( isset($usefulness[$recId]) && $usefulness[$recId]->usefulness == 'useful') ? 'btn-primary' : 'btn-outline-primary' }} {{ ( isset($usefulness[$recId]) && $usefulness[$recId]->usefulness == 'useful') ? 'active' : 'inactive' }} usefulnessBtn" data-id="useful{{$i}}" data-status="{{ ( isset($usefulness[$recId]) && $usefulness[$recId]->usefulness == 'useful') ? 'active' : 'inactive' }}" data-usefulness="useful" data-rec="{{$cryptedRecId}}" data-user="{{$cryptedUser}}"><i class="fas fa-thumbs-up mr-1"></i>Me resulta útil</button>
                                    <button type="button" id="useless{{$i}}" class="my-2 btn {{ ( isset($usefulness[$recId]) && $usefulness[$recId]->usefulness == 'useless') ? 'btn-danger' : 'btn-outline-danger' }} {{ ( isset($usefulness[$recId]) && $usefulness[$recId]->usefulness == 'useless') ? 'active' : 'inactive' }} usefulnessBtn" data-id="useless{{$i}}" data-status="{{ ( isset($usefulness[$recId]) && $usefulness[$recId]->usefulness == 'useless') ? 'active' : 'inactive' }}" data-usefulness="useless" data-rec="{{$cryptedRecId}}" data-user="{{$cryptedUser}}"><i class="fas fa-thumbs-down mr-1"></i>No me resulta útil</button>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col text-right">
                            @if (Auth::user()->id == $users[$i]->id)
                                @php 
                                $cryptedMovieId = Crypt::encryptString($movies[$i]->id);
                                $cryptedUserId = Crypt::encryptString($users[$i]->id);
                                @endphp
                                <a href="{{url('recommendations/edit/'.$movies[$i]->id.'/'.$users[$i]->id)}}" type="button" class="btn btn-warning"><i class="fas fa-edit mr-1"></i>Editar tu reseña</a>
                                <a type="button" class="btn btn-danger deleteBtn" data-movie="{{$movies[$i]->title}}" data-movieid="{{$cryptedMovieId}}" data-userid="{{$cryptedUserId}}"><i class="fas fa-trash mr-1"></i>Eliminar tu reseña</a>
                            @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endfor
            {{-- Pagination --}}
            <div class="d-flex justify-content-center mt-4">
                {!! $recommendations->links() !!}
            </div>
            @else
            <h4 class="py-3">Gente a la que seguir</h4>
            <div class="d-flex flex-wrap justify-content-around">
                @for ($j=0; $j<$usersLength; $j++)
                <div class="card bg-dark my-3 shadow" style="width:350px">
                @if (Storage::exists('user_images/'.$usersToFollow[$j]->id.'/banner_pic/User_'.$usersToFollow[$j]->id.'_banner_pic.png') || Storage::exists('user_images/'.$usersToFollow[$j]->id.'/banner_pic/User_'.$usersToFollow[$j]->id.'_banner_pic.jpg') || Storage::exists('user_images/'.$usersToFollow[$j]->id.'/banner_pic/User_'.$usersToFollow[$j]->id.'_banner_pic.gif'))
                    <div class="card-header border-bottom-0" style='height: 150px; background: no-repeat center center; background-size: cover; background-image: linear-gradient(to top, rgba(41, 43, 44, 1), rgba(41, 43, 44, 0.3)), url("{{asset('storage/'.$usersToFollow[$j]->banner_pic)}}");'>
                        @if (Storage::exists('user_images/'.$usersToFollow[$j]->id.'/profile_pic/User_'.$usersToFollow[$j]->id.'_profile_pic.png') || Storage::exists('user_images/'.$usersToFollow[$j]->id.'/profile_pic/User_'.$usersToFollow[$j]->id.'_profile_pic.jpg') || Storage::exists('user_images/'.$usersToFollow[$j]->id.'/profile_pic/User_'.$usersToFollow[$j]->id.'_profile_pic.gif'))
                        <img src="{{ asset('storage/'.$usersToFollow[$j]->profile_pic) }}" alt="Foto de perfil de {{ $usersToFollow[$j]->name }}" width="150" height="150" class="profile-picture img-thumbnail rounded-circle">
                        @else
                        <img src="{{ asset('storage/images/default/Default_profile_pic.png') }}" alt="Foto de perfil de {{ $usersToFollow[$j]->name }}" width="150" height="150" class="profile-picture img-thumbnail rounded-circle">
                        @endif
                    </div>
                @else
                    <div class="card-header border-bottom-0" style='height: 150px; background: no-repeat center center; background-size: cover; background-image: linear-gradient(to top, rgba(41, 43, 44, 1), rgba(41, 43, 44, 0.3)), url("{{asset('storage/images/default/Default_profile_background.jpg')}}");'>
                        @if (Storage::exists('user_images/'.$usersToFollow[$j]->id.'/profile_pic/User_'.$usersToFollow[$j]->id.'_profile_pic.png') || Storage::exists('user_images/'.$usersToFollow[$j]->id.'/profile_pic/User_'.$usersToFollow[$j]->id.'_profile_pic.jpg') || Storage::exists('user_images/'.$usersToFollow[$j]->id.'/profile_pic/User_'.$usersToFollow[$j]->id.'_profile_pic.gif'))
                        <img src="{{ asset('storage/'.$usersToFollow[$j]->profile_pic) }}" alt="Foto de perfil de {{ $usersToFollow[$j]->name }}" width="150" height="150" class="profile-picture img-thumbnail rounded-circle">
                        @else
                        <img src="{{ asset('storage/images/default/Default_profile_pic.png') }}" alt="Foto de perfil de {{ $usersToFollow[$j]->name }}" width="150" height="150" class="profile-picture img-thumbnail rounded-circle">
                        @endif
                    </div>
                @endif    
                    <div class="card-body d-flex flex-column">
                        <h4 class="card-title text-right mt-2">{{$usersToFollow[$j]->name}}</h4>
                        @if(!is_null($usersToFollow[$j]->about_me))
                        <h5>Sobre mí:</h5>
                        <p class="card-text">{{$usersToFollow[$j]->about_me}}</p>
                        @endif
                        <div class="row mt-auto">
                            <div class="col"><i class="fas fa-feather-alt mr-1"></i>{{$reviewsCount[$j]->reviewsCount}}<br>Reseñas</div>
                            <div class="col"><i class="fas fa-user-friends mr-1"></i>{{$followers[$j]}}<br>Seguidores</div>
                        </div>
                        <div class="mt-auto">
                            <a href="{{url('user/'.$usersToFollow[$j]->id)}}" class="mt-3 btn btn-secondary btn-block">Ver perfil<i class="fas fa-external-link-square-alt ml-1"></i></a>
                        </div>
                    </div>
                </div>
                @endfor
            </div>
            {{-- Pagination --}}
            <div class="d-flex justify-content-center mt-4">
                {!! $reviewsCount->links() !!}
            </div>
            @endif
        </div>
        <div class="col-md-3 col-11 mx-auto m-3 bg-gradient text-white h-100 order-md-2 order-1">
        <h2 class="py-3 d-md-none d-block">Bienvenido/a, {{ Auth::user()->name }}</h2>
        @if (Storage::exists('user_images/'.Auth::user()->id.'/profile_pic/User_'.Auth::user()->id.'_profile_pic.png') || Storage::exists('user_images/'.Auth::user()->id.'/profile_pic/User_'.Auth::user()->id.'_profile_pic.jpg') || Storage::exists('user_images/'.Auth::user()->id.'/profile_pic/User_'.Auth::user()->id.'_profile_pic.gif'))
            <div class="py-3 pl-2">
                <a href="{{url('user/'.Auth::user()->id)}}" class="text-white">
                    <img src="{{ asset('storage/'.Auth::user()->profile_pic) }}" alt="Foto de perfil de {{ Auth::user()->name }}" width="50" height="50" class="mb-2 img-thumbnail rounded-circle">
                    <span>{{Auth::user()->name}}<i class="fas fa-external-link-square-alt ml-1"></i></span>
                </a>
            </div>
        @else
            <div class="py-3 pl-2">
                <a href="{{url('user/'.Auth::user()->id)}}" class="text-white">
                    <img src="{{ asset('storage/images/default/Default_profile_pic.png') }}" alt="Foto de perfil de {{ Auth::user()->name }}" width="50" height="50" class="mb-2 img-thumbnail rounded-circle">
                    <span>{{Auth::user()->name}}<i class="fas fa-external-link-square-alt ml-1"></i></span>
                </a>
            </div>
        @endif
        <div class="row mb-3">
            <div class="col">
                <form action="{{ action('App\Http\Controllers\UsersController@getResults') }}" method="POST" autocomplete="off">
                    <div class="input-group">
                        <input type="text" name="userSearch" class="form-control" placeholder="Buscar usuario">
                        {{ csrf_field() }}
                        <div class="input-group-append">
                            <button id="searchBtn" class="btn btn-success" type="submit" disabled><i class="fas fa-search mr-1"></i>Buscar</button>
                        </div>
                    </div>
                    <div id="searchError" class="alert alert-danger alert-dismissible d-none fade mt-1">
                        <button id="searchErrorBtn" type="button" class="close">&times;</button>
                    </div>
                </form>
            </div>
        </div>
        <h4>Gente a la que sigues:</h4>
        @if($followedUsers->count())
        @foreach($followedUsers as $user)
        @if (Storage::exists('user_images/'.$user->id.'/profile_pic/User_'.$user->id.'_profile_pic.png') || Storage::exists('user_images/'.$user->id.'/profile_pic/User_'.$user->id.'_profile_pic.jpg') || Storage::exists('user_images/'.$user->id.'/profile_pic/User_'.$user->id.'_profile_pic.gif'))
            <div class="py-3 pl-2">
                <a href="{{url('user/'.$user->id)}}" class="text-white">
                    <img src="{{ asset('storage/'.$user->profile_pic) }}" alt="Foto de perfil de {{ $user->name }}" width="50" height="50" class="mb-2 img-thumbnail rounded-circle">
                    <span>{{$user->name}}<i class="fas fa-external-link-square-alt ml-1"></i></span>
                </a>
            </div>
        @else
            <div class="py-3 pl-2">
                <a href="{{url('user/'.$user->id)}}" class="text-white">
                    <img src="{{ asset('storage/images/default/Default_profile_pic.png') }}" alt="Foto de perfil de {{ $user->name }}" width="50" height="50" class="mb-2 img-thumbnail rounded-circle">
                    <span>{{$user->name}}<i class="fas fa-external-link-square-alt ml-1"></i></span>
                </a>
            </div>
        @endif
        @endforeach
        @if($followedUsersCount>5)
        <h4 class="text-center">
            <a href="{{url('user/followed/'.Auth::user()->id)}}" class="text-white">
                Ver todos ({{$followedUsersCount}})
            </a>
        </h4>
        @endif
        @else
        <h6>¡Vaya! Aún no sigues a nadie</h6>
        @endif
        </div>
    </div>
</div>

<script>

$(document).ready(function(){

    $('[data-toggle="tooltip"]').tooltip();

    $("input[name='userSearch']").keyup(function() {
        if($(this).val().length>3 && validSearch($(this).val())) {
            $("#searchBtn").removeAttr("disabled");
        }
        else {
            $("#searchBtn").prop("disabled",true);
        }
    });

    function validSearch(query) {
        var regexp = /^[\w ,\.':\-¿?¡!ÁáÉéÍíÓóÚúÑñÀàÈèÌìÒòÙù]{0,100}$/;
        return regexp.test(query);
    }

    $("form").submit(function() {
        if ($("input[name='userSearch']").val() != "" && $("input[name='userSearch']").val().length > 3 && validSearch($("input[name='userSearch']").val())) {
            return true;
        }
        else {
            $("#searchError").removeClass("d-none").addClass("show");
            $("#searchError span:last-child").remove();
            $("#searchError").append("<span>Error en el nombre de búsqueda.</span>");
            return false;
        }
    });

    $("#searchErrorBtn").click(function() {
        $("#searchError").removeClass("show").addClass("d-none");
    });

    $(".usefulnessBtn").click(function() {
        var id = $(this).data('id');
        var status = $(this).data('status');
        var usefulness = $(this).data('usefulness');
        var recommendation = $(this).data("rec");
        var user = $(this).data("user");
        var _token = $('input[name="_token"]').val();
        $.ajax({
            url:"{{ route('recommendation.usefulness') }}",
            method:"POST",
            data:{status:status, usefulness:usefulness, recommendation:recommendation, user:user, _token:_token},
            success:function(status) {
                if (usefulness == "useful") {
                    var contraryId = id.slice(6,7);
                    if (status == "active") {
                        $("#"+id).removeClass("btn-outline-primary").addClass("btn-primary");
                        $("#"+id).removeClass('inactive').addClass('active');
                        if ($("#useless"+contraryId).hasClass("active")) {
                            $("#useless"+contraryId).removeClass("btn-danger").addClass("btn-outline-danger");
                            $("#useless"+contraryId).removeClass('active').addClass('inactive');
                        }
                    }
                    else {
                        $("#"+id).removeClass("btn-primary").addClass("btn-outline-primary");
                        $("#"+id).removeClass('active').addClass('inactive');
                    }
                }
                else {
                    var contraryId = id.slice(7,8);
                    if (status == "active") {
                        $("#"+id).removeClass("btn-outline-danger").addClass("btn-danger");
                        $("#"+id).removeClass('inactive').addClass('active');
                        if ($("#useful"+contraryId).hasClass("active")) {
                            $("#useful"+contraryId).removeClass("btn-primary").addClass("btn-outline-primary");
                            $("#useful"+contraryId).removeClass('active').addClass('inactive');
                        }
                    }
                    else {
                        $("#"+id).removeClass("btn-danger").addClass("btn-outline-danger");
                        $("#"+id).removeClass('active').addClass('inactive');
                    }
                }
            }
        });
    });

});

</script>

@endsection

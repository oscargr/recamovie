<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'REC A Movie')
<img src="https://imgbox.es/images/2021/06/06/Logo3b36bee7953c9ae6.png" class="logo" alt="{{ config('app.name') }} Logo">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>

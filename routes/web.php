<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;
use App\Http\Middleware\AuthorAccess;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CatalogController;
use App\Http\Controllers\RecommendationsController;
use App\Http\Controllers\UsersController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
*   The following routes are for the email verification process.
*
*/

Auth::routes(['verify' => true]);

Route::get('/email/verify', function () {
    return view('auth.verify-email');
})->middleware('auth')->name('verification.notice');

Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();

    return redirect('/home');
})->middleware(['auth', 'signed'])->name('verification.verify');

Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();

    return back()->with('message', '¡Email de verificación enviado!');
})->middleware(['auth', 'throttle:6,1'])->name('verification.send');

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::group(['middleware'=>'auth', 'middleware'=>'is_admin'], function() {

    Route::get('catalog', [CatalogController::class, 'getIndex']);

    Route::get('catalog/create', [CatalogController::class, 'getCreate']);

    Route::post('catalog/create', [CatalogController::class, 'postCreate']);

    Route::get('catalog/show/{id}', [CatalogController::class, 'getShow']);

    Route::get('catalog/edit/{id}', [CatalogController::class, 'getEdit']);

    Route::put('catalog/edit/{id}', [CatalogController::class, 'putEdit']);

    Route::get('catalog/delete/{id}', [CatalogController::class, 'getDelete']);

    Route::delete('catalog/delete/{id}', [CatalogController::class, 'deleteMovie']);

});

Route::group(['middleware'=>'auth'], function() {

    Route::resource('recommendations', RecommendationsController::class)->except([
        'create','edit','update','destroy','show'
    ]);

    Route::get('recommendations/genres/{genre}', [RecommendationsController::class, 'genresIndex']);

    Route::get('recommendations/years/{year}', [RecommendationsController::class, 'yearsIndex']);

    Route::get('recommendations/genres/{genre}/years/{year}', [RecommendationsController::class, 'filteredIndex']);

    Route::get('recommendations/genres/{genre}/user/{year}', [RecommendationsController::class, 'filteredByUser']);

    Route::get('recommendations/create/{movieId}', [RecommendationsController::class, 'create']);

    Route::get('recommendations/edit/{movieId}/{userId}', [RecommendationsController::class, 'edit'])->middleware(AuthorAccess::class);

    Route::put('recommendations/edit/', [RecommendationsController::class, 'update']);

    Route::delete('recommendations', [RecommendationsController::class, 'destroy']);

    Route::post('recommendation/usefulness', [RecommendationsController::class, 'usefulness'])->name('recommendation.usefulness');

    //Route::get('home', [HomeController::class, 'index']);

    Route::get('catalog', [CatalogController::class, 'getIndex']);

    Route::get('catalog/genres/{genre}', [CatalogController::class, 'getGenres']);

    Route::get('catalog/years/{year}', [CatalogController::class, 'getYears']);

    Route::get('catalog/genres/{genre}/years/{year}', [CatalogController::class, 'getFiltered']);

    Route::get('catalog/search', [CatalogController::class, 'getResults'])->name('catalog.search');

    Route::get('catalog/show/{id}', [CatalogController::class, 'getShow']);

    Route::get('user/{userId}', [UsersController::class, 'viewUserProfile']);

    Route::post('user/search', [UsersController::class, 'getResults']);

    Route::post('user/follow', [UsersController::class, 'followUser']);

    Route::delete('user/unfollow', [UsersController::class, 'unfollowUser']);

    Route::get('user/followed/{userId}', [UsersController::class, 'getFollowedUsers'])->middleware(AuthorAccess::class);

    Route::get('user/followers/{userId}', [UsersController::class, 'getFollowers'])->middleware(AuthorAccess::class);

    Route::get('user/edit/{userId}', [UsersController::class, 'edit'])->middleware(AuthorAccess::class);

    Route::put('user/update', [UsersController::class, 'update']);

    Route::delete('user/delete', [UsersController::class, 'delete']);

});

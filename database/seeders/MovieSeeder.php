<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Movie;

class MovieSeeder extends Seeder
{

    /**
     * This is a clear array of a movie
     */

    /*
    array(
        'title'=>'',
        'year'=>'',
        'director'=>'',
        'poster'=>'',
        'synopsis'=>'',
        'duration'=>'',
        'trailer'=>'',
        'ratings'=>array(
            array(
                'key'=>'imdb',
                'value'=>''
            ),
            array(
                'key'=>'filmaffinity',
                'value'=>''
            ),
            array(
                'key'=>'rottentomatoes',
                'value'=>''
            )
        ),
        'screenshots'=>array(
            '',
        )
    ),
    */

    private $movieArray = array(
        array(
            'title'=>'Mandy',
            'year'=>'2018',
            'director'=>'Panos Cosmatos',
            'poster'=>'https://imgbox.es/images/2021/03/28/Mandy-posterd195655c7c2b259e.png',
            'synopsis'=>'Red (Nicholas Cage) es un leñador que vive alejado del mundo junto al amor de su vida, Mandy (Andrea Riseborough). Un día, mientras da un paseo abstraída en una de las novelas de fantasía que suele leer a diario, Mandy se cruza sin saberlo con el líder de una secta que desarrolla una obsesión por ella. Decidido a poseerla a cualquier precio, él y su grupo de secuaces invocan a una banda de motoristas venidos del infierno que la raptan y, en el proceso, hacen añicos la vida de Red. Decidido a vengarse y equipado con toda clase de artilugios, pone en marcha una matanza que deja cuerpos, sangre y vísceras allá por donde pasa.',
            'duration'=>'2h 01min',
            'genre'=>'horror',
            'trailer'=>'rI054ow6KJk',
            'ratings'=>array(
                array(
                    'key'=>'imdb',
                    'value'=>'6.5'
                ),
                array(
                    'key'=>'filmaffinity',
                    'value'=>'5.7'
                ),
                array(
                    'key'=>'rottentomatoes',
                    'value'=>'90'
                )
            ),
            'screenshots'=>array(
                'https://imgbox.es/images/2021/03/28/Screenshot-1a8c154a9461acff5.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-2b7b9b0330650faf2.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-38c26e01d71ecba20.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-48addfc89d4e9e40e.jpg'
            )
        ),
        array(
            'title'=>'Mad Max: Fury Road',
            'year'=>'2015',
            'director'=>'George Miller',
            'poster'=>'https://imgbox.es/images/2021/03/30/Mad-max-fury-road-poster5466b221b5822fc6.jpg',
            'synopsis'=>'Perseguido por su turbulento pasado, Mad Max cree que la mejor forma de sobrevivir es ir solo por el mundo. Sin embargo, se ve arrastrado a formar parte de un grupo que huye a través del desierto en un War Rig conducido por una Emperatriz de élite: Furiosa. Escapan de una Ciudadela tiranizada por Immortan Joe, a quien han arrebatado algo irreemplazable. Enfurecido, el Señor de la Guerra moviliza a todas sus bandas y persigue implacablemente a los rebeldes en una "guerra de la carretera" de altas revoluciones... Cuarta entrega de la saga post-apocalíptica que resucita la trilogía que a principios de los ochenta protagonizó Mel Gibson.',
            'duration'=>'2h 00min',
            'genre'=>'action',
            'trailer'=>'hEJnMQG9ev8',
            'ratings'=>array(
                array(
                    'key'=>'imdb',
                    'value'=>'8.1'
                ),
                array(
                    'key'=>'filmaffinity',
                    'value'=>'7.1'
                ),
                array(
                    'key'=>'rottentomatoes',
                    'value'=>'97'
                )
            ),
            'screenshots'=>array(
                'https://imgbox.es/images/2021/03/30/Screenshot-178ddf6c5519b8912.jpg',
                'https://imgbox.es/images/2021/03/30/Screenshot-2ed165257dab92852.jpg',
                'https://imgbox.es/images/2021/03/30/Screenshot-13f3ebacabb116097.png',
                'https://imgbox.es/images/2021/03/30/Screenshot-30d479bf61009223c.jpg',
                'https://imgbox.es/images/2021/03/30/Screenshot-5102c22ff117d7086.jpg',
                'https://imgbox.es/images/2021/03/30/Screenshot-79d44771aa92a9859.jpg',
                'https://imgbox.es/images/2021/03/30/Screenshot-666ea3477888d0d9a.jpg'
            )
        ),
        array(
            'title'=>'The Lighthouse',
            'year'=>'2019',
            'director'=>'Robert Eggers',
            'poster'=>'https://imgbox.es/images/2021/03/28/The-lighthouse-poster-122ba4a53a633d5d1.jpg',
            'synopsis'=>'Una remota y misteriosa isla de Nueva Inglaterra en la década de 1890. El veterano farero Thomas Wake (Willem Dafoe) y su joven ayudante Ephraim Winslow (Robert Pattinson) deberán convivir durante cuatro semanas. Su objetivo será mantener el faro en buenas condiciones hasta que llegue el relevo que les permita volver a tierra. Pero las cosas se complicarán cuando surjan conflictos por jerarquías de poder entre ambos.',
            'duration'=>'1h 49min',
            'genre'=>'fantasy',
            'trailer'=>'Hyag7lR8CPA',
            'ratings'=>array(
                array(
                    'key'=>'imdb',
                    'value'=>'7.5'
                ),
                array(
                    'key'=>'filmaffinity',
                    'value'=>'6.6'
                ),
                array(
                    'key'=>'rottentomatoes',
                    'value'=>'90'
                )
            ),
            'screenshots'=>array(
                'https://imgbox.es/images/2021/03/28/Screenshot-1422d6ae910dd990e.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-14fbd0ff0b10b0056.png',
                'https://imgbox.es/images/2021/03/28/Screenshot-2408e10d7f1b6a9d4.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-3e21237865cb80ab5.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-4e3fdaa2d7f9c939d.jpg'
            )
        ),
        array(
            'title'=>'Mutafukaz',
            'year'=>'2017',
            'director'=>'Shojiro Nishimi, Guillaume Renard',
            'poster'=>'https://imgbox.es/images/2021/03/30/Mutafukaz-poster014b085fe4f0cb51.jpg',
            'synopsis'=>'Tras un accidente de moto, Angelino, un bala perdida en medio de Dark Meat City, comienza a experimentar violentos dolores de cabeza y extrañas alucinaciones. Al menos eso es lo que él cree, pues quizá sus imaginaciones tienen algo de verdad: unos hombres de negro le persiguen, y Angelino terminará descubriendo algo terrible sobre su propia esencia.',
            'duration'=>'1h 34min',
            'genre'=>'animation',
            'trailer'=>'BHG8WON_MEQ',
            'ratings'=>array(
                array(
                    'key'=>'imdb',
                    'value'=>'6.7'
                ),
                array(
                    'key'=>'filmaffinity',
                    'value'=>'6.5'
                ),
                array(
                    'key'=>'rottentomatoes',
                    'value'=>'39'
                )
            ),
            'screenshots'=>array(
                'https://imgbox.es/images/2021/03/30/Screenshot-1667e8dd0e08550e7.jpg',
                'https://imgbox.es/images/2021/03/30/Screenshot-205f02bb7b764d06e.jpg',
                'https://imgbox.es/images/2021/03/30/Screenshot-3913240c5fdb9ab3d.jpg',
                'https://imgbox.es/images/2021/03/30/Screenshot-4561fd2de0de70608.jpg'
            )
        ),
        array(
            'title'=>'Midsommar',
            'year'=>'2019',
            'director'=>'Ari Aster',
            'poster'=>'https://imgbox.es/images/2021/03/28/Midsommar-postere9dce3417b17c5af.jpg',
            'synopsis'=>'Una pareja estadounidense que no está pasando por su mejor momento acude con unos amigos al Midsommar, un festival de verano que se celebra cada 90 años en una aldea remota de Suecia. Lo que comienza como unas vacaciones de ensueño en un lugar en el que el sol no se pone nunca, poco a poco se convierte en una oscura pesadilla cuando los misteriosos aldeanos les invitan a participar en sus perturbadoras actividades festivas.',
            'duration'=>'2h 28min',
            'genre'=>'horror',
            'trailer'=>'1Vnghdsjmd0',
            'ratings'=>array(
                array(
                    'key'=>'imdb',
                    'value'=>'7.1'
                ),
                array(
                    'key'=>'filmaffinity',
                    'value'=>'6.4'
                ),
                array(
                    'key'=>'rottentomatoes',
                    'value'=>'83'
                )
            ),
            'screenshots'=>array(
                'https://imgbox.es/images/2021/03/28/Screenshot-11b7065f02be43135.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-2e5c170d41ffdda7f.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-3c4b64d5afbf49f5e.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-4fcf8976cc189d70d.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-509c48e610e006965.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-671025840cf61aee0.jpg'
            )
        ),
        array(
            'title'=>'Color out of space',
            'year'=>'2019',
            'director'=>'Richard Stanley',
            'poster'=>'https://imgbox.es/images/2021/03/28/Color-out-of-space-poster6d439bfb14d38ed1.jpg',
            'synopsis'=>'Un meteorito se estrella cerca de la granja de los Gardner, liberando un organismo extraterrestre que convierte la tranquila vida rural de la familia en una pesadilla colorista y alucinógena. Uno de los relatos más emblemáticos de Lovecraft llega al cine de la mano de Nicolas Cage y Richard Stanley.',
            'duration'=>'1h 51min',
            'genre'=>'horror',
            'trailer'=>'agnpaFLo0to',
            'ratings'=>array(
                array(
                    'key'=>'imdb',
                    'value'=>'6.2'
                ),
                array(
                    'key'=>'filmaffinity',
                    'value'=>'5.5'
                ),
                array(
                    'key'=>'rottentomatoes',
                    'value'=>'86'
                )
            ),
            'screenshots'=>array(
                'https://imgbox.es/images/2021/03/28/Screenshot-10ee2ede22f966939.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-28aebaf339a8a534c.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-4dfc010c8a6412b7d.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-58a7eb67e92e94b74.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-35206a1d0c38edb85.jpg'
            )
        ),
        array(
            'title'=>'Upgrade',
            'year'=>'2018',
            'director'=>'Leigh Whannell',
            'poster'=>'https://imgbox.es/images/2021/03/30/Upgrade-poster91a50d41cf349843.png',
            'synopsis'=>'Tras ver cómo su mujer es asesinada tras un accidente que le deja parapléjico, un hombre se somete a una operación que le permite volver a caminar para así poder vengar a su esposa.',
            'duration'=>'1h 40min',
            'genre'=>'sci-fi',
            'trailer'=>'gEnRNIvEKu8',
            'ratings'=>array(
                array(
                    'key'=>'imdb',
                    'value'=>'7.5'
                ),
                array(
                    'key'=>'filmaffinity',
                    'value'=>'6.7'
                ),
                array(
                    'key'=>'rottentomatoes',
                    'value'=>'88'
                )
            ),
            'screenshots'=>array(
                'https://imgbox.es/images/2021/03/30/Screenshot-12857ab6cf6b9f5a8.jpg',
                'https://imgbox.es/images/2021/03/30/Screenshot-3ce5dbe9847209586.png',
                'https://imgbox.es/images/2021/03/30/Screenshot-2f142dd91560c563e.jpg',
                'https://imgbox.es/images/2021/03/30/Screenshot-1a22885a0483f3a87.png',
                'https://imgbox.es/images/2021/03/30/Screenshot-23fc2e38fac0d3178.png',
                'https://imgbox.es/images/2021/03/30/Screenshot-3bb73ebd15134cf24.jpg'
            )
        ),
        array(
            'title'=>'The Matrix',
            'year'=>'1999',
            'director'=>'Lana Wachowski, Lilly Wachowski',
            'poster'=>'https://imgbox.es/images/2021/03/30/The-matrix-poster46e2f7d3caa60d77.jpg',
            'synopsis'=>'Thomas Anderson es un brillante programador de una respetable compañía de software. Pero fuera del trabajo es Neo, un hacker que un día recibe una misteriosa visita...',
            'duration'=>'2h 16min',
            'genre'=>'sci-fi',
            'trailer'=>'D4eJx-0g3Do',
            'ratings'=>array(
                array(
                    'key'=>'imdb',
                    'value'=>'8.7'
                ),
                array(
                    'key'=>'filmaffinity',
                    'value'=>'7.9'
                ),
                array(
                    'key'=>'rottentomatoes',
                    'value'=>'88'
                )
            ),
            'screenshots'=>array(
                'https://imgbox.es/images/2021/03/30/Screenshot-1a980e5549f675b90.png',
                'https://imgbox.es/images/2021/03/30/Screenshot-160c7606bf990abbf.jpg',
                'https://imgbox.es/images/2021/03/30/Screenshot-22b3505eac11a83cd.png',
                'https://imgbox.es/images/2021/03/30/Screenshot-36e294cb8c897c09e.png',
                'https://imgbox.es/images/2021/03/30/Screenshot-4dc97945ac5579af5.png',
                'https://imgbox.es/images/2021/03/30/Screenshot-27acc1e4f206712ff.jpg'
            )
        ),
        array(
            'title'=>'Sorry to bother you',
            'year'=>'2018',
            'director'=>'Boots Riley',
            'poster'=>'https://imgbox.es/images/2021/03/30/Sorry-to-bother-you-poster7e93f5da15cb2b48.jpg',
            'synopsis'=>'Un vendedor telefónico con problemas de autoestima descubre la clave para el éxito en el negocio. Pero cuando empieza a subir escalones en la compañía, sus amigos activistas denuncian prácticas laborales injustas.',
            'duration'=>'1h 52min',
            'genre'=>'comedy',
            'trailer'=>'enH3xA4mYcY',
            'ratings'=>array(
                array(
                    'key'=>'imdb',
                    'value'=>'6.9'
                ),
                array(
                    'key'=>'filmaffinity',
                    'value'=>'6'
                ),
                array(
                    'key'=>'rottentomatoes',
                    'value'=>'93'
                )
            ),
            'screenshots'=>array(
                'https://imgbox.es/images/2021/03/30/Screenshot-1ea4fcf301f88d45e.jpg',
                'https://imgbox.es/images/2021/03/30/Screenshot-2286b76c2a91abb59.jpg',
                'https://imgbox.es/images/2021/03/30/Screenshot-14b1bc5ee05d72076.png',
                'https://imgbox.es/images/2021/03/30/Screenshot-33767590fcd26e387.jpg',
                'https://imgbox.es/images/2021/03/30/Screenshot-48a54bcf65407b435.jpg'
            )
        ),
        array(
            'title'=>'Onward',
            'year'=>'2020',
            'director'=>'Dan Scanlon',
            'poster'=>'https://imgbox.es/images/2021/03/30/Onward-posterf8a200d0f90bf645.jpg',
            'synopsis'=>'Ambientado en un mundo de fantasía suburbana, dos hermanos elfos adolescentes, Ian y Barley Lightfood, se embarcan en una aventura en la que se proponen descubrir si existe aún algo de magia en el mundo que les permita pasar un último día con su padre, que falleció cuando ellos eran aún muy pequeños como para poder recordarlo.',
            'duration'=>'1h 42min',
            'genre'=>'family',
            'trailer'=>'gn5QmllRCn4',
            'ratings'=>array(
                array(
                    'key'=>'imdb',
                    'value'=>'7.4'
                ),
                array(
                    'key'=>'filmaffinity',
                    'value'=>'6.8'
                ),
                array(
                    'key'=>'rottentomatoes',
                    'value'=>'88'
                )
            ),
            'screenshots'=>array(
                'https://imgbox.es/images/2021/03/30/Screenshot-12f08fb6527e11f87.jpg',
                'https://imgbox.es/images/2021/03/30/Screenshot-28adece448ed059dd.jpg',
                'https://imgbox.es/images/2021/03/30/Screenshot-34db88fbcc4050238.jpg',
                'https://imgbox.es/images/2021/03/30/Screenshot-2df6727f50c30ea41.png',
                'https://imgbox.es/images/2021/03/30/Screenshot-122d8d93c12dec87f.png'
            )
        ),
        array(
            'title'=>'Bone Tomahawk',
            'year'=>'2015',
            'director'=>'S. Craig Zahler',
            'poster'=>'https://imgbox.es/images/2021/03/30/Bone-tomahawk-poster5436cfef85f1334b.jpg',
            'synopsis'=>"A la localidad de Bright Hope llega un forastero que rápidamente despierta las sospechas del sheriff, que termina por arrestarlo tras dispararle en la pierna. Samantha O'Dwyer se encarga de extraerle la bala en el calabozo. Pero esa noche un joven en un establo es asesinado y el ayudante del sheriff, la Sra. O'Dwyer y el detenido han desaparecido. Siguiendo la única pista que tiene, una flecha india, el sheriff buscará a la joven con la ayuda de algunos hombres, entre ellos un vaquero y un anciano.",
            'duration'=>'2h 12min',
            'genre'=>'western',
            'trailer'=>'4KNWbfRDEv8',
            'ratings'=>array(
                array(
                    'key'=>'imdb',
                    'value'=>'7.1'
                ),
                array(
                    'key'=>'filmaffinity',
                    'value'=>'6.6'
                ),
                array(
                    'key'=>'rottentomatoes',
                    'value'=>'91'
                )
            ),
            'screenshots'=>array(
                'https://imgbox.es/images/2021/03/30/Screenshot-151b9c52213c67292.jpg',
                'https://imgbox.es/images/2021/03/30/Screenshot-218e0eae360d178cd.jpg',
                'https://imgbox.es/images/2021/03/30/Screenshot-37d5cb25f5cd2aa8c.jpg',
                'https://imgbox.es/images/2021/03/30/Screenshot-13f49fcaa415f2ee9.png',
                'https://imgbox.es/images/2021/03/30/Screenshot-4e7a9487a80f46485.jpg'
            )
        ),
        array(
            'title'=>'In the mouth of madness',
            'year'=>'1994',
            'director'=>'John Carpenter',
            'poster'=>'https://imgbox.es/images/2021/03/30/In-the-mouth-of-madness-posterc93e1044d6a0ed07.jpg',
            'synopsis'=>'El éxito de Sutter Cane, un famoso escritor de novelas de terror, no tiene parangón. Pero, inesperadamente, poco antes de entregar a su editor su última novela, desaparece sin dejar rastro. Al mismo tiempo, algunos de sus fans se están volviéndo inexplicablemente violentos. Para encontrar a Cane, el editor contrata al detective John, que está convencido de que todo es un montaje publicitario para promocionar el próximo libro del novelista; sin embargo, no tardará en descubrir que está completamente equivocado.',
            'duration'=>'1h 35min',
            'genre'=>'horror',
            'trailer'=>'AlugldzO9zY',
            'ratings'=>array(
                array(
                    'key'=>'imdb',
                    'value'=>'7.2'
                ),
                array(
                    'key'=>'filmaffinity',
                    'value'=>'6.6'
                ),
                array(
                    'key'=>'rottentomatoes',
                    'value'=>'58'
                )
            ),
            'screenshots'=>array(
                'https://imgbox.es/images/2021/03/30/Screenshot-1cb4f95d5899a0140.jpg',
                'https://imgbox.es/images/2021/03/30/Screenshot-1a946cea5b0dcdb5e.png',
                'https://imgbox.es/images/2021/03/30/Screenshot-302190e33c4502fdc.png',
                'https://imgbox.es/images/2021/03/30/Screenshot-29cde361f2d1dcc51.png'
            )
        ),
        array(
            'title'=>'Blindness',
            'year'=>'2008',
            'director'=>'Fernando Meirelles',
            'poster'=>'https://imgbox.es/images/2021/03/30/Blindness-poster747731cd624d7bc9.jpg',
            'synopsis'=>'Adaptación de la novela "Ensayo sobre la ceguera", del Premio Nobel portugués José Saramago. Una misteriosa epidemia de ceguera se propaga en todo un país. Las primeras víctimas son recluidas en un hospital sin recibir explicaciones. Entre ellas está una mujer que conserva la vista, pero lo mantiene en secreto para poder acompañar a su marido, que se ha quedado ciego. Dentro del hospital se impone la ley del más fuerte, lo que dará lugar a todo tipo de atrocidades. Mientras tanto, el caos y el terror dominan las calles.',
            'duration'=>'2h 01min',
            'genre'=>'drama',
            'trailer'=>'XvTvP55cxM0',
            'ratings'=>array(
                array(
                    'key'=>'imdb',
                    'value'=>'6.6'
                ),
                array(
                    'key'=>'filmaffinity',
                    'value'=>'6.3'
                ),
                array(
                    'key'=>'rottentomatoes',
                    'value'=>'43'
                )
            ),
            'screenshots'=>array(
                'https://imgbox.es/images/2021/03/30/Screenshot-1f2c42e74a6041ed1.png',
                'https://imgbox.es/images/2021/03/30/Screenshot-122b7b33a0e9b498a.jpg',
                'https://imgbox.es/images/2021/03/30/Screenshot-20ba0d2895fca61d8.png',
                'https://imgbox.es/images/2021/03/30/Screenshot-2f7aa2be6e9431d47.jpg'
            )
        ),
        array(
            'title'=>'Once upon a time... in Hollywood',
            'year'=>'2019',
            'director'=>'Quentin Tarantino',
            'poster'=>'https://imgbox.es/images/2021/03/28/Once-upon-a-time...in-hollywood-posterdcf1fcc5639fb3e0.jpg',
            'synopsis'=>'Hollywood, años 60. La estrella de un western televisivo, Rick Dalton (DiCaprio), intenta amoldarse a los cambios del medio al mismo tiempo que su doble (Pitt). La vida de Dalton está ligada completamente a Hollywood, y es vecino de la joven y prometedora actriz y modelo Sharon Tate (Robbie) que acaba de casarse con el prestigioso director Roman Polanski.',
            'duration'=>'2h 41min',
            'genre'=>'drama',
            'trailer'=>'ELeMaP8EPAA',
            'ratings'=>array(
                array(
                    'key'=>'imdb',
                    'value'=>'7.6'
                ),
                array(
                    'key'=>'filmaffinity',
                    'value'=>'7.1'
                ),
                array(
                    'key'=>'rottentomatoes',
                    'value'=>'85'
                )
            ),
            'screenshots'=>array(
                'https://imgbox.es/images/2021/03/28/Screenshot-19218f93063207ec0.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-126a65698b6fe9cf8.png',
                'https://imgbox.es/images/2021/03/28/Screenshot-2cb105f289c707d4e.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-2cf27ef3ae6c4e2d2.png',
                'https://imgbox.es/images/2021/03/28/Screenshot-344a1cfaab35f67eb.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-4211cdedb9e1798f0.jpg'
            )
        ),
        array(
            'title'=>'The Invisible Man',
            'year'=>'2020',
            'director'=>'Leigh Whannell',
            'poster'=>'https://imgbox.es/images/2021/03/28/The-invisible-man-poster9bc028cb28916419.jpg',
            'synopsis'=>'Cecilia (Elisabeth Moss) rehace su vida tras recibir la noticia de que su exnovio, un maltratador empedernido, ha fallecido. Sin embargo, su cordura comienza a tambalearse cuando empieza tener la certeza de que en realidad sigue vivo.',
            'duration'=>'2h 04min',
            'genre'=>'horror',
            'trailer'=>'WO_FJdiY9dA',
            'ratings'=>array(
                array(
                    'key'=>'imdb',
                    'value'=>'7.1'
                ),
                array(
                    'key'=>'filmaffinity',
                    'value'=>'6.2'
                ),
                array(
                    'key'=>'rottentomatoes',
                    'value'=>'91'
                )
            ),
            'screenshots'=>array(
                'https://imgbox.es/images/2021/03/28/Screenshot-15bcc55a83a018124.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-2cd9cc50b4ca0c9a1.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-3db78927eb1b73fad.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-19037dd29156b6338.png',
                'https://imgbox.es/images/2021/03/28/Screenshot-4ba0265768475e57b.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-5ada1087f36feaa16.jpg'
            )
        ),
        array(
            'title'=>'Guns akimbo',
            'year'=>'2019',
            'director'=>'Jason Lei Howden',
            'poster'=>'https://imgbox.es/images/2021/03/28/Guns-akimbo-poster613abcbec6f76d0d.jpg',
            'synopsis'=>'Miles (Daniel Radcliffe) se siente atascado en la vida: su trabajo no tiene futuro y sigue enamorado de su exnovia Nova. Un día descubre que un grupo de mafiosos llamado "Skizm" planea celebrar una peligrosa competición que reúne a extraños de distintos puntos de la ciudad con el propósito de comprobar cuál de ellos logra una mayor cantidad de espectadores online. Aunque al principio tiene dudas, pronto descubre que Nora ha sido secuestrada por Nix, un grupo armado que participa en el concurso, por lo que Miles decide dejar atrás sus miedos para participar en el torneo.',
            'duration'=>'1h 38min',
            'genre'=>'action',
            'trailer'=>'ZOFatKD0Vzo',
            'ratings'=>array(
                array(
                    'key'=>'imdb',
                    'value'=>'6.3'
                ),
                array(
                    'key'=>'filmaffinity',
                    'value'=>'5.6'
                ),
                array(
                    'key'=>'rottentomatoes',
                    'value'=>'51'
                )
            ),
            'screenshots'=>array(
                'https://imgbox.es/images/2021/03/28/Screenshot-1c521a449b72acff2.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-23d37754d6b928639.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-3e215d799e6959579.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-4ce7faf6e09840239.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-525387e6198986583.png'
            )
        ),
        array(
            'title'=>'Rango',
            'year'=>'2011',
            'director'=>'Gore Verbinski',
            'poster'=>'https://imgbox.es/images/2021/03/28/Rango-posterb2963d5e5ebd3a5e.jpg',
            'synopsis'=>'Rango es un camaleón que llega por accidente al desierto de Mojave. Allí se encontrará en el poblado de "Dirt", donde asola una grave sequía. De nuevo por accidente, se convierte en sheriff del pueblo. ¿Solucionará Rango el problema del agua? Film de animación del director de "Piratas del Caribe", con las voces originales de Johnny Depp (Rango), Abigail Breslin, Isla Fisher y Bill Nighy.',
            'duration'=>'1h 47min',
            'genre'=>'animation',
            'trailer'=>'DDgoDooApwM',
            'ratings'=>array(
                array(
                    'key'=>'imdb',
                    'value'=>'7.2'
                ),
                array(
                    'key'=>'filmaffinity',
                    'value'=>'6.4'
                ),
                array(
                    'key'=>'rottentomatoes',
                    'value'=>'88'
                )
            ),
            'screenshots'=>array(
                'https://imgbox.es/images/2021/03/28/Screenshot-183ff4679ee476be8.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-204bcf55a0c90bc90.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-30dc53caf3a7fe10d.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-4955578b3a1cb09fc.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-5e4bbb9ce82bcbabc.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-675484c345447cb57.jpg'
            )
        ),
        array(
            'title'=>'Tenet',
            'year'=>'2020',
            'director'=>'Christopher Nolan',
            'poster'=>'https://imgbox.es/images/2021/03/28/Tenet-posterf21506a5163ff9ef.jpg',
            'synopsis'=>'Armado con tan solo una palabra –Tenet– el protagonista de esta historia deberá pelear por la supervivencia del mundo entero en una misión que le lleva a viajar a través del oscuro mundo del espionaje internacional, y cuya experiencia se desdoblará más allá del tiempo lineal.',
            'duration'=>'2h 30min',
            'genre'=>'thriller',
            'trailer'=>'LdOM0x0XDMo',
            'ratings'=>array(
                array(
                    'key'=>'imdb',
                    'value'=>'7.5'
                ),
                array(
                    'key'=>'filmaffinity',
                    'value'=>'6.4'
                ),
                array(
                    'key'=>'rottentomatoes',
                    'value'=>'70'
                )
            ),
            'screenshots'=>array(
                'https://imgbox.es/images/2021/03/28/Screenshot-1ddd9ea891f1ecacf.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-228ab00527b696951.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-3787fdb1822bf2e4b.jpg',
                'https://imgbox.es/images/2021/03/28/Screenshot-41a52445afd1557a7.jpg'
            )
        ),
    );

    private function seedCatalog() {
        DB::table('movies')->delete();

        foreach($this->movieArray as $movie) {
            $m = new Movie;
            $m->title = $movie['title'];
            $m->year = $movie['year'];
            $m->director = $movie['director'];
            $m->poster = $movie['poster'];
            $m->synopsis = $movie['synopsis'];
            $m->duration = $movie['duration'];
            $m->genre = $movie['genre'];
            $m->trailer = $movie['trailer'];
            $m->ratings = $movie['ratings'];
            $m->screenshots = $movie['screenshots'];
            $m->save();
        }

    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedCatalog();
        $this->command->info("Movies' table seeded with exit.");
    }
}

<?php

namespace Database\Seeders;

use App\Models\Subscription;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FollowsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('subscriptions')->delete();

        Subscription::create([
            'creatorId' => 2,
            'subscriberId' => 3,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        for ($i=3; $i<=10; $i++) {

            Subscription::create([
                'creatorId' => $i,
                'subscriberId' => 2,
                'created_at' => now(),
                'updated_at' => now()
            ]);

        }

        $this->command->info("Subscriptions' table seeded with exit.");

    }
}

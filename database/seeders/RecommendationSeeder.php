<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Recommendation;
use App\Models\Movie;
use App\Models\User;

class RecommendationSeeder extends Seeder
{

    private function generateRecommendations() {

        $recommendationArray = array();
        $countMovies = count(Movie::all());
        $countUsers = count(User::all());

        $movieRange = range(1, $countMovies);
        $userRange = range(2, $countUsers);

        shuffle($movieRange);
        shuffle($userRange);

        for ($i=0; $i<9; $i++) {
            $movieId = $movieRange[$i];
            $userId = $userRange[$i];
            $rating = mt_rand(5, 10);
            $recommendation = array(
                'movieId'=>$movieId,
                'userId'=>$userId,
                'rating'=>$rating,
                'headline'=>'Lorem ipsum',
                'review'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Leo vel orci porta non pulvinar neque laoreet. Aliquam ut porttitor leo a diam sollicitudin. Faucibus in ornare quam viverra orci sagittis eu. Integer quis auctor elit sed vulputate mi sit. Aenean sed adipiscing diam donec adipiscing tristique risus nec. Risus in hendrerit gravida rutrum quisque. Risus commodo viverra maecenas accumsan lacus vel. Enim praesent elementum facilisis leo vel fringilla. Augue mauris augue neque gravida in fermentum et sollicitudin. Vitae elementum curabitur vitae nunc sed velit dignissim sodales. Integer vitae justo eget magna fermentum iaculis eu non. Donec ac odio tempor orci. Eget nulla facilisi etiam dignissim diam. Aliquet sagittis id consectetur purus ut faucibus pulvinar. Quisque sagittis purus sit amet volutpat consequat mauris. Mi bibendum neque egestas congue quisque egestas diam in arcu.',
                'useful'=>'0',
                'useless'=>'0'
            );
            array_push($recommendationArray, $recommendation);
        }

        return $recommendationArray;

    }

    private function seedRecommendations() {
        DB::table('recommendations')->delete();

        $recommendationArray = $this->generateRecommendations();

        foreach($recommendationArray as $recommendation) {
            $r = new Recommendation;
            $r->movieId = $recommendation['movieId'];
            $r->userId = $recommendation['userId'];
            $r->rating = $recommendation['rating'];
            $r->headline = $recommendation['headline'];
            $r->review = $recommendation['review'];
            $r->useful = $recommendation['useful'];
            $r->useless = $recommendation['useless'];
            $r->save();
        }

    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedRecommendations();
        $this->command->info("Recommendations' table seeded with exit.");
    }
}

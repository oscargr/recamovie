<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        User::create([
            'name'=>'Óscar Gil Riesgo',
            'email'=>'oscargil7196@gmail.com',
            'email_verified_at'=>Carbon::now(),
            'password'=>Hash::make('test1234'),
            'role'=>'admin'
        ]);

        $faker = Faker::create();
        
        foreach (range(1, 9) as $index) {
            DB::table('users')->insert([
                'name'=>$faker->name,
                'email'=>'user'.$index.'@gmail.com',
                'email_verified_at'=>Carbon::now(),
                'password'=>Hash::make('test1234'),
                'role'=>'user',
                'profile_pic'=>'storage/images/default/Default_profile_pic.png',
                'banner_pic'=>'storage/images/default/Default_profile_background.jpg',
                'created_at'=>now(),
                'updated_at'=>now()
            ]);
        }

        $this->command->info("Users' table seeded with exit.");

    }
}

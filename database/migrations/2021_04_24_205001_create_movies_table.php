<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->id();
            $table->string('title', 100);
            $table->string('year', 8);
            $table->string('director', 100);
            $table->string('poster', 500);
            $table->text('synopsis');
            $table->string('duration', 100);
            $table->enum('genre', ['action','animation','adventure','biography','war','sci-fi','comedy','documentary','drama','family','fantasy','musical','thriller','horror','western']);
            $table->json('ratings');
            $table->string('trailer', 500);
            $table->json('screenshots');
            $table->unique(['title','year','director']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecommendationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recommendations', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('movieId')->unsigned();
            $table->bigInteger('userId')->unsigned();
            $table->string('rating');
            $table->string('headline')->nullable();
            $table->text('review');
            $table->bigInteger('useful')->default(0);
            $table->bigInteger('useless')->default(0);
            $table->unique(['movieId','userId']);
            $table->timestamps();
            $table->foreign('movieId')->references('id')->on('movies')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('userId')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recommendations');
    }
}

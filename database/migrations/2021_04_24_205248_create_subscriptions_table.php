<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->bigInteger('creatorId')->unsigned();
            $table->bigInteger('subscriberId')->unsigned();
            $table->primary(['creatorId','subscriberId']);
            $table->timestamps();
            $table->foreign('creatorId')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('subscriberId')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}

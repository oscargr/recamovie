<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movie;
use App\Models\Recommendation;
use App\Models\User;
use App\Models\Utility;
use App\Rules\Director;
use App\Rules\Duration;
use App\Rules\Text;
use App\Rules\Title;
use App\Rules\Trailer;
use App\Rules\Year;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

use function PHPUnit\Framework\returnSelf;

class CatalogController extends Controller
{

    /**
     * The following function return us the movie creation view.
     */
    public function getCreate() {

        $genres = array(
            'action'=>'Acción',
            'animation'=>'Animación',
            'adventure'=>'Aventuras',
            'biography'=>'Biográfica',
            'war'=>'Bélica',
            'sci-fi'=>'Ciencia ficción',
            'comedy'=>'Comedia',
            'documentary'=>'Documental',
            'drama'=>'Drama',
            'family'=>'Familiar',
            'fantasy'=>'Fantasía',
            'musical'=>'Musical',
            'thriller'=>'Suspense',
            'horror'=>'Terror',
            'western'=>'Western',
        );

        return view('catalog.create', compact('genres'));
    }

    /**
     * The following function creates the movie and inserts it in the database.
     */
    public function postCreate(Request $request) {

        $validated = $request->validate([
            'title' => ['required', new Title],
            'year' => ['required', new Year],
            'director' => ['required', new Director],
            'poster' => ['required','url'],
            'duration' => ['required', new Duration],
            'trailer' => ['required', new Trailer],
            'synopsis' => ['required', new Text],
            'genre' => ['required'],
            'ratings' => ['array','min:1'],
            'ratings.*' => ['required'],
            'screenshots' => ['array', 'min:1'],
            'screenshots.*' => ['required','url','distinct']
        ]);

        try {
            $movie = Movie::create($request->all());
            return redirect('catalog');
        }
        catch (\Illuminate\Database\QueryException $e) {
            return Redirect::back()->withErrors("La película \"".$request->input('title')."\" (".$request->input('year').") de ".$request->input('director').", ya existe en nuestra base de datos.")->withInput();
        }
    }

    /**
     * The following function returns us the index of movies view.
     */
    public function getIndex() {

        $genres = array(
            'action'=>'Acción',
            'animation'=>'Animación',
            'adventure'=>'Aventuras',
            'biography'=>'Biográfica',
            'war'=>'Bélica',
            'sci-fi'=>'Ciencia ficción',
            'comedy'=>'Comedia',
            'documentary'=>'Documental',
            'drama'=>'Drama',
            'family'=>'Familiar',
            'fantasy'=>'Fantasía',
            'musical'=>'Musical',
            'thriller'=>'Suspense',
            'horror'=>'Terror',
            'western'=>'Western',
        );

        $years = array();
        
        $movieYears = DB::table('movies')->select('year')->distinct()->orderByDesc('year')->get();

        foreach($movieYears as $movieYear) {
            array_push($years, $movieYear->year);
        }

        $movies = Movie::latest('updated_at')->paginate(8);

        return view('catalog.index',compact('movies','genres','years'));
    }

    /**
     * The following function returns us the index of movies for each genre.
     */

    public function getGenres($genre) {

        $genres = array(
            'action'=>'Acción',
            'animation'=>'Animación',
            'adventure'=>'Aventuras',
            'biography'=>'Biográfica',
            'war'=>'Bélica',
            'sci-fi'=>'Ciencia ficción',
            'comedy'=>'Comedia',
            'documentary'=>'Documental',
            'drama'=>'Drama',
            'family'=>'Familiar',
            'fantasy'=>'Fantasía',
            'musical'=>'Musical',
            'thriller'=>'Suspense',
            'horror'=>'Terror',
            'western'=>'Western',
        );

        if (!isset($genres[$genre])) {
            return back();
        }

        $moviesGenre = $genres[$genre];

        $moviesCountByGenre = array();

        $movies = Movie::latest('updated_at')->where('genre','=',$genre)->paginate(6);

        foreach($genres as $genreId=>$genre) {
            $count = Movie::where('genre','=',$genreId)->count();
            $moviesCountByGenre[$genreId]=$count;
        }

        return view('catalog.genres', compact('movies','moviesGenre','genres','moviesCountByGenre'));

    }

    /**
     * The following function returns us the index of movies for each genre.
     */

    public function getYears($year) {

        $genres = array(
            'action'=>'Acción',
            'animation'=>'Animación',
            'adventure'=>'Aventuras',
            'biography'=>'Biográfica',
            'war'=>'Bélica',
            'sci-fi'=>'Ciencia ficción',
            'comedy'=>'Comedia',
            'documentary'=>'Documental',
            'drama'=>'Drama',
            'family'=>'Familiar',
            'fantasy'=>'Fantasía',
            'musical'=>'Musical',
            'thriller'=>'Suspense',
            'horror'=>'Terror',
            'western'=>'Western',
        );

        $intYear = (int) $year;

        if ($intYear < 1833 || $intYear > date("Y")) {
            return back();
        }

        $years = array();
        
        $movieYears = DB::table('movies')->select('year')->distinct()->orderByDesc('year')->get();

        foreach($movieYears as $movieYear) {
            array_push($years, $movieYear->year);
        }

        $movies = Movie::latest('updated_at')->where('year','=',$year)->paginate(8);

        return view('catalog.years', compact('movies','intYear','genres','years'));

    }

    /**
     * The following function returns us the index of movies for each genre.
     */

    public function getFiltered($genre, $year) {

        $genres = array(
            'action'=>'Acción',
            'animation'=>'Animación',
            'adventure'=>'Aventuras',
            'biography'=>'Biográfica',
            'war'=>'Bélica',
            'sci-fi'=>'Ciencia ficción',
            'comedy'=>'Comedia',
            'documentary'=>'Documental',
            'drama'=>'Drama',
            'family'=>'Familiar',
            'fantasy'=>'Fantasía',
            'musical'=>'Musical',
            'thriller'=>'Suspense',
            'horror'=>'Terror',
            'western'=>'Western',
        );

        $intYear = (int) $year;

        if (!isset($genres[$genre]) || $intYear < 1833 || $intYear > date("Y")) {
            return back();
        }

        $moviesGenre = $genres[$genre];

        $movies = Movie::latest('updated_at')->where('genre','=',$genre)->where('year','=',$year)->paginate(8);

        return view('catalog.filtered', compact('movies','intYear','genres','moviesGenre'));

    }

    /**
     * The following function returns us the search results.
     */
    public function getResults(Request $request) {
        if($request->get('query')) {
            $query = $request->get('query');
            $data = DB::table('movies')
            ->where('title', 'LIKE', "%{$query}%")
            ->get();
            if ($data->count()>0) {
                $output = '<ul class="dropdown-menu w-100 shadow-lg" style="display:block; position:absolute">';
                foreach($data as $row) {
                    $output .= '
                    <a href="catalog/show/'.$row->id.'"><li class="dropdown-item"><img src="'.$row->poster.'" height="100" alt="Portada de '.$row->title.'"> - "'.$row->title.'" ('.$row->year.')</li></a>
                    ';
                }
                $output .= '</ul>';
            }
            else {
                $output = '<ul class="dropdown-menu w-100 shadow-lg" style="display:block; position:absolute">';
                $output .= '<li class="text-center">No se ha encontrado ningún resultado</li>';
                $output .= '</ul>';
            }
            echo $output;
        }
    }

    /**
     * The following function returns us the view of the movie with the id we sent.
     */
    public function getShow($id) {
        $movie = Movie::find($id);
        if (is_null($movie)) {
            return back();
        }
        $recommendations = Recommendation::where('movieId','=',$id)->latest()->paginate(5);
        $userUsefulness = Utility::where('userId','=',Auth::user()->id)->get();

        $users = array();

        $usefulness = array();

        $valorations = array();

        $totalVal = array();

        foreach ($recommendations as $recommendation) {
            $user = User::find($recommendation['userId']);
            $useful = $recommendation->useful;
            $total = $recommendation->useful + $recommendation->useless;
            if ($total>0) {
                $percentage = round(($useful/$total)*100);
            }
            else {
                $percentage = 0;
            }
            array_push($users, $user);
            array_push($valorations, $percentage);
            array_push($totalVal, $total);
            foreach ($userUsefulness as $use) {
                if ($recommendation['id'] == $use['recommendationId']) {
                    $usefulness[$recommendation['id']] = $use;
                }
            }
        }

        $genres = array(
            'action'=>'Acción',
            'animation'=>'Animación',
            'adventure'=>'Aventuras',
            'biography'=>'Biográfica',
            'war'=>'Bélica',
            'sci-fi'=>'Ciencia ficción',
            'comedy'=>'Comedia',
            'documentary'=>'Documental',
            'drama'=>'Drama',
            'family'=>'Familiar',
            'fantasy'=>'Fantasía',
            'musical'=>'Musical',
            'thriller'=>'Suspense',
            'horror'=>'Terror',
            'western'=>'Western',
        );

        return view('catalog.show', compact('movie','recommendations','users','usefulness','valorations','totalVal','genres'));
    }

    /**
     * The following function returns us the movie edit view.
     */
    public function getEdit($id) {

        $genres = array(
            'action'=>'Acción',
            'animation'=>'Animación',
            'adventure'=>'Aventuras',
            'biography'=>'Biográfica',
            'war'=>'Bélica',
            'sci-fi'=>'Ciencia ficción',
            'comedy'=>'Comedia',
            'documentary'=>'Documental',
            'drama'=>'Drama',
            'family'=>'Familiar',
            'fantasy'=>'Fantasía',
            'musical'=>'Musical',
            'thriller'=>'Suspense',
            'horror'=>'Terror',
            'western'=>'Western',
        );

        $movie = Movie::find($id);
        if (is_null($movie)) {
            return back();
        }

        return view('catalog.edit',array('movie'=>$movie, 'id'=>$id, 'genres'=>$genres));
    }

    /**
     * The following function edits the movie with the id we sent and updates it in the database.
     */
    public function putEdit(Request $request, $id) {

        $validated = $request->validate([
            'title' => ['required', new Title],
            'year' => ['required', new Year],
            'director' => ['required', new Director],
            'poster' => ['required','url'],
            'duration' => ['required', new Duration],
            'trailer' => ['required', new Trailer],
            'synopsis' => ['required', new Text],
            'genre' => ['required'],
            'ratings' => ['array','min:1'],
            'ratings.*' => ['required'],
            'screenshots' => ['array', 'min:1'],
            'screenshots.*' => ['required','url','distinct']
        ]);
        
        try {
            $movie = Movie::find($id);
            $movie->title = $request->input('title');
            $movie->year = $request->input('year');
            $movie->director = $request->input('director');
            $movie->poster = $request->input('poster');
            $movie->duration = $request->input('duration');
            $movie->trailer = $request->input('trailer');
            $movie->synopsis = $request->input('synopsis');
            $movie->genre = $request->input('genre');
            $movie->ratings = $request->input('ratings');
            $movie->screenshots = $request->input('screenshots');
            $movie->save();
            return redirect('catalog/show/'.$id);
        }
        catch (\Illuminate\Database\QueryException $e) {
            return Redirect::back()->withErrors("La película \"".$request->input('title')."\" (".$request->input('year').") de ".$request->input('director').", ya existe en nuestra base de datos.")->withInput();
        }
    }

    /**
     * The following function returns us the movie edit view.
     */
    public function getDelete($id) {
        $movie = Movie::find($id);
        if (is_null($movie)) {
            return back();
        }
        return view('catalog.delete',array('movie'=>$movie, 'id'=>$id));
    }

    /**
     * The following function deletes the movie with the id we sent from the database.
     */
    public function deleteMovie($id) {
        $movie = Movie::find($id);
        $movie->delete();
        return redirect('catalog');
    }


}

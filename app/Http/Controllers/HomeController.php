<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Movie;
use App\Models\User;
use App\Models\Utility;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        if (Auth::user()->role=='admin') {
            return redirect('catalog');
        }

        if (Auth::user()->role=='user') {
            $recommendations = DB::table('recommendations')
            ->join('subscriptions','recommendations.userId','=','subscriptions.creatorId')
            ->where('subscriptions.subscriberId','=',Auth::user()->id)
            ->latest('recommendations.updated_at')
            ->paginate(5);

            $reviewsCount = DB::table('recommendations')
            ->selectRaw('COUNT(recommendations.id) AS reviewsCount, recommendations.userId')
            ->where('recommendations.userId','!=',Auth::user()->id)
            ->groupByRaw('recommendations.userId')
            ->orderByRaw('reviewsCount DESC')
            ->paginate(3);

            $usersToFollow = array();

            $followers = array();

            foreach ($reviewsCount as $count) {
                $userToFollow = User::find($count->userId);
                array_push($usersToFollow, $userToFollow);
                $followerCount = DB::table('subscriptions')->where('creatorId','=',$count->userId)->count();
                array_push($followers, $followerCount);
            }

            $followedUsers = User::join('subscriptions','users.id','=','subscriptions.creatorId')->where('subscriptions.subscriberId','=',Auth::user()->id)->latest('subscriptions.updated_at')->limit(5)->get();

            $followedUsersCount = User::join('subscriptions','users.id','=','subscriptions.creatorId')->where('subscriptions.subscriberId','=',Auth::user()->id)->count();

            $userUsefulness = Utility::where('userId','=',Auth::user()->id)->get();

            $movies = array();

            $users = array();

            $usefulness = array();

            $valorations = array();

            $totalVal = array();

            foreach ($recommendations as $recommendation) {
                $movie = Movie::find($recommendation->movieId);
                $user = User::find($recommendation->userId);
                $useful = $recommendation->useful;
                $total = $recommendation->useful + $recommendation->useless;
                if ($total>0) {
                    $percentage = round(($useful/$total)*100);
                }
                else {
                    $percentage = 0;
                }
                array_push($movies, $movie);
                array_push($users, $user);
                array_push($valorations, $percentage);
                array_push($totalVal, $total);
                foreach ($userUsefulness as $use) {
                    if ($recommendation->id == $use['recommendationId']) {
                        $usefulness[$recommendation->id] = $use;
                    }
                }
            }

            return view('home', compact('recommendations','followedUsers','followedUsersCount','movies','users','usefulness','valorations','totalVal','usersToFollow','reviewsCount','followers'));
        }

    }
}

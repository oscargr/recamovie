<?php

namespace App\Http\Controllers;

use App\Models\Recommendation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Models\Movie;
use App\Models\User;
use App\Models\Utility;
use App\Rules\Headline;
use App\Rules\Text;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

class RecommendationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $genres = array(
            'action'=>'Acción',
            'animation'=>'Animación',
            'adventure'=>'Aventuras',
            'biography'=>'Biográfica',
            'war'=>'Bélica',
            'sci-fi'=>'Ciencia ficción',
            'comedy'=>'Comedia',
            'documentary'=>'Documental',
            'drama'=>'Drama',
            'family'=>'Familiar',
            'fantasy'=>'Fantasía',
            'musical'=>'Musical',
            'thriller'=>'Suspense',
            'horror'=>'Terror',
            'western'=>'Western',
        );

        $years = array();
        
        $movieYears = DB::table('movies')->select('year')->distinct()->orderByDesc('year')->get();

        foreach($movieYears as $movieYear) {
            array_push($years, $movieYear->year);
        }

        $recommendations = Recommendation::latest('updated_at')->paginate(5);

        $userUsefulness = Utility::where('userId','=',Auth::user()->id)->get();

        $movies = array();

        $users = array();

        $usefulness = array();

        $valorations = array();

        $totalVal = array();

        foreach ($recommendations as $recommendation) {
            $movie = Movie::find($recommendation['movieId']);
            $user = User::find($recommendation['userId']);
            $useful = $recommendation->useful;
            $total = $recommendation->useful + $recommendation->useless;
            if ($total>0) {
                $percentage = round(($useful/$total)*100);
            }
            else {
                $percentage = 0;
            }
            array_push($movies, $movie);
            array_push($users, $user);
            array_push($valorations, $percentage);
            array_push($totalVal, $total);
            foreach ($userUsefulness as $use) {
                if ($recommendation['id'] == $use['recommendationId']) {
                    $usefulness[$recommendation['id']] = $use;
                }
            }
        }

        return view('recommendations.index', compact('recommendations','movies','users','usefulness','valorations','totalVal','genres','years'));
    }

    /**
     * Display a listing of the resource by genre.
     *
     * @return \Illuminate\Http\Response
     */
    public function genresIndex($genre)
    {

        $genres = array(
            'action'=>'Acción',
            'animation'=>'Animación',
            'adventure'=>'Aventuras',
            'biography'=>'Biográfica',
            'war'=>'Bélica',
            'sci-fi'=>'Ciencia ficción',
            'comedy'=>'Comedia',
            'documentary'=>'Documental',
            'drama'=>'Drama',
            'family'=>'Familiar',
            'fantasy'=>'Fantasía',
            'musical'=>'Musical',
            'thriller'=>'Suspense',
            'horror'=>'Terror',
            'western'=>'Western',
        );

        if (!isset($genres[$genre])) {
            return back();
        }

        $moviesGenre = $genres[$genre];

        $recommendations = DB::table('recommendations')->join('movies','recommendations.movieId','=','movies.id')->select('recommendations.*')->where('movies.genre','=',$genre)->orderByDesc('updated_at')->paginate(5);

        $userUsefulness = Utility::where('userId','=',Auth::user()->id)->get();

        $movies = array();

        $users = array();

        $usefulness = array();

        $valorations = array();

        $totalVal = array();

        $recommendationsCountByGenre = array();

        foreach ($recommendations as $recommendation) {
            $movie = Movie::find($recommendation->movieId);
            $user = User::find($recommendation->userId);
            $useful = $recommendation->useful;
            $total = $recommendation->useful + $recommendation->useless;
            if ($total>0) {
                $percentage = round(($useful/$total)*100);
            }
            else {
                $percentage = 0;
            }
            array_push($movies, $movie);
            array_push($users, $user);
            array_push($valorations, $percentage);
            array_push($totalVal, $total);
            foreach ($userUsefulness as $use) {
                if ($recommendation->id == $use['recommendationId']) {
                    $usefulness[$recommendation->id] = $use;
                }
            }
        }

        foreach($genres as $genreId=>$genre) {
            $count = DB::table('recommendations')->join('movies','recommendations.movieId','=','movies.id')->where('movies.genre','=',$genreId)->count();
            $recommendationsCountByGenre[$genreId]=$count;
        }

        return view('recommendations.genres', compact('recommendations','movies','users','usefulness','valorations','totalVal','moviesGenre','genres','recommendationsCountByGenre'));
    }

    /**
     * Display a listing of the resource by year.
     *
     * @return \Illuminate\Http\Response
     */
    public function yearsIndex($year)
    {

        $intYear = (int) $year;

        if ($intYear < 1833 || $intYear > date("Y")) {
            return back();
        }

        $years = array();
        
        $movieYears = DB::table('movies')->select('year')->distinct()->orderByDesc('year')->get();

        foreach($movieYears as $movieYear) {
            array_push($years, $movieYear->year);
        }

        $recommendations = DB::table('recommendations')->join('movies','recommendations.movieId','=','movies.id')->select('recommendations.*')->where('movies.year','=',$year)->orderByDesc('updated_at')->paginate(5);

        $userUsefulness = Utility::where('userId','=',Auth::user()->id)->get();

        $movies = array();

        $users = array();

        $usefulness = array();

        $valorations = array();

        $totalVal = array();

        foreach ($recommendations as $recommendation) {
            $movie = Movie::find($recommendation->movieId);
            $user = User::find($recommendation->userId);
            $useful = $recommendation->useful;
            $total = $recommendation->useful + $recommendation->useless;
            if ($total>0) {
                $percentage = round(($useful/$total)*100);
            }
            else {
                $percentage = 0;
            }
            array_push($movies, $movie);
            array_push($users, $user);
            array_push($valorations, $percentage);
            array_push($totalVal, $total);
            foreach ($userUsefulness as $use) {
                if ($recommendation->id == $use['recommendationId']) {
                    $usefulness[$recommendation->id] = $use;
                }
            }
        }

        return view('recommendations.years', compact('recommendations','movies','users','usefulness','valorations','totalVal','intYear','years'));
    }

    /**
     * Display a listing of the resource by genre and year.
     *
     * @return \Illuminate\Http\Response
     */
    public function filteredIndex($genre, $year)
    {

        $genres = array(
            'action'=>'Acción',
            'animation'=>'Animación',
            'adventure'=>'Aventuras',
            'biography'=>'Biográfica',
            'war'=>'Bélica',
            'sci-fi'=>'Ciencia ficción',
            'comedy'=>'Comedia',
            'documentary'=>'Documental',
            'drama'=>'Drama',
            'family'=>'Familiar',
            'fantasy'=>'Fantasía',
            'musical'=>'Musical',
            'thriller'=>'Suspense',
            'horror'=>'Terror',
            'western'=>'Western',
        );

        if (!isset($genres[$genre])) {
            return back();
        }

        $moviesGenre = $genres[$genre];

        $intYear = (int) $year;

        if ($intYear < 1833 || $intYear > date("Y")) {
            return back();
        }

        $recommendations = DB::table('recommendations')->join('movies','recommendations.movieId','=','movies.id')->select('recommendations.*')->where('movies.genre','=',$genre)->where('movies.year','=',$year)->orderByDesc('updated_at')->paginate(5);

        $userUsefulness = Utility::where('userId','=',Auth::user()->id)->get();

        $movies = array();

        $users = array();

        $usefulness = array();

        $valorations = array();

        $totalVal = array();

        foreach ($recommendations as $recommendation) {
            $movie = Movie::find($recommendation->movieId);
            $user = User::find($recommendation->userId);
            $useful = $recommendation->useful;
            $total = $recommendation->useful + $recommendation->useless;
            if ($total>0) {
                $percentage = round(($useful/$total)*100);
            }
            else {
                $percentage = 0;
            }
            array_push($movies, $movie);
            array_push($users, $user);
            array_push($valorations, $percentage);
            array_push($totalVal, $total);
            foreach ($userUsefulness as $use) {
                if ($recommendation->id == $use['recommendationId']) {
                    $usefulness[$recommendation->id] = $use;
                }
            }
        }

        return view('recommendations.filtered', compact('recommendations','movies','users','usefulness','valorations','totalVal','moviesGenre','intYear'));
    }

    /**
     * Display a listing of the resource by genre and user
     */

    public function filteredByUser($genre, $userId) {

        $genres = array(
            'action'=>'Acción',
            'animation'=>'Animación',
            'adventure'=>'Aventuras',
            'biography'=>'Biográfica',
            'war'=>'Bélica',
            'sci-fi'=>'Ciencia ficción',
            'comedy'=>'Comedia',
            'documentary'=>'Documental',
            'drama'=>'Drama',
            'family'=>'Familiar',
            'fantasy'=>'Fantasía',
            'musical'=>'Musical',
            'thriller'=>'Suspense',
            'horror'=>'Terror',
            'western'=>'Western',
        );

        if (!isset($genres[$genre])) {
            return back();
        }

        $moviesGenre = $genres[$genre];

        $recommendations = DB::table('recommendations')->join('movies','recommendations.movieId','=','movies.id')->select('recommendations.*')->where('movies.genre','=',$genre)->where('recommendations.userId','=',$userId)->orderByDesc('updated_at')->paginate(5);

        if ($recommendations->total()==0) {
            return back();
        }

        $userUsefulness = Utility::where('userId','=',Auth::user()->id)->get();

        $movies = array();

        $users = array();

        $usefulness = array();

        $valorations = array();

        $totalVal = array();

        foreach ($recommendations as $recommendation) {
            $movie = Movie::find($recommendation->movieId);
            $user = User::find($recommendation->userId);
            $useful = $recommendation->useful;
            $total = $recommendation->useful + $recommendation->useless;
            if ($total>0) {
                $percentage = round(($useful/$total)*100);
            }
            else {
                $percentage = 0;
            }
            array_push($movies, $movie);
            array_push($users, $user);
            array_push($valorations, $percentage);
            array_push($totalVal, $total);
            foreach ($userUsefulness as $use) {
                if ($recommendation->id == $use['recommendationId']) {
                    $usefulness[$recommendation->id] = $use;
                }
            }
        }

        return view('recommendations.byuser', compact('recommendations','movies','users','usefulness','valorations','totalVal','moviesGenre'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($movieId)
    {
        $movie = Movie::find($movieId);
        if (is_null($movie)) {
            return back();
        }
        return view('recommendations.create', array('movieId'=>$movieId, 'movie'=>$movie));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validated = $request->validate([
            'rating' => ['required'],
            'headline' => [new Headline],
            'review' => ['required', new Text],
        ]);

        try {
            $cryptedMovieId = $request->input('movieId');
            $movieId = Crypt::decryptString($cryptedMovieId);
            $cryptedUserId = $request->input('userId');
            $userId = Crypt::decryptString($cryptedUserId);
            $rating = $request->input('rating');
            $headline = $request->input('headline');
            $review = $request->input('review');
            DB::table('recommendations')->insert([
                'movieId' => $movieId,
                'userId' => $userId,
                'rating' => $rating,
                'headline' => $headline,
                'review' => $review,
                'created_at' => now(),
                'updated_at' => now()
            ]);
            return redirect('recommendations');
        }
        catch (\Illuminate\Database\QueryException $e) {
            return Redirect::back()->withErrors("Ya has escrito una reseña de esta película.")->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Recommendation  $recommendation
     * @return \Illuminate\Http\Response
     */
    public function edit($movieId, $userId)
    {
        $recommendation = Recommendation::where('movieId','=',$movieId)->where('userId','=',$userId)->first();
        $movie = Movie::find($movieId);
        if (is_null($recommendation) || is_null($movie)) {
            return back();
        }
        return view('recommendations.edit', compact('recommendation','movie'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Recommendation  $recommendation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $validated = $request->validate([
            'rating' => ['required'],
            'headline' => [new Headline],
            'review' => ['required', new Text],
        ]);

        try {
            $cryptedMovieId = $request->input('movieId');
            $movieId = Crypt::decryptString($cryptedMovieId);
            $cryptedUserId = $request->input('userId');
            $userId = Crypt::decryptString($cryptedUserId);
            $rating = $request->input('rating');
            $headline = $request->input('headline');
            $review = $request->input('review');
            DB::table('recommendations')->where('movieId','=',$movieId)->where('userId','=',$userId)->update([
                'rating'=>$rating,
                'headline'=>$headline,
                'review'=>$review,
                'updated_at'=>now()
            ]);
            return redirect('user/'.$userId);
        }
        catch (\Illuminate\Database\QueryException $e) {
            return Redirect::back()->withErrors("Ha habido un error.")->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Recommendation  $recommendation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $cryptedMovieId = $request->input('movieId');
            $movieId = Crypt::decryptString($cryptedMovieId);
            $cryptedUserId = $request->input('userId');
            $userId = Crypt::decryptString($cryptedUserId);
            DB::table('recommendations')->where('movieId','=',$movieId)->where('userId','=',$userId)->delete();
            return back();
        }
        catch (\Illuminate\Database\QueryException $e) {
            return Redirect::back();
        }
    }

    public function usefulness(Request $request) {
        $status = $request->get('status');
        $usefulness = $request->get('usefulness');
        $recommendation = $request->get('recommendation');
        $recId = Crypt::decryptString($recommendation);
        $user = $request->get('user');
        $userId = Crypt::decryptString($user);

        if ($usefulness == 'useful') {
            $search = DB::table('utilities')->where('recommendationId','=',$recId)->where('userId','=',$userId)->where('usefulness','=',$usefulness)->count();
            $contrarySearch = DB::table('utilities')->where('recommendationId','=',$recId)->where('userId','=',$userId)->where('usefulness','=','useless')->count();
            if ($search == 0 && $contrarySearch == 0) {
                DB::table('utilities')->insert([
                    'recommendationId'=>$recId,
                    'userId'=>$userId,
                    'usefulness'=>$usefulness,
                    'created_at' => now(),
                    'updated_at' => now()
                ]);
                DB::table('recommendations')->where('id','=',$recId)->increment('useful',1);
                if ($status == "inactive") {
                    $status = "active";
                }
            }
            if ($contrarySearch > 0) {
                DB::table('utilities')->where('recommendationId','=',$recId)->where('userId','=',$userId)->update([
                    'usefulness'=>'useful',
                    'updated_at'=>now()
                ]);
                DB::table('recommendations')->where('id','=',$recId)->decrement('useless',1);
                DB::table('recommendations')->where('id','=',$recId)->increment('useful',1);
                if ($status == "inactive") {
                    $status = "active";
                }
            }
            if ($search > 0) {
                DB::table('utilities')->where('recommendationId','=',$recId)->where('userId','=',$userId)->where('usefulness','=',$usefulness)->delete();
                DB::table('recommendations')->where('id','=',$recId)->decrement('useful',1);
                if ($status == "active") {
                    $status = "inactive";
                }
            }
        }

        else {
            $search = DB::table('utilities')->where('recommendationId','=',$recId)->where('userId','=',$userId)->where('usefulness','=',$usefulness)->count();
            $contrarySearch = DB::table('utilities')->where('recommendationId','=',$recId)->where('userId','=',$userId)->where('usefulness','=','useful')->count();
            if ($search == 0 && $contrarySearch == 0) {
                DB::table('utilities')->insert([
                    'recommendationId'=>$recId,
                    'userId'=>$userId,
                    'usefulness'=>$usefulness,
                    'created_at' => now(),
                    'updated_at' => now()
                ]);
                DB::table('recommendations')->where('id','=',$recId)->increment('useless',1);
                if ($status == "inactive") {
                    $status = "active";
                }
            }
            if ($contrarySearch > 0) {
                DB::table('utilities')->where('recommendationId','=',$recId)->where('userId','=',$userId)->update([
                    'usefulness'=>'useless',
                    'updated_at'=>now()
                ]);
                DB::table('recommendations')->where('id','=',$recId)->decrement('useful',1);
                DB::table('recommendations')->where('id','=',$recId)->increment('useless',1);
                if ($status == "inactive") {
                    $status = "active";
                }
            }
            if ($search > 0) {
                DB::table('utilities')->where('recommendationId','=',$recId)->where('userId','=',$userId)->where('usefulness','=',$usefulness)->delete();
                DB::table('recommendations')->where('id','=',$recId)->decrement('useless',1);
                if ($status == "active") {
                    $status = "inactive";
                }
            }
        }

        echo $status;
    }
}

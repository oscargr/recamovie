<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\Models\User;
use App\Models\Recommendation;
use App\Models\Movie;
use App\Models\Subscription;
use App\Models\Utility;
use App\Rules\Text;
use App\Rules\User as RulesUser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UsersController extends Controller
{
    
    public function viewUserProfile($userId) {
        $user = User::find($userId);
        if (is_null($user)) {
            return back();
        }
        $userRecommendations = Recommendation::where('userId', '=', $userId)->latest('updated_at')->paginate(2);
        $recommendations = Recommendation::all()->where('userId','=',$userId);
        $countUserRecommendations = count($recommendations);
        $subscribers = Subscription::all()->where('creatorId','=',$userId);
        $countUserSubscribers = count($subscribers);
        $subscribed = false;
        $subscriber = Subscription::where('creatorId','=',$userId)->where('subscriberId','=',Auth::user()->id)->count();
        if ($subscriber == 1) {
            $subscribed = true;
        }

        $userUsefulness = Utility::where('userId','=',Auth::user()->id)->get();

        $movies = array();

        $usefulness = array();

        $valorations = array();

        $totalVal = array();

        foreach ($userRecommendations as $recommendation) {
            $movie = Movie::find($recommendation['movieId']);
            $useful = $recommendation->useful;
            $total = $recommendation->useful + $recommendation->useless;
            if ($total>0) {
                $percentage = round(($useful/$total)*100);
            }
            else {
                $percentage = 0;
            }
            array_push($movies, $movie);
            array_push($valorations, $percentage);
            array_push($totalVal, $total);
            foreach ($userUsefulness as $use) {
                if ($recommendation['id'] == $use['recommendationId']) {
                    $usefulness[$recommendation['id']] = $use;
                }
            }
        }

        $genresTrans = array(
            'action'=>'Acción',
            'animation'=>'Animación',
            'adventure'=>'Aventuras',
            'biography'=>'Biográfica',
            'war'=>'Bélica',
            'sci-fi'=>'Ciencia ficción',
            'comedy'=>'Comedia',
            'documentary'=>'Documental',
            'drama'=>'Drama',
            'family'=>'Familiar',
            'fantasy'=>'Fantasía',
            'musical'=>'Musical',
            'thriller'=>'Suspense',
            'horror'=>'Terror',
            'western'=>'Western',
        );

        $genres = array(
            'action'=>0,
            'animation'=>0,
            'adventure'=>0,
            'biography'=>0,
            'war'=>0,
            'sci-fi'=>0,
            'comedy'=>0,
            'documentary'=>0,
            'drama'=>0,
            'family'=>0,
            'fantasy'=>0,
            'musical'=>0,
            'thriller'=>0,
            'horror'=>0,
            'western'=>0,
        );

        $reviewedGenres = DB::table('movies')->join('recommendations','movies.id','=','recommendations.movieId')->join('users','recommendations.userId','=','users.id')->select('movies.genre')->where('users.id','=',$userId)->get();

        foreach($reviewedGenres as $reviewedGenre) {
            $genres[$reviewedGenre->genre]++;
        }

        foreach($genres as $genreId=>$genre) {
            if ($genre==0) {
                unset($genres[$genreId]);
            }
        }

        /*foreach($genres as $genreId=>$genre) {
            $genres[$genreId] = round(($genre/$countUserRecommendations)*100,2);
        }*/

        arsort($genres);

        return view('users.profile', compact('user','userRecommendations','subscribed','countUserRecommendations','countUserSubscribers','movies','usefulness','valorations','totalVal','genres','genresTrans'));
    }

    public function getFollowedUsers($userId) {

        $user = User::find($userId);
        if (is_null($user)) {
            return back();
        }

        $followedUsers = User::join('subscriptions','users.id','=','subscriptions.creatorId')->where('subscriptions.subscriberId','=',Auth::user()->id)->latest('subscriptions.updated_at')->paginate(25);

        return view('users.followed', compact('followedUsers'));
    }

    public function getFollowers($userId) {

        $user = User::find($userId);
        if (is_null($user)) {
            return back();
        }

        $followers = User::join('subscriptions','users.id','=','subscriptions.subscriberId')->where('subscriptions.creatorId','=',Auth::user()->id)->latest('subscriptions.updated_at')->paginate(25);

        return view('users.followers', compact('followers'));
    }

    public function followUser(Request $request) {
        
        try {
            $cryptedCreatorId = $request->input('creatorId');
            $creatorId = Crypt::decryptString($cryptedCreatorId);
            $cryptedSubscriberId = $request->input('subscriberId');
            $subscriberId = Crypt::decryptString($cryptedSubscriberId);
            DB::table('subscriptions')->insert([
                'creatorId' => $creatorId,
                'subscriberId' => $subscriberId,
                'created_at' => now(),
                'updated_at' => now()
            ]);
            return redirect('user/'.$creatorId);
        }
        catch (\Illuminate\Database\QueryException $e) {
            return Redirect::back()->withErrors("Ha habido un error.")->withInput();
        } 

    }

    public function unfollowUser(Request $request) {
        
        try {
            $cryptedCreatorId = $request->input('creatorId');
            $creatorId = Crypt::decryptString($cryptedCreatorId);
            $cryptedSubscriberId = $request->input('subscriberId');
            $subscriberId = Crypt::decryptString($cryptedSubscriberId);
            DB::table('subscriptions')->where('creatorId','=',$creatorId)->where('subscriberId','=',$subscriberId)->delete();
            return redirect('user/'.$creatorId);
        }
        catch (\Illuminate\Database\QueryException $e) {
            return Redirect::back()->withErrors("Ha habido un error.")->withInput();
        } 

    }

    public function edit($userId) {

        $user = User::find($userId);
        if (is_null($user)) {
            return back();
        }
        return view('users.edit', compact('user'));

    }

    public function update(Request $request) {

        $validated = $request->validate([
            'name' => ['required',],
            'about_me' => [new Text],
            'profile_pic' => ['image','max:10240'],
            'banner_pic' => ['image','max:10240'],
        ]);

        try {
            $userId = Auth::user()->id;
            $user = User::find($userId);
            $userData = $request->all();

            if (!is_null($request->file('profile_pic'))) {
                $profilePicExtension = $request->file('profile_pic')->extension();
                $userData['profile_pic'] = $request->file('profile_pic')->storeAs('user_images/'.Auth::user()->id.'/profile_pic', 'User_'.Auth::user()->id.'_profile_pic.'.$profilePicExtension);
            }

            if (!is_null($request->file('banner_pic'))) {
                $bannerPicExtension = $request->file('banner_pic')->extension();
                $userData['banner_pic'] = $request->file('banner_pic')->storeAs('user_images/'.Auth::user()->id.'/banner_pic', 'User_'.Auth::user()->id.'_banner_pic.'.$bannerPicExtension);
            }

            $user->update($userData);
        }
        catch (\Illuminate\Database\QueryException $e) {
            return Redirect::back()->withErrors("Ha habido un error.")->withInput();
        }
        
        return redirect('user/'.$userId);

    }

    public function delete(Request $request) {
        try {
            $user = User::find(Auth::user()->id);
            if ($request->input('picture')=="profile") {
                Storage::deleteDirectory('user_images/'.Auth::user()->id.'/profile_pic');
                $user->profile_pic = 'storage/images/default/Default_profile_pic.png';
                $user->save();
            }
            else {
                Storage::deleteDirectory('user_images/'.Auth::user()->id.'/banner_pic');
                $user->banner_pic = 'storage/images/default/Default_profile_background.jpg';
                $user->save();
            }
        }
        catch (\Illuminate\Database\QueryException $e) {
            return Redirect::back()->withErrors("Ha habido un error.");
        }

        return redirect('user/'.Auth::user()->id);

    }

    public function getResults(Request $request) {

        $validated = $request->validate([
            'userSearch' => [new RulesUser]
        ]);

        $query = $request->input("userSearch");

        try {
            $results = DB::table('users')->where('name','LIKE',"%{$query}%")->where("role",'=','user')->paginate(10);
        }
        catch (\Illuminate\Database\QueryException $e) {
            return Redirect::back()->withErrors("Ha habido un error.");
        }

        return view('users.search', compact('results'));

    }

}

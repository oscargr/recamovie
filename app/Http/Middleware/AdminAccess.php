<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminAccess
{
    /**
     * Handle an incoming request, check if the user has the admin role and if the user doesn't have this role he would be redirected to the home page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(null!==Auth::user() && Auth::user()->role=='admin'){
            return $next($request);
        }

        return back();
    }
}

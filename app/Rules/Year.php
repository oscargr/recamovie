<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Year implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $regexp = "/^\d{4}$/";
        $actualYear = date("Y");
        if (preg_match_all($regexp, $value) && $value>="1833" && $value<=$actualYear) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Error en el año de la película';
    }
}

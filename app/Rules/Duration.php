<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Duration implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $regexp = "/^\d{1}h \d{2}min$/";
        if (preg_match_all($regexp, $value)) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Error en el formato de la duración. El formato es Xh XXmin.';
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use function PHPUnit\Framework\isNull;

class Movie extends Model
{
    use HasFactory;

    /**
     * Here we establish the relationship with the recommendations' table.
     */
    public function recommendations() {
        return $this->hasMany(Recommendation::class);

    }

    protected $fillable = [
        'title',
        'year',
        'director',
        'poster',
        'synopsis',
        'duration',
        'genre',
        'ratings',
        'trailer',
        'screenshots'
    ];

    /**
     * Here we set the array for the screenshots column.
     */

    protected $casts = [
        'ratings' => 'array',
        'screenshots' => 'array'
    ];

    /*public function setScreenshotsAttribute($value) {
        $screenshots = [];

        foreach ($value as $array_item) {
            if (!is_null($array_item['key'])) {
                $screenshots = $array_item;
            }
        }

        $this->attributes['screenshots'] = json_encode($screenshots);
    }*/

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recommendation extends Model
{
    use HasFactory;

    protected $fillable = [
        'movieId',
        'userId',
        'rating',
        'headline',
        'review'
    ];

    /**
     * Here we establish the relationship with the users' table.
     */
    public function users() {
        return $this->belongsTo(User::class);

    }

    /**
     * Here we establish the relationship with the movies' table.
     */
    public function movies() {
        return $this->belongsTo(Movie::class);

    }

    /**
     * Here we establish the relationship with the utility's table.
     */
    public function utility() {
        return $this->hasMany(Utility::class);

    }

}

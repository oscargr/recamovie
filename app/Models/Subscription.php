<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    use HasFactory;

    /**
     * Here we establish the relationship with the users' table.
     */
    public function users() {
        return $this->belongsTo(User::class);

    }

}

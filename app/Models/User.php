<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail, CanResetPassword
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'about_me',
        'profile_pic',
        'banner_pic'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Here we establish the relationship with the recommendations' table.
     */
    public function recommendations() {
        return $this->hasMany(Recommendation::class);

    }

    /**
     * Here we establish the relationship with the subscriptions' table.
     */
    public function subscriptions() {
        return $this->hasMany(Subscription::class);

    }

    /**
     * Here we establish the relationship with the utility's table.
     */
    public function utility() {
        return $this->hasMany(Utility::class);

    }
    
}

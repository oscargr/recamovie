<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Utility extends Model
{
    use HasFactory;

    /**
     * Here we establish the relationship with the recommendations' table.
     */
    public function recommendation() {
        return $this->belongsTo(Recommendation::class);

    }

    /**
     * Here we establish the relationship with the users' table.
     */
    public function users() {
        return $this->belongsTo(User::class);

    }
}
